package com.o2m.gsk.challenge;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.o2m.gsk.R;

public class OnlineAnswerAdapter extends ArrayAdapter<String> {

	//private View v;
	private Context context;
	private ArrayList<String> optionList;
	private int selectedPosition = -1 , currentCorrectOption = -1;

	public OnlineAnswerAdapter(Context context, int resource , ArrayList<String> _optionList) {
		super(context, resource);
		this.context = context;
		this.optionList = _optionList;
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public int getCount() {
		return optionList.size();
	}
	
	@Override
    public View getView(int pos, View view, ViewGroup parent){
        
		//this.v = convertView;
        
//        if(v == null) 
//        {
        	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          //  v = vi.inflate(R.layout.quiz_ans_item, parent, false);
        //}
            ViewHolder holder = new ViewHolder();
    		if (view == null)
    		{

    			holder = new ViewHolder();
		
    			view = inflater.inflate(R.layout.quiz_ans_item, parent, false);
    			holder.ansOptionTitle = (TextView) view.findViewById(R.id.ansOptionTitle);
    			holder.ansTxt = (TextView) view.findViewById(R.id.ansOptionTxt);
    	        
    			holder.ansTxt.setText(optionList.get(pos));
    	        
    	        if(pos == 0)
    	        {
    	        	holder.ansOptionTitle.setText("A");
    	        	holder.ansOptionTitle.setBackgroundColor(context.getResources().getColor(R.color.gskOrange));
    	        	holder.ansTxt.setBackgroundColor(Color.GRAY);
    	        }
    	        else if(pos == 1)
    	        {
    	        	holder.ansOptionTitle.setText("B");
    	        	holder.ansOptionTitle.setBackgroundColor(Color.GRAY);
    	        	holder.ansTxt.setTextColor(Color.BLACK);
    	        	holder.ansTxt.setBackgroundColor(Color.WHITE);
    	        }
    	        else if(pos == 2)
    	        {
    	        	holder.ansOptionTitle.setText("C");
    	        	holder.ansOptionTitle.setBackgroundColor(context.getResources().getColor(R.color.gskOrange));
    	        	holder.ansTxt.setBackgroundColor(Color.GRAY);
    	        }
    	        else if(pos == 3)
    	        {
    	        	holder.ansOptionTitle.setText("D");
    	        	holder.ansOptionTitle.setBackgroundColor(Color.GRAY);
    	        	holder.ansTxt.setTextColor(Color.BLACK);
    	        	holder.ansTxt.setBackgroundColor(Color.WHITE);
    	        }
    	        
    	        Log.v("", "getSelectedPosition() : " + getSelectedPosition() + "pos : " + pos + "getCurrentCorrectOption() : " + getCurrentCorrectOption());
    	        
    	        
    	        if( pos == getSelectedPosition())
    	        {
    	        	holder.ansTxt.setBackgroundColor(context.getResources().getColor(R.color.correct_ans));
    	        }
    	        
    	        
    			
    			view.setTag(holder);
    		}
    		else
    		{
    			holder = (ViewHolder) view.getTag();
    		}

        
        
        
        
        
        return view;
    }
	
	private static class ViewHolder
	{

		public TextView ansOptionTitle;
		public TextView ansTxt;

	}

	public int getSelectedPosition() {
		return selectedPosition;
	}


	public void setSelectedPosition(int selectedPosition) {
		this.selectedPosition = selectedPosition;
		this.notifyDataSetChanged();
		Log.v("ANSWER ADAPTER", "" + selectedPosition);
	}


	public int getCurrentCorrectOption() {
		return currentCorrectOption;
	}


	public void setCurrentCorrectOption(int currentCorrectOption) {
		this.currentCorrectOption = currentCorrectOption;
	}
	

}
