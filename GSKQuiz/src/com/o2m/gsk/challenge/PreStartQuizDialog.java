package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.myscore.MyScoreFragment;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class PreStartQuizDialog extends Dialog implements
		android.view.View.OnClickListener {

	public Activity activity;
	public Dialog dialog;
	public Button cancelBtn, playBtn, scoreBtn;
	private TextView quizNameTV, quizModeTV;
	private String quizId, mode , quizName;

	public PreStartQuizDialog(Activity context, String _quizId , String _mode, String _name) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;
		this.quizId = _quizId;
		this.mode = _mode;
		this.quizName = _name;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.pre_start_quiz_dialog);

		quizNameTV = (TextView) findViewById(R.id.quizNameTV);
		quizModeTV = (TextView) findViewById(R.id.quizModeTV);
		quizModeTV.setText("" + mode);
		quizNameTV.setText(""+quizName);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
		playBtn = (Button) findViewById(R.id.playBtn);
		scoreBtn = (Button) findViewById(R.id.scoreBtn);
		cancelBtn.setOnClickListener(this);
		playBtn.setOnClickListener(this);
		scoreBtn.setOnClickListener(this);
		
		
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0 == cancelBtn)
		{
			this.dismiss();
		}
		else if(arg0 == playBtn)
		{
			CacheMemory.setOnlineChallengeStatus(
					activity,
					CacheMemory.ONLINE_CHALLENGE_YES);
			
			if(mode.equalsIgnoreCase("Challenge Yourself"))
				this.loadSelfQuiz(this.quizId);
			else if(mode.equalsIgnoreCase("Challenge Friend"))
				this.loadfriends(this.quizId);
		}
		else if(arg0 == scoreBtn)
		{
			this.loadMyScore();
		}
		
	}

	private void loadMyScore() {
		// TODO Auto-generated method stub
		new ApplicationAsk(activity, "Fetching score", new ApplicationService() {

			private String resp;

			@Override
			public void postExecute() {
				

				try {

					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = dashboardResponseObj
								.getJSONArray("data");

						PreStartQuizDialog.this.dismiss();
						Fragment fragment = new MyScoreFragment(dataObj);
						FragmentTransaction ft = activity.getFragmentManager()
								.beginTransaction();

						ft.addToBackStack("MyScoreFragment");
						ft.replace(R.id.content_frame, fragment,
								"MyScoreFragment");

						ft.commit();

					} else {
						Toast.makeText(activity,
								"" + dashboardResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {

				String loginURL = URLGenerator.getMyScoreUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(loginURL, "",
						CacheMemory.getToken(activity), activity);
				

			}
		}).execute();

	}

	protected void loadSelfQuiz(String quizID) {
		// TODO Auto-generated method stub

		final String QuizId = quizID;

		new ApplicationAsk(activity, "Fetching questions", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {


				try {

					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataArray = dashboardResponseObj
								.getJSONArray("data");
						PreStartQuizDialog.this.dismiss();
						QuestionListFragment fragment = new QuestionListFragment(
								dataArray);
						fragment.setQuizId(QuizId);
						FragmentManager fragmentManager = activity
								.getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();

						//ft.addToBackStack("QuestionListFragment");
						ft.replace(R.id.content_frame, fragment,
								"QuestionListFragment");

						ft.commit();

					} else {
						Toast.makeText(
								activity,
								"" + dashboardResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject subTopicReq = new JSONObject();
				try {
					subTopicReq.put("QuizId", QuizId);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				String subtopicURL = URLGenerator.getQuestionByQuizUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(subtopicURL,
						subTopicReq.toString(), CacheMemory
								.getToken(activity) ,activity);// preferences.getString("Token"));

				

			}
		}).execute();

	}

	private void loadfriends(String quizID) {

		new ApplicationAsk(activity, "Fetching friends",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {

						String friendListURL = URLGenerator.getFriendListForChallengeURL();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								friendListURL, "", CacheMemory
										.getToken(activity), activity);

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONArray dataObj = friendResponseObj
										.getJSONArray("data");
								PreStartQuizDialog.this.dismiss();
								ChallengeFriendListFragment fragment = new ChallengeFriendListFragment(
										dataObj, activity);
								fragment.setQuizId(PreStartQuizDialog.this.quizId);
								FragmentManager fragmentManager = activity.getFragmentManager();

								FragmentTransaction ft = fragmentManager
										.beginTransaction();

								ft.addToBackStack("FriendListFragment");
								ft.replace(R.id.content_frame, fragment,
										"FriendListFragment");

								ft.commit();

							} else {
								Toast.makeText(
										activity,
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				}).execute();
	}
	
	
}
