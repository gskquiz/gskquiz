package com.o2m.gsk.challenge;

import org.json.JSONArray;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.gsk.GSKHomeActivity;
import com.o2m.gsk.R;
import com.o2m.gsk.utility.CacheMemory;

public class ChallengeQuizCompleteFragment extends Fragment implements
		OnClickListener {

	private Button viewAnsBtn, playagainBtn;
	private ImageView imageView;
	private TextView quizNameTV, pointsTV, statusTV, correctAnsTV , msgTV;
	private String quizname, points;
	private int correctAns, totalquestions;
	protected String TAG;
	private JSONArray queAnsArray = new JSONArray();

	public ChallengeQuizCompleteFragment(String _quizname, String _points,
			int _correctAns, int _totalquestions) {
		// TODO Auto-generated constructor stub
		this.quizname = _quizname;
		this.points = _points;
		this.correctAns = _correctAns;
		this.totalquestions = _totalquestions;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.challengequizcompletion_fragment,
				container, false);
		viewAnsBtn = (Button) rootView.findViewById(R.id.winnerviewanswerBtn);
		playagainBtn = (Button) rootView.findViewById(R.id.winnerplayagainBtn);

		quizNameTV = (TextView) rootView.findViewById(R.id.quizName);
		pointsTV = (TextView) rootView.findViewById(R.id.pointswonTv);
		
		
		
		
		
		viewAnsBtn.setOnClickListener(this);
		playagainBtn.setOnClickListener(this);
		
		quizNameTV.setText(quizname);
		pointsTV.setText("Your Points :- " + points);
		
		CacheMemory.setOnlineChallengeStatus(
				ChallengeQuizCompleteFragment.this.getActivity(),
				CacheMemory.ONLINE_CHALLENGE_NO);
			

		return rootView;
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == playagainBtn) {
			// loadLevels(0);
			
			Intent home = new Intent(ChallengeQuizCompleteFragment.this.getActivity() , GSKHomeActivity.class);
			ChallengeQuizCompleteFragment.this.getActivity().startActivity(home);
		} else if (arg0 == viewAnsBtn) {
			Fragment fragment = new QueAnsListFragment(queAnsArray);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
		
			ft.addToBackStack("InviteFriendFragment");
			ft.replace(R.id.content_frame, fragment, "InviteFriendFragment");

			ft.commit();
		}

	}

	public JSONArray getQueAnsArray() {
		return queAnsArray;
	}

	public void setQueAnsArray(JSONArray queAnsArray) {
		this.queAnsArray = queAnsArray;
	}


}
