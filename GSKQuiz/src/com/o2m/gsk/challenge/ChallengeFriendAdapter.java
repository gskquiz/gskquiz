package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.o2m.gsk.R;

public class ChallengeFriendAdapter extends BaseAdapter {

	private JSONArray items;
	private Activity context;

	public ChallengeFriendAdapter(Activity _context, JSONArray _items) {
		this.items = _items;
		this.context = _context;
	}

	@Override
	public int getCount() {

		return this.items.length();
	}

	@Override
	public Object getItem(int position) {

		try {
			return this.items.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0) {

		return 0;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null) {

			holder = new ViewHolder();
			view = inflater
					.inflate(R.layout.adapter_challenge_friendslist, parent, false);
			holder.txtView_msgTitle = (TextView) view
					.findViewById(R.id.name_friend);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		try {
			final JSONObject msgObj = (JSONObject) this.items.get(position);

			holder.txtView_msgTitle.setText("" + msgObj.getString("Fullname"));

		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * if (position % 2 == 0) {
		 * view.setBackgroundColor(Color.parseColor(context.getResources()
		 * .getString(R.string.gray_gradient_1))); }
		 */
		return view;

	}

	private static class ViewHolder {

		public TextView txtView_msgTitle;

	}

	public JSONArray getItems() {
		return items;
	}

	public void setItems(JSONArray items) {
		this.items = items;
	}

	public JSONArray RemoveJSONArray(JSONArray jarray, int pos) {

		JSONArray Njarray = new JSONArray();
		try {
			for (int i = 0; i < jarray.length(); i++) {
				if (i != pos)
					Njarray.put(jarray.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Njarray;

	}

}
