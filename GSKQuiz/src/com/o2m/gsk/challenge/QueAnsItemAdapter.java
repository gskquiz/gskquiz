package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.o2m.gsk.R;

public class QueAnsItemAdapter extends BaseAdapter {

	Context context;

	JSONArray queAnsJsonArray;
	String unreadCount;

	public QueAnsItemAdapter(Context context, JSONArray _queAnsArray) {

		this.context = context;
		this.queAnsJsonArray = _queAnsArray;
	}

	@Override
	public int getCount() {

		return queAnsJsonArray.length();
	}

	@Override
	public Object getItem(int position) {

		try {
			return queAnsJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0) {

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		//if (view == null) {

			holder = new ViewHolder();

			view = inflater.inflate(R.layout.que_ans_list_item, parent, false);
			holder.txtView_question = (TextView) view
					.findViewById(R.id.questionTV);
			holder.txtView_ans = (TextView) view
					.findViewById(R.id.correctAnsTV);
			holder.txtView_questionNo = (TextView) view
					.findViewById(R.id.questionId);

			view.setTag(holder);

		/*} else {
			holder = (ViewHolder) view.getTag();
		}*/

		try {

			JSONObject queObj = (JSONObject) queAnsJsonArray.get(position);
			int correctOption = queObj.getInt("CorrectOption");
			holder.txtView_question.setText("" + queObj.getString("Question"));
			holder.txtView_ans.setText(""
					+ queObj.getString("Option" + correctOption));
			holder.txtView_questionNo.setText("Q." + (position + 1));

		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (position % 2 == 0) {
			view.setBackgroundColor(Color.parseColor(context.getResources()
					.getString(R.string.gray_gradient_1)));
		}

		return view;

	}

	private static class ViewHolder {

		public TextView txtView_question;
		public TextView txtView_ans;
		public TextView txtView_questionNo;

	}

}
