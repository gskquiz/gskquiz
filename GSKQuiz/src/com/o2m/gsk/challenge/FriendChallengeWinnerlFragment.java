package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.CacheMemory;

public class FriendChallengeWinnerlFragment extends Fragment implements
		OnClickListener {

	private Button viewAnsBtn, playagainBtn;
	private ImageView imageView;
	private TextView quizNameTV, statusTV , msgTV , yourPointsTXT , opponentPointsTXT;
	private String quizname, oppoName , oppoScore , myscore ;
	
	protected String TAG;
	private JSONObject ssObj;
	private JSONArray queAnsArray = new JSONArray();

	public FriendChallengeWinnerlFragment(String _quizname, JSONObject _ssObj ) {
		// TODO Auto-generated constructor stub
		this.quizname = _quizname;
		this.ssObj = _ssObj;
		
	}
	
	public FriendChallengeWinnerlFragment(String _quizname, String _opponame , String _opposcore , String _myscore ) {
		// TODO Auto-generated constructor stub
		this.quizname = _quizname;
		this.oppoName = _opponame;
		this.oppoScore = _opposcore;
		this.myscore = _myscore;
		
	}
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.online_challenge_winner_fragment,
				container, false);
		viewAnsBtn = (Button) rootView.findViewById(R.id.winnerviewanswerBtn);
		playagainBtn = (Button) rootView.findViewById(R.id.winnerplayagainBtn);

		quizNameTV = (TextView) rootView.findViewById(R.id.quizName);
		//pointsTV = (TextView) rootView.findViewById(R.id.pointswonTv);
		//correctAnsTV = (TextView) rootView.findViewById(R.id.totalanswersTv);
		statusTV = (TextView) rootView.findViewById(R.id.quizstatusTv);
		msgTV = (TextView) rootView.findViewById(R.id.msgTv);
		imageView = (ImageView) rootView.findViewById(R.id.imageView);
		
		viewAnsBtn.setOnClickListener(this);
		playagainBtn.setOnClickListener(this);
		
		quizNameTV.setText(quizname);
		
		yourPointsTXT = (TextView) rootView.findViewById(R.id.pointswonTv);
		opponentPointsTXT = (TextView) rootView.findViewById(R.id.opponentspointsTV);
		
		
		
		if(oppoScore != null && myscore != null)
		{
			yourPointsTXT.setText("Your Points - "+myscore);
			opponentPointsTXT.setText(""+ oppoName + "'s Points - "+oppoScore);
			if(Integer.valueOf(myscore) == Integer.valueOf(oppoScore))
			{
				opponentPointsTXT.setTextColor(Color.BLACK);
				opponentPointsTXT.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
				statusTV.setText("SCORE TIE");
				imageView.setImageResource(R.drawable.tied);
			}
			else if(Integer.valueOf(myscore) > Integer.valueOf(oppoScore))
			{
				opponentPointsTXT.setTextColor(Color.BLACK);
				opponentPointsTXT.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
				statusTV.setText("YOU WON");
				imageView.setImageResource(R.drawable.trophy);
			}
			else{
				
				yourPointsTXT.setTextColor(Color.BLACK);
				yourPointsTXT.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
				statusTV.setText("YOU LOSE");
				imageView.setImageResource(R.drawable.loseremoticon);
			}

		}
		
		
		CacheMemory.setOnlineChallengeStatus(
				FriendChallengeWinnerlFragment.this.getActivity(),
				CacheMemory.ONLINE_CHALLENGE_NO);

		return rootView;
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == playagainBtn) {
			
			
			/*Fragment fragment = new QuestionListFragment(queAnsArray);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
	
			ft.addToBackStack("QuestionListFragment");
			ft.replace(R.id.content_frame, fragment, "QuestionListFragment");

			ft.commit();*/
			
			
			
		} 
		else if (arg0 == viewAnsBtn)
		{
			Fragment fragment = new QueAnsListFragment(queAnsArray);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
	
			ft.addToBackStack("QueAnsListFragment");
			ft.replace(R.id.content_frame, fragment, "QueAnsListFragment");

			ft.commit();
		}

	}

	public JSONArray getQueAnsArray() {
		return queAnsArray;
	}

	public void setQueAnsArray(JSONArray queAnsArray) {
		this.queAnsArray = queAnsArray;
	}


}
