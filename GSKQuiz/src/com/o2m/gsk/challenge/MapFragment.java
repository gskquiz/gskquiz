package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MapFragment extends Fragment {

	private WebView myBrowser;
	private int screenName , counter = 0;
	MyCountDown5SecTimer myDown5SecTimer;
	protected String quizId;

	public MapFragment(String _quizId) {
		this.quizId = _quizId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_offline_web,
				container, false);

		myBrowser = (WebView) rootView.findViewById(R.id.mybrowser);
		myBrowser.setBackgroundColor(Color.TRANSPARENT);

		myBrowser.getSettings().setJavaScriptEnabled(true); // enable javascript

		myBrowser.getSettings().setDomStorageEnabled(true);
		String databasePath = this.getActivity().getApplicationContext()
				.getDir("database", Context.MODE_PRIVATE).getPath();
		myBrowser.getSettings().setDatabasePath(databasePath);
		if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
			myBrowser.getSettings().setAllowUniversalAccessFromFileURLs(true);

		this.loadRespectiveHtml();
		
		MapFragment.this.RandomOppenentSelection();
		
		myDown5SecTimer = new MyCountDown5SecTimer(
				1000 * 5, 1000 * 5);
		myDown5SecTimer.start();
		

		return rootView;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		myBrowser.onPause();
		super.onPause();
	}

	private void loadRespectiveHtml() {

		myBrowser.loadUrl("file:///android_asset/Map/index.html");

		myBrowser.setWebViewClient(new WebViewClient() {

			public void onPageFinished(WebView view, String url) {

				Log.v("WEBACtivity", "JS Call");

			}
		});

	}
	
	
public class MyCountDown10SecTimer extends CountDownTimer {
		

		public MyCountDown10SecTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {

			Log.v("", "MyCountDown10SecTimer");
			MapFragment.this.RandomOppenentSelection();
			MapFragment.this.repeat10SecTimer();
		}

		@Override
		public void onTick(long milliUntilFinished) {

		
		}

		
	}
	
	public class MyCountDown5SecTimer extends CountDownTimer {
		

		public MyCountDown5SecTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
			MapFragment.this.counter ++;
		}

		@Override
		public void onFinish() {

			Log.v("", "MyCountDown5SecTimer " + MapFragment.this.counter );
			
			if(MapFragment.this.counter <= 6)
			{
				MapFragment.this.CheckOnlineStatus();
				MapFragment.this.repeat5SecTimer();
			}
			else
			{
				MapFragment.this.showAlertAndPopBack();
			}
			
		}

		@Override
		public void onTick(long milliUntilFinished) {

		
			
		}



	}


	public void repeat10SecTimer() {
		// TODO Auto-generated method stub
		/*if (myCountDown10SecTimer != null)
			myCountDown10SecTimer.cancel();
		myCountDown10SecTimer = new MyCountDown10SecTimer(1000 * 10, 1000 * 5);
		myCountDown10SecTimer.start();*/
	}

	public void showAlertAndPopBack() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		// 2. Chain together various setter methods to set the dialog characteristics
		builder.setMessage("No Opponent Available. Try Challenge yourself")
		       .setTitle("");
		
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	               // User clicked OK button
	        	   CacheMemory.setOnlineChallengeStatus(
	       				MapFragment.this.getActivity(),
	       				CacheMemory.ONLINE_CHALLENGE_NO);
	        	   MapFragment.this.getActivity().getFragmentManager().popBackStack();
	           }
	       });
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public void repeat5SecTimer() {
		// TODO Auto-generated method stub
		if (myDown5SecTimer != null)
			myDown5SecTimer.cancel();
		myDown5SecTimer = new MyCountDown5SecTimer(1000 * 5, 1000 * 5);
		myDown5SecTimer.start();
	}
	
	public void CheckOnlineStatus() {
		// TODO Auto-generated method stub
		ApplicationAsk aaa = new ApplicationAsk(MapFragment.this.getActivity(), "",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject sOZ = new JSONObject();
						try {
							sOZ.put("ConnectionId",
									CacheMemory.getConnectionId(getActivity()));
							

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String friendListURL = URLGenerator
								.getCheckConnectionStatusUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								friendListURL, sOZ.toString(), CacheMemory
										.getToken(MapFragment.this
												.getActivity()),MapFragment.this
												.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONObject dataObj = friendResponseObj
										.getJSONArray("data").getJSONObject(0);
								//IsAccepted
								
								int isAccepted = dataObj.getInt("IsAccepted");
								
								if(isAccepted == 1)
								{
									// cancel Timers as quiz accepted 
									/*if(myCountDown10SecTimer != null)
										myCountDown10SecTimer.cancel();*/
									
									if(myDown5SecTimer != null)
										myDown5SecTimer.cancel();
									
									//Move To Next
									//save opponent and challenger ID
									String opponentName = dataObj.getString("OpponentName");
									MapFragment.this.loadOnlineQuiz(opponentName);
									
								}
								else 
								{
									// nobody accepted quiz
								}
								
							
								
							} else {
								Toast.makeText(
										MapFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				});
		aaa.setProgressHidden(true);
		aaa.execute();

	}

	public void RandomOppenentSelection() {
		// TODO Auto-generated method stub
		ApplicationAsk aa = new ApplicationAsk(MapFragment.this.getActivity(),"",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject sOZ = new JSONObject();
						try {
							sOZ.put("ConnectionId",
									CacheMemory.getConnectionId(getActivity()));
							String requestCnt = CacheMemory
									.getRequestCount(getActivity());
							
								sOZ.put("RequestCount", 0);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String friendListURL = URLGenerator
								.getSelectOpponentUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								friendListURL, sOZ.toString(), CacheMemory
										.getToken(MapFragment.this
												.getActivity()),MapFragment.this
												.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONObject dataObj = friendResponseObj
										.getJSONArray("data").getJSONObject(0);

								String requestCnt = dataObj
										.getString("RequestCount");

								CacheMemory.setRequestCount(getActivity(),
										requestCnt);
								
								
							
							} else {
								Toast.makeText(
										MapFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				});
		aa.setProgressHidden(true);
		aa.execute();
		
	}
	
	public void loadOnlineQuiz(final String opponentName)
	{
		new ApplicationAsk(MapFragment.this.getActivity(),"Fetching questions . .",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject gQO = new JSONObject();
						try {
							gQO.put("ConnectionId",
									CacheMemory.getConnectionId(getActivity()));
							gQO.put("QuizId",
									MapFragment.this.quizId);
							

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							
						}
						String getQuestiondOnlineURL = URLGenerator
								.getQuestionByQuizChallengeUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								getQuestiondOnlineURL, gQO.toString(), CacheMemory
										.getToken(MapFragment.this
												.getActivity()),MapFragment.this
												.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONArray dataArray = friendResponseObj
										.getJSONArray("data");
								
								OnlineQuestionListFragment oqlf = new OnlineQuestionListFragment(dataArray);
								oqlf.setOpponentName(opponentName);
								FragmentTransaction ft = getFragmentManager().beginTransaction();
								//ft.addToBackStack("OnlineQuestionListFragment");
								ft.replace(R.id.content_frame, oqlf, "OnlineQuestionListFragment");

								ft.commit();
								
								
							
								
							} else {
								Toast.makeText(
										MapFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				}).execute();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.v("MAP FRAGMENT", "OnDestroy");
		CacheMemory.setOnlineChallengeStatus(
				MapFragment.this.getActivity(),
				CacheMemory.ONLINE_CHALLENGE_NO);
		if(myDown5SecTimer != null)
		myDown5SecTimer.cancel();
		
	}
	
	
		
	
}
