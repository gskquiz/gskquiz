package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class ChallengeListFragment extends Fragment implements OnClickListener {

	private TextView challengeYourselfTV, challengemyfriendTV,
			challengeOnlineTV, quizNameTV, topicNameTV;
	protected String TAG, quizId, quizName;
	// MyCountDown10SecTimer myCountDown10SecTimer;
	MyCountDown5SecTimer myDown5SecTimer;

	public ChallengeListFragment(String _id, String _name) {
		this.quizId = _id;
		this.quizName = _name;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.challenge_list_fragment,
				container, false);
		quizNameTV = (TextView) rootView.findViewById(R.id.levelTitleTv);
		topicNameTV = (TextView) rootView.findViewById(R.id.topicNameTv);
		challengeYourselfTV = (TextView) rootView
				.findViewById(R.id.challengeyourselfTV);

		challengemyfriendTV = (TextView) rootView
				.findViewById(R.id.challengemyfriendTV);
		challengeOnlineTV = (TextView) rootView
				.findViewById(R.id.challengeOnlineTV);

		challengeYourselfTV.setOnClickListener(this);
		challengemyfriendTV.setOnClickListener(this);
		challengeOnlineTV.setOnClickListener(this);

		topicNameTV.setText(""
				+ CacheMemory.getTopicName(ChallengeListFragment.this
						.getActivity()));
		quizNameTV.setText("" + quizName);

		CacheMemory.setLevelName(ChallengeListFragment.this.getActivity(),
				quizName);

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.w("ChallengeListFragment", "ChallengeListFragment:onResume()");
		CacheMemory.setOnlineChallengeStatus(
				ChallengeListFragment.this.getActivity(),
				CacheMemory.ONLINE_CHALLENGE_NO);
	}
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == challengeYourselfTV) {
			// loadSelfQuiz(this.quizId);
			PreStartQuizDialog psqd = new PreStartQuizDialog(getActivity(),
					quizId, "Challenge Yourself", quizName);
			psqd.show();

		} else if (arg0 == challengemyfriendTV) {
			// this.loadfriends(this.quizId);
			PreStartQuizDialog psqd = new PreStartQuizDialog(getActivity(),
					quizId, "Challenge Friend", quizName);
			psqd.show();

		} else if (arg0 == challengeOnlineTV) {
			CacheMemory.setOnlineChallengeStatus(
					ChallengeListFragment.this.getActivity(),
					CacheMemory.ONLINE_CHALLENGE_YES);
			saveQuizConnection(this.quizId);
		}

	}

	/*protected void loadSelfQuiz(String quizID) {
		// TODO Auto-generated method stub

		final String QuizId = quizID;

		new ApplicationAsk(getActivity(), new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				Log.v(TAG, " onPostExecute Subtopic Response : " + resp);

				try {

					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataArray = dashboardResponseObj
								.getJSONArray("data");

						QuestionListFragment fragment = new QuestionListFragment(
								dataArray);
						fragment.setQuizId(QuizId);
						FragmentManager fragmentManager = getActivity()
								.getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();

						ft.addToBackStack("QueListFragment");
						ft.replace(R.id.content_frame, fragment,
								"QueListFragment");

						ft.commit();

					} else {
						Toast.makeText(
								ChallengeListFragment.this.getActivity(),
								"" + dashboardResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject subTopicReq = new JSONObject();
				try {
					subTopicReq.put("QuizId", QuizId);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String subtopicURL = URLGenerator.getQuestionByQuizUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(subtopicURL,
						subTopicReq.toString(), CacheMemory
								.getToken(ChallengeListFragment.this
										.getActivity()));// preferences.getString("Token"));

			}
		}).execute();

	}*/

	private void loadfriends(String quizID) {

		new ApplicationAsk(ChallengeListFragment.this.getActivity(), "Fetching friends",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {

						String friendListURL = URLGenerator
								.getFriendListForChallengeURL();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								friendListURL, "", CacheMemory
										.getToken(ChallengeListFragment.this
												.getActivity()), ChallengeListFragment.this
												.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONArray dataObj = friendResponseObj
										.getJSONArray("data");

								ChallengeFriendListFragment fragment = new ChallengeFriendListFragment(
										dataObj, ChallengeListFragment.this
												.getActivity());
								fragment.setQuizId(ChallengeListFragment.this.quizId);
								FragmentManager fragmentManager = getFragmentManager();

								FragmentTransaction ft = getFragmentManager()
										.beginTransaction();

								ft.addToBackStack("FriendListFragment");
								ft.replace(R.id.content_frame, fragment,
										"FriendListFragment");

								ft.commit();

							} else {
								Toast.makeText(
										ChallengeListFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				}).execute();
	}

	private void saveQuizConnection(final String quizID) {

		new ApplicationAsk(ChallengeListFragment.this.getActivity(),"Please wait",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject sOZ = new JSONObject();
						try {
							sOZ.put("QuizId", quizID);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String friendListURL = URLGenerator
								.getSaveOnlineQuizURL();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								friendListURL, sOZ.toString(), CacheMemory
										.getToken(ChallengeListFragment.this
												.getActivity()) , ChallengeListFragment.this
												.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONObject dataObj = friendResponseObj
										.getJSONArray("data").getJSONObject(0);

								String connectionID = dataObj
										.getString("ConnectionId");

								CacheMemory.setConnectionId(getActivity(),
										connectionID);

								Fragment fragment = new MapFragment(
										ChallengeListFragment.this.quizId);
								FragmentTransaction ft = getFragmentManager()
										.beginTransaction();
								ft.addToBackStack("MapFragment");
								ft.replace(R.id.content_frame, fragment,
										"MapFragment");
								ft.commit();

							} else {
								Toast.makeText(
										ChallengeListFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				}).execute();
	}

	public class MyCountDown10SecTimer extends CountDownTimer {

		public MyCountDown10SecTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {

			Log.v("", "MyCountDown10SecTimer");
			ChallengeListFragment.this.RandomOppenentSelection();
			ChallengeListFragment.this.repeat10SecTimer();
		}

		@Override
		public void onTick(long milliUntilFinished) {

		}

	}

	public class MyCountDown5SecTimer extends CountDownTimer {

		public MyCountDown5SecTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {

			Log.v("", "MyCountDown5SecTimer");
			ChallengeListFragment.this.CheckOnlineStatus();
			ChallengeListFragment.this.repeat5SecTimer();

		}

		@Override
		public void onTick(long milliUntilFinished) {

		}

	}

	public void repeat10SecTimer() {
		// TODO Auto-generated method stub
		/*
		 * if (myCountDown10SecTimer != null) myCountDown10SecTimer.cancel();
		 * myCountDown10SecTimer = new MyCountDown10SecTimer(1000 * 10, 1000 *
		 * 5); myCountDown10SecTimer.start();
		 */
	}

	public void repeat5SecTimer() {
		// TODO Auto-generated method stub
		if (myDown5SecTimer != null)
			myDown5SecTimer.cancel();
		myDown5SecTimer = new MyCountDown5SecTimer(1000 * 5, 1000 * 5);
		myDown5SecTimer.start();
	}

	public void CheckOnlineStatus() {
		// TODO Auto-generated method stub
		ApplicationAsk aaa = new ApplicationAsk(
				ChallengeListFragment.this.getActivity(),"Please wait . .",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject sOZ = new JSONObject();
						try {
							sOZ.put("ConnectionId",
									CacheMemory.getConnectionId(getActivity()));

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String friendListURL = URLGenerator
								.getCheckConnectionStatusUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								friendListURL, sOZ.toString(), CacheMemory
										.getToken(ChallengeListFragment.this
												.getActivity()),ChallengeListFragment.this
												.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONObject dataObj = friendResponseObj
										.getJSONArray("data").getJSONObject(0);
								// IsAccepted

								int isAccepted = dataObj.getInt("IsAccepted");

								if (isAccepted == 1) {
									// cancel Timers as quiz accepted
									/*
									 * if(myCountDown10SecTimer != null)
									 * myCountDown10SecTimer.cancel();
									 */

									if (myDown5SecTimer != null)
										myDown5SecTimer.cancel();

									// Move To Next
									// save opponent and challenger ID

									ChallengeListFragment.this.loadOnlineQuiz();

								} else {
									// nobody accepted quiz
								}

							} else {
								Toast.makeText(
										ChallengeListFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				});
		aaa.setProgressHidden(true);
		aaa.execute();

	}

	public void RandomOppenentSelection() {
		// TODO Auto-generated method stub
		ApplicationAsk aa = new ApplicationAsk(
				ChallengeListFragment.this.getActivity(),"Please wait",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject sOZ = new JSONObject();
						try {
							sOZ.put("ConnectionId",
									CacheMemory.getConnectionId(getActivity()));
							String requestCnt = CacheMemory
									.getRequestCount(getActivity());

							sOZ.put("RequestCount", 0);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String friendListURL = URLGenerator
								.getSelectOpponentUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								friendListURL, sOZ.toString(), CacheMemory
										.getToken(ChallengeListFragment.this
												.getActivity()),ChallengeListFragment.this
												.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONObject dataObj = friendResponseObj
										.getJSONArray("data").getJSONObject(0);

								String requestCnt = dataObj
										.getString("RequestCount");

								CacheMemory.setRequestCount(getActivity(),
										requestCnt);

							} else {
								Toast.makeText(
										ChallengeListFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				});
		aa.setProgressHidden(true);
		aa.execute();

	}

	public void loadOnlineQuiz() {
		new ApplicationAsk(ChallengeListFragment.this.getActivity(),"Please wait",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject gQO = new JSONObject();
						try {
							gQO.put("ConnectionId",
									CacheMemory.getConnectionId(getActivity()));
							gQO.put("QuizId", ChallengeListFragment.this.quizId);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();

						}
						String getQuestiondOnlineURL = URLGenerator
								.getQuestionByQuizChallengeUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								getQuestiondOnlineURL, gQO.toString(),
								CacheMemory.getToken(ChallengeListFragment.this
										.getActivity()),ChallengeListFragment.this
										.getActivity());

					}

					@Override
					public void postExecute() {

						try {

							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONArray dataArray = friendResponseObj
										.getJSONArray("data");

								OnlineQuestionListFragment oqlf = new OnlineQuestionListFragment(
										dataArray);
								FragmentTransaction ft = getFragmentManager()
										.beginTransaction();
								ft.addToBackStack("OnlineQuestionListFragment");
								ft.replace(R.id.content_frame, oqlf,
										"OnlineQuestionListFragment");

								ft.commit();

							} else {
								Toast.makeText(
										ChallengeListFragment.this
												.getActivity(),
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				}).execute();
	}
}
