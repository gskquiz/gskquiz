package com.o2m.gsk.challenge;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.o2m.gsk.R;

public class AnswerDetailsDialog extends Dialog implements
		android.view.View.OnClickListener {

	public Activity activity;
	public Dialog dialog;
	public Button cancelBtn;
	private TextView queNoTxt, queTxt, ansTxt, descTxt;
	private JSONObject queObj;
	private int position;

	public AnswerDetailsDialog(Activity context, JSONObject _nTObj, int _pos) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;
		this.queObj = _nTObj;
		this.position = _pos;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.answer_details_dialog);

		queNoTxt = (TextView) findViewById(R.id.questionId);
		queTxt = (TextView) findViewById(R.id.questionTV);
		ansTxt = (TextView) findViewById(R.id.correctAnsTV);
		descTxt = (TextView) findViewById(R.id.descTV);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
		cancelBtn.setOnClickListener(this);

		Log.v("", "QueObj : " + queObj.toString());

		try {
			int correctOption = queObj.getInt("CorrectOption");
			queTxt.setText("" + queObj.getString("Question"));
			queNoTxt.setText(""+(position+1));
			ansTxt.setText("" +queObj.getString("Option" + correctOption));
			descTxt.setText("" + queObj.getString("Description"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == cancelBtn) {

			this.dismiss();
		}
	}

	
}
