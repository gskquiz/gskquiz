package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.invitefriend.InviteFriendFragment;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class ChallengeFriendListFragment extends Fragment implements
OnItemClickListener, OnClickListener {

	private JSONArray friendList;
	ListView list_friends;
	private static ChallengeFriendAdapter adapter;
	private Button myFriendsBtn, invitesBtn, invitenowBtn;
	private TextView invitetitleTV;
	Context context;
	private String quizId;

	public ChallengeFriendListFragment(JSONArray _friendList, Context context) {
		this.context = context;
		this.friendList = _friendList;

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.challengefriendlist_fragment,
				container, false);

		list_friends = (ListView) view.findViewById(R.id.list_friends);
		invitenowBtn = (Button) view.findViewById(R.id.invitenowBtn);
		invitetitleTV = (TextView) view.findViewById(R.id.notInvitedTitle);
		invitenowBtn.setOnClickListener(this);

		if (friendList.length() > 0) {
			adapter = new ChallengeFriendAdapter(getActivity(), friendList);
			list_friends.setAdapter(adapter);
			list_friends.setOnItemClickListener(this);
		} else {
			list_friends.setVisibility(View.GONE);
			invitetitleTV.setVisibility(View.VISIBLE);
			invitenowBtn.setVisibility(View.VISIBLE);
		}

		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (friendList.length() > 0) {
			adapter = new ChallengeFriendAdapter(getActivity(), friendList);
			list_friends.setAdapter(adapter);
			list_friends.setOnItemClickListener(this);
		} else {
			list_friends.setVisibility(View.GONE);
			invitetitleTV.setVisibility(View.VISIBLE);
			invitenowBtn.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

		try {
			String friendId = friendList.getJSONObject(arg2).getString("Id");

			this.loadSelfQuiz(this.quizId, friendId);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected void loadSelfQuiz(String quizID, final String friendID) {
		// TODO Auto-generated method stub

		final String QuizId = quizID;

		new ApplicationAsk(getActivity(), "Fetching questions", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				

				try {

					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataArray = dashboardResponseObj
								.getJSONArray("data");

						QuestionListFragment fragment = new QuestionListFragment(
								dataArray);
						fragment.setQuizId(ChallengeFriendListFragment.this.quizId);
						fragment.setFriendId(friendID);
						fragment.setChallengeFriend(true);

						FragmentManager fragmentManager = getActivity()
								.getFragmentManager();

						FragmentTransaction ft = fragmentManager
								.beginTransaction();

						ft.addToBackStack("QuestionListFragment");
						ft.replace(R.id.content_frame, fragment,
								"QuestionListFragment");

						ft.commit();
					} else {
						Toast.makeText(
								ChallengeFriendListFragment.this.getActivity(),
								"" + dashboardResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject subTopicReq = new JSONObject();
				try {
					subTopicReq.put("QuizId", QuizId);
					subTopicReq.put("FriendId", friendID);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String subtopicURL = URLGenerator
						.getQuestionByQuizChallengeNewUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(subtopicURL,
						subTopicReq.toString(), CacheMemory
						.getToken(ChallengeFriendListFragment.this
								.getActivity()) , ChallengeFriendListFragment.this
								.getActivity());// preferences.getString("Token"));

			}
		}).execute();

	}

	public String getQuizId() {
		return quizId;
	}

	public void setQuizId(String quizId) {
		this.quizId = quizId;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == invitenowBtn) {
			Fragment fragment = new InviteFriendFragment();
			FragmentTransaction ft = getFragmentManager().beginTransaction();

			ft.addToBackStack("InviteFriendFragment");
			ft.replace(R.id.content_frame, fragment, "InviteFriendFragment");

			ft.commit();
		}
	}

}
