package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.o2m.gsk.R;

public class QueAnsListFragment extends Fragment implements OnItemClickListener {

	private ListView topicList;
	private JSONArray queanslistJsonArray;
	protected String TAG;
	
	 public QueAnsListFragment(JSONArray _topiclistJsonArray) {
         // Empty constructor required for fragment subclasses
		 this.queanslistJsonArray = _topiclistJsonArray;
     }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.que_ans_list_fragment, container, false);
		topicList = (ListView) rootView.findViewById(R.id.queanslist);
		QueAnsItemAdapter topicAdapter = new QueAnsItemAdapter(this.getActivity(), queanslistJsonArray);
		topicList.setAdapter(topicAdapter);
		topicList.setOnItemClickListener(this);
		
		
		return rootView;
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		// TODO Auto-generated method stub
		
		try {
			
			AnswerDetailsDialog ansDetails = new AnswerDetailsDialog(QueAnsListFragment.this.getActivity(), queanslistJsonArray.getJSONObject(position) ,position);
			ansDetails.show();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
