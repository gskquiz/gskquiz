package com.o2m.gsk.challenge;

import org.json.JSONArray;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.CacheMemory;

public class SelfQuizWinnerlFragment extends Fragment implements
		OnClickListener {

	private Button viewAnsBtn, playagainBtn;
	private ImageView imageView;
	private TextView quizNameTV, pointsTV, statusTV, correctAnsTV , msgTV;
	private String quizname, points, quizId;
	private int correctAns, totalquestions;
	protected String TAG;
	private JSONArray queAnsArray = new JSONArray();
	private boolean isWon = false;

	public SelfQuizWinnerlFragment(String _quizId, String _quizname, String _points,
			int _correctAns, int _totalquestions) {
		// TODO Auto-generated constructor stub
		this.quizId = _quizId;
		this.quizname = _quizname;
		this.points = _points;
		this.correctAns = _correctAns;
		this.totalquestions = _totalquestions;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.selfquizwinner_fragment,
				container, false);
		viewAnsBtn = (Button) rootView.findViewById(R.id.winnerviewanswerBtn);
		playagainBtn = (Button) rootView.findViewById(R.id.winnerplayagainBtn);

		quizNameTV = (TextView) rootView.findViewById(R.id.quizName);
		pointsTV = (TextView) rootView.findViewById(R.id.pointswonTv);
		correctAnsTV = (TextView) rootView.findViewById(R.id.totalanswersTv);
		statusTV = (TextView) rootView.findViewById(R.id.quizstatusTv);
		msgTV = (TextView) rootView.findViewById(R.id.msgTv);
		imageView = (ImageView) rootView.findViewById(R.id.imageView);
		
		viewAnsBtn.setOnClickListener(this);
		playagainBtn.setOnClickListener(this);
		
		quizNameTV.setText(quizname);
		/*correctAns = 8;
		totalquestions = 10;*/
		correctAnsTV.setText(""+correctAns+"/"+totalquestions);
		
		//You qualified for next level
		if((correctAns*100)/totalquestions >= 80)
		{
			isWon = true;
			msgTV.setText("You qualified for next level");
			statusTV.setText("YOU WON");
			pointsTV.setText("Points Won : " + points);
			imageView.setImageResource(R.drawable.trophy);
			playagainBtn.setText("Play Next");
		}
		else{
			msgTV.setText("That was not quite enough score to win. You should answer atleast 8 out of 10 correctly to qualify for next round");
			statusTV.setText("Oopz!");
			pointsTV.setText("Your Points : " + points);
			imageView.setImageResource(R.drawable.loseremoticon);
			playagainBtn.setText("Play Again");
		}
			
		CacheMemory.setOnlineChallengeStatus(
				SelfQuizWinnerlFragment.this.getActivity(),
				CacheMemory.ONLINE_CHALLENGE_NO);
		return rootView;
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == playagainBtn) {
			// loadLevels(0);
			
			/*Intent home = new Intent(SelfQuizWinnerlFragment.this.getActivity() , GSKHomeActivity.class);
			SelfQuizWinnerlFragment.this.getActivity().startActivity(home);*/
			if(isWon)
			{	
				removeCurrentFragment();
				getFragmentManager()
				.popBackStack();
			}
			else
			{
			
			
				QuestionListFragment fragment = new QuestionListFragment(queAnsArray);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			fragment.setQuizId(this.quizId);
			//ft.addToBackStack("QuestionListFragment");
			ft.replace(R.id.content_frame, fragment, "QuestionListFragment");

			ft.commit();
			}
			
			
		} 
		else if (arg0 == viewAnsBtn)
		{
			Fragment fragment = new QueAnsListFragment(queAnsArray);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
	
			ft.addToBackStack("QueAnsListFragment");
			ft.replace(R.id.content_frame, fragment, "QueAnsListFragment");

			ft.commit();
		}

	}
	
	public void removeCurrentFragment() {
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		Fragment currentFrag = getFragmentManager().findFragmentById(
				R.id.content_frame);

		if (currentFrag != null)
			transaction.remove(currentFrag);

		transaction.commit();

	}

	public JSONArray getQueAnsArray() {
		return queAnsArray;
	}

	public void setQueAnsArray(JSONArray queAnsArray) {
		this.queAnsArray = queAnsArray;
	}


}
