package com.o2m.gsk.challenge;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.AssessmentType;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class QueListFragment extends Fragment implements OnItemClickListener,
		OnClickListener {

	private String TAG = "QueListFragment";
	private ListView optionListView;
	private TextView questionTxtView, questionNoTxtView, timerTxtView;
	private Button nextBtn;
	private JSONObject assessmentObj, outerAssessmentObj;
	private boolean isChallengeFriend = false;
	private String friendId;

	private JSONObject ePollSubmitObj = new JSONObject();
	private JSONArray assessmentDataArray = new JSONArray();

	// private JSONArray assessmentAnsArray = new JSONArray();

	int totalQuestions, currentQueNo = 0, currentCorrectOption = 0,
			correctAns = 0, score = 0, currentpoint = 0;
	double percentage = 0.0;
	ArrayAdapter<String> arrayAdapter;
	ArrayList<String> optionList;
	boolean showAns;
	String asstId;
	MyCountDownTimer myCountDownTimer;
	AssessmentType assessmentType;

	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	private HashMap<String, String> answersHashMap = new HashMap<String, String>();
	private JSONArray assessmentQuestionList;
	private String quizId;

	public QueListFragment(JSONArray questionArray) {
		assessmentQuestionList = questionArray;
		totalQuestions = assessmentQuestionList.length();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_assessment_que,
				container, false);
		optionListView = (ListView) rootView
				.findViewById(R.id.assessmentOptionList);
		questionTxtView = (TextView) rootView
				.findViewById(R.id.assessmentQuestion);
		questionNoTxtView = (TextView) rootView.findViewById(R.id.questionNo);
		timerTxtView = (TextView) rootView.findViewById(R.id.timeLeft);
		nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
		nextBtn.setOnClickListener(this);
		TextView assessmentNameTxt = (TextView) rootView
				.findViewById(R.id.assessmentName);
		optionListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		int time_interval = Integer.parseInt("10");
		myCountDownTimer = new MyCountDownTimer(time_interval * 60 * 1000, 1000);
		myCountDownTimer.start();
		setValuesfor(0);

		return rootView;
	}

	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {

		Log.v("AssessmentQueListFragment", "position" + position);

		// Toast.makeText(this.getActivity(), ""+position,
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onClick(View v) {

		currentQueNo++;

		if (optionListView.getCheckedItemPosition() == ListView.INVALID_POSITION) {
			Toast.makeText(this.getActivity(),
					" Please select answer then next ", Toast.LENGTH_SHORT)
					.show();
			return;
		} else {
			this.captureAns();
		}

		if (currentQueNo < totalQuestions) {
			setValuesfor(currentQueNo);
		} else {
			Toast.makeText(this.getActivity(), " Quiz finished Score " + score,
					Toast.LENGTH_SHORT).show();

			percentage = (correctAns * 100.0) / totalQuestions;
			Log.v(TAG, "" + percentage);
			
						submiteQuiz(getQuizId(), String.valueOf(score),
					String.valueOf(currentpoint));
							
			

		}
	}

	private void submiteQuiz(final String quizId, final String score,
			final String points) {
		// TODO Auto-generated method stub
		new ApplicationAsk(getActivity(), "Saving quiz", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				Log.v("", " onPostExecute Subtopic Response : " + resp);

				try {

					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {
						if (isChallengeFriend) {
							ChallengeQuizCompleteFragment fragment = new ChallengeQuizCompleteFragment(
									"Level",
									String.valueOf(QueListFragment.this.score),
									QueListFragment.this.correctAns,
									QueListFragment.this.totalQuestions);
							fragment.setQueAnsArray(assessmentQuestionList);
							FragmentManager fragmentManager = getActivity()
									.getFragmentManager();
							FragmentTransaction ft = fragmentManager
									.beginTransaction();

							ft.addToBackStack("ChallengeQuizCompleteFragment");
							ft.replace(R.id.content_frame, fragment,
									"ChallengeQuizCompleteFragment");
							ft.commit();
						} else {
							SelfQuizWinnerlFragment fragment = new SelfQuizWinnerlFragment(getQuizId(),
									"Level",
									String.valueOf(QueListFragment.this.score),
									QueListFragment.this.correctAns,
									QueListFragment.this.totalQuestions);
							fragment.setQueAnsArray(assessmentQuestionList);
							FragmentManager fragmentManager = getActivity()
									.getFragmentManager();
							FragmentTransaction ft = fragmentManager
									.beginTransaction();

							ft.addToBackStack("SelfQuizWinnerlFragment");
							ft.replace(R.id.content_frame, fragment,
									"SelfQuizWinnerlFragment");
							ft.commit();
						}
					} else {
						Toast.makeText(getActivity(),
								"" + dashboardResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject SaveSelfQuizReq = new JSONObject();
				try {
					SaveSelfQuizReq.put("UserId", CacheMemory
							.getUserId(QueListFragment.this.getActivity()));
					SaveSelfQuizReq.put("QuizId", quizId);
					SaveSelfQuizReq.put("Score", score);
					SaveSelfQuizReq.put("Points", points);
					if (QueListFragment.this.isChallengeFriend)
						SaveSelfQuizReq.put("FriendId",
								QueListFragment.this.getFriendId());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String subtopicURL = URLGenerator.getSaveSelfQuizUrl();
				if (QueListFragment.this.isChallengeFriend)
					subtopicURL = URLGenerator.getSaveAnswersChallengerUrl();
				CommunicationService commService = new CommunicationService();
				
				if(( correctAns * 100) / totalQuestions >= 80)
				{
					resp = commService.sendSyncRequestWithToken(subtopicURL,
						SaveSelfQuizReq.toString(), CacheMemory
								.getToken(QueListFragment.this.getActivity()) , QueListFragment.this.getActivity() );// preferences.getString("Token"));
				}
				else {
					resp = "{\"status\":\"success\", \"message\":\"\", \"data\" :[]}";
				}
				// return resp;

			}
		}).execute();

	}

	private void setValuesfor(int questionNo) {
		// Toast.makeText(this.getActivity(), "Question No : "+(questionNo+1),
		// Toast.LENGTH_SHORT).show();
		questionNoTxtView.setText("Question : " + (questionNo + 1) + "/"
				+ totalQuestions);
		try {
			if (questionNo != 0) {
				/*
				 * String selectedAns =
				 * optionList.get(optionListView.getCheckedItemPosition());
				 * answersHashMap.put((String) questionTxtView.getText(),
				 * selectedAns);
				 * 
				 * JSONObject ansObj = new JSONObject();
				 * ansObj.put("quest_id",""+questionTxtView.getTag());
				 * ansObj.put("option",
				 * (optionListView.getCheckedItemPosition()+1));
				 * ePollDataArray.put(ansObj);
				 */

			}
			Log.v("EPollListFragment", "" + answersHashMap);

			JSONObject questionJsonObj;

			questionJsonObj = assessmentQuestionList.getJSONObject(questionNo);
			String question = questionJsonObj.getString("Question");
			String questionId = questionJsonObj.getString("Id");

			String opt1 = questionJsonObj.getString("Option1");
			String opt2 = questionJsonObj.getString("Option2");
			String opt3 = questionJsonObj.getString("Option3");
			String opt4 = questionJsonObj.getString("Option4");
			// String opt5 = questionJsonObj.getString("option5");
			// String opt6 = questionJsonObj.getString("opt6");

			currentCorrectOption = questionJsonObj.getInt("CorrectOption");
			currentpoint = questionJsonObj.getInt("Points");
			optionList = new ArrayList<String>();
			if (!opt1.equals(""))
				optionList.add(opt1);
			if (!opt2.equals(""))
				optionList.add(opt2);
			if (!opt3.equals(""))
				optionList.add(opt3);
			if (!opt4.equals(""))
				optionList.add(opt4);
			/*
			 * if(!opt5.equals("")) optionList.add(opt5);
			 */
			/*
			 * if(!opt6.equals("")) optionList.add(opt6);
			 */

			// questionTxtView.setText(""+(questionNo+1)+")  "+question);
			questionTxtView.setText("" + question);
			questionTxtView.setTag(questionId);

			// arrayAdapter = new ArrayAdapter(getActivity(),
			// android.R.layout.simple_list_item_checked, optionList);
			arrayAdapter = new ArrayAdapter(getActivity(), R.layout.epoll_item,
					optionList);

			optionListView.setAdapter(arrayAdapter);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void captureAns() {
		String selectedAns = optionList.get(optionListView
				.getCheckedItemPosition());
		// answersHashMap.put((String) questionTxtView.getText(), selectedAns);
		int currentSelection = (optionListView.getCheckedItemPosition() + 1);

		// JSONObject ansObj = new JSONObject();
		// try {
		// int currentSelection = (optionListView.getCheckedItemPosition()+1);
		// ansObj.put("QuestionID",""+questionTxtView.getTag());
		// ansObj.put("selected_option", currentSelection);
		// ansObj.put("CorrectAnswer", currentCorrectOption);

		if (currentSelection == currentCorrectOption) {
			correctAns++;
			score = score + currentpoint;
		}

		// assessmentDataArray.put(ansObj);
		// } catch (JSONException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

	public class MyCountDownTimer extends CountDownTimer {
		long min, sec;

		public MyCountDownTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			new AlertDialog.Builder(getActivity())
					.setTitle("Assessment")
					.setMessage("Times Up")
					.setPositiveButton("Back To Assessment",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

									QueListFragment.this.getFragmentManager()
											.popBackStack();

								}
							}).setIcon(android.R.drawable.ic_dialog_alert)
					.show();

		}

		@Override
		public void onTick(long milliUntilFinished) {

			long totalSecs = milliUntilFinished / 1000;

			min = totalSecs / 60;
			sec = totalSecs % 60;

			timerTxtView.setText("Time Remaining : " + min + ":" + sec);

		}

		public long getMin() {
			return min;
		}

		public long getSec() {
			return sec;
		}

	}

	public String getQuizId() {
		return quizId;
	}

	public void setQuizId(String quizId) {
		this.quizId = quizId;
	}

	public boolean isChallengeFriend() {
		return isChallengeFriend;
	}

	public void setChallengeFriend(boolean isChallengeFriend) {
		this.isChallengeFriend = isChallengeFriend;
	}

	public String getFriendId() {
		return friendId;
	}

	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}

}
