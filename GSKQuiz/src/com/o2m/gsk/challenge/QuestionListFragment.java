package com.o2m.gsk.challenge;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.AssessmentType;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class QuestionListFragment extends Fragment implements
		OnItemClickListener, OnClickListener {

	private String TAG = "QuestionListFragment";
	private ListView optionListView;
	private ImageView queImgView;
	private VideoView videoView;
	private TextView questionTxtView, timerTxtView, scoreTxtView;
	private TextView oneTxtView, twoTxtView, threeTxtView, fourTxtView,
			fiveTxtView, sixTxtView, sevenTxtView, eightTxtView, nineTxtView,
			tenTxtView;
	private Button nextBtn, closeBtn;
	private JSONObject assessmentObj, outerAssessmentObj;
	private boolean isChallengeFriend = false, isChallengee = false;
	private String friendId;
	private SelectionLockTimer selectionLockTimer;

	private JSONObject ePollSubmitObj = new JSONObject();
	private JSONArray assessmentDataArray = new JSONArray();

	// private JSONArray assessmentAnsArray = new JSONArray();

	int totalQuestions, currentQueNo = 0, currentCorrectOption = 0,
			correctAns = 0, score = 0, currentpoint = 0,
			friendPointperQuest = 0, currentPosition = 0;
	double percentage = 0.0;
	AnswerAdapter arrayAdapter;
	ArrayList<String> optionList;
	boolean showAns;
	String asstId;
	MyCountDownTimer myCountDownTimer;
	AssessmentType assessmentType;
	private ProgressBar progressBar;

	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	private HashMap<String, String> answersHashMap = new HashMap<String, String>();
	private JSONArray assessmentQuestionList;
	private String quizId;

	// private Activity activity;

	public QuestionListFragment(JSONArray questionArray) {
		assessmentQuestionList = questionArray;
		// this.activity = _activity;
		totalQuestions = assessmentQuestionList.length();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.que_list_fragment, container,
				false);
		optionListView = (ListView) rootView
				.findViewById(R.id.assessmentOptionList);
		questionTxtView = (TextView) rootView
				.findViewById(R.id.assessmentQuestion);
		scoreTxtView = (TextView) rootView.findViewById(R.id.scoreTxt);
		// questionNoTxtView = (TextView)
		// rootView.findViewById(R.id.questionNo);
		timerTxtView = (TextView) rootView.findViewById(R.id.timeLeft);

		oneTxtView = (TextView) rootView.findViewById(R.id.oneTxt);
		twoTxtView = (TextView) rootView.findViewById(R.id.twoTxt);
		threeTxtView = (TextView) rootView.findViewById(R.id.threeTxt);
		fourTxtView = (TextView) rootView.findViewById(R.id.fourTxt);
		fiveTxtView = (TextView) rootView.findViewById(R.id.fiveTxt);
		sixTxtView = (TextView) rootView.findViewById(R.id.sixTxt);
		sevenTxtView = (TextView) rootView.findViewById(R.id.sevenTxt);
		eightTxtView = (TextView) rootView.findViewById(R.id.eightTxt);
		nineTxtView = (TextView) rootView.findViewById(R.id.nineTxt);
		tenTxtView = (TextView) rootView.findViewById(R.id.tenTxt);
		queImgView = (ImageView) rootView.findViewById(R.id.questionImageView);
		videoView = (VideoView) rootView.findViewById(R.id.VideoView);
		/*
		 * nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
		 * nextBtn.setOnClickListener(this);
		 */
		progressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
		TextView assessmentNameTxt = (TextView) rootView
				.findViewById(R.id.assessmentName);
		optionListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		optionListView.setOnItemClickListener(this);

		closeBtn = (Button) rootView.findViewById(R.id.closeBtn);
		closeBtn.setOnClickListener(this);

		setValuesfor(0);

		return rootView;
	}

	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {

		Log.v(TAG, "position : " + position);
		currentPosition = position;
		if (myCountDownTimer != null)
			myCountDownTimer.cancel();
		// optionListView.getChildAt(position);
		arrayAdapter = new AnswerAdapter(CacheMemory.mActivity,
				R.layout.quiz_ans_item, optionList);
		arrayAdapter.setSelectedPosition(position);
		arrayAdapter.setCurrentCorrectOption(currentCorrectOption - 1);
		optionListView.setAdapter(arrayAdapter);
		optionListView.setOnItemClickListener(null);

		if (selectionLockTimer != null)
			selectionLockTimer.cancel();
		selectionLockTimer = new SelectionLockTimer(position, 1000, 1000);
		selectionLockTimer.start();

		/*
		 * try { Thread.sleep(2000); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

	}

	public void onNext(int position) {

		currentQueNo++;

		/*
		 * if (optionListView.getCheckedItemPosition() ==
		 * ListView.INVALID_POSITION) { Toast.makeText(this.getActivity(),
		 * " Please select answer then next ", Toast.LENGTH_SHORT) .show();
		 * return; } else {
		 */
		this.captureAns();
		// }

		if (currentQueNo < totalQuestions) {
			setValuesfor(currentQueNo);
		} else {
			Toast.makeText(this.getActivity(), " Quiz finished Score " + score,
					Toast.LENGTH_SHORT).show();

			percentage = (correctAns * 100.0) / totalQuestions;
			Log.v(TAG, "" + percentage);

			submiteQuiz(getQuizId(), String.valueOf(score),
					String.valueOf(currentpoint));

		}
	}

	private void submiteQuiz(final String quizId, final String score,
			final String point) {
		// String points = "";
		// TODO Auto-generated method stub
		new ApplicationAsk(getActivity(), "Saving quiz",
				new ApplicationService() {
					private String resp;

					@Override
					public void postExecute() {
						Log.v("", " onPostExecute Subtopic Response : " + resp);

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {
								if (QuestionListFragment.this.isChallengeFriend) {
									ChallengeQuizCompleteFragment fragment = new ChallengeQuizCompleteFragment(
											""
													+ CacheMemory
															.getLevelName(QuestionListFragment.this
																	.getActivity()),
											String.valueOf(QuestionListFragment.this.score),
											QuestionListFragment.this.correctAns,
											QuestionListFragment.this.totalQuestions);
									fragment.setQueAnsArray(assessmentQuestionList);
									FragmentManager fragmentManager = getActivity()
											.getFragmentManager();
									FragmentTransaction ft = fragmentManager
											.beginTransaction();

									// ft.addToBackStack("ChallengeQuizCompleteFragment");
									ft.replace(R.id.content_frame, fragment,
											"ChallengeQuizCompleteFragment");
									ft.commit();
								} else {
									SelfQuizWinnerlFragment fragment = new SelfQuizWinnerlFragment(
											getQuizId(), ""
													+ CacheMemory
															.getLevelName(QuestionListFragment.this
																	.getActivity()),
											String.valueOf(QuestionListFragment.this.score),
											QuestionListFragment.this.correctAns,
											QuestionListFragment.this.totalQuestions);
									fragment.setQueAnsArray(assessmentQuestionList);
									FragmentManager fragmentManager = getActivity()
											.getFragmentManager();
									FragmentTransaction ft = fragmentManager
											.beginTransaction();

									// ft.addToBackStack("SelfQuizWinnerlFragment");
									ft.replace(R.id.content_frame, fragment,
											"SelfQuizWinnerlFragment");
									ft.commit();
								}

							} else {
								Toast.makeText(
										getActivity(),
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {

						String points = "";
						if (QuestionListFragment.this.isChallengeFriend
								&& !QuestionListFragment.this.isChallengee)
							points = String.valueOf(friendPointperQuest);
						else
							points = String.valueOf(currentpoint);

						JSONObject SaveSelfQuizReq = new JSONObject();
						try {
							SaveSelfQuizReq.put("UserId", CacheMemory
									.getUserId(CacheMemory.mActivity));
							SaveSelfQuizReq.put("QuizId", quizId);
							SaveSelfQuizReq.put("Score", score);
							SaveSelfQuizReq.put("Points", points);
							if (QuestionListFragment.this.isChallengeFriend)
								SaveSelfQuizReq
										.put("FriendId",
												QuestionListFragment.this
														.getFriendId());

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						String subtopicURL = URLGenerator.getSaveSelfQuizUrl();

						if (QuestionListFragment.this.isChallengeFriend)
							subtopicURL = URLGenerator
									.getSaveAnswersChallengerUrl();

						if (QuestionListFragment.this.isChallengee)
							subtopicURL = URLGenerator
									.getSaveAnswersChallengeeUrl();

						CommunicationService commService = new CommunicationService();

						if (QuestionListFragment.this.isChallengeFriend) {
							resp = commService.sendSyncRequestWithToken(
									subtopicURL, SaveSelfQuizReq.toString(),
									CacheMemory
											.getToken(QuestionListFragment.this
													.getActivity()), QuestionListFragment.this
													.getActivity());
						} else if (((correctAns * 100) / totalQuestions >= 80)) {
							resp = commService.sendSyncRequestWithToken(
									subtopicURL, SaveSelfQuizReq.toString(),
									CacheMemory
											.getToken(QuestionListFragment.this
													.getActivity()), QuestionListFragment.this
													.getActivity());
						} else {
							resp = "{\"status\":\"success\", \"message\":\"\", \"data\" :[]}";
						}
						// return resp;

					}
				}).execute();

	}

	private void setValuesfor(int questionNo) {

		/*
		 * questionNoTxtView.setText("Question : " + (questionNo + 1) + "/" +
		 * totalQuestions);
		 */
		queImgView.setVisibility(View.GONE);
		videoView.setVisibility(View.GONE);
		progressBar.setVisibility(View.GONE);
		scoreTxtView.setText("Score : " + this.score);
		try {
			if (questionNo != 0) {

			}

			JSONObject questionJsonObj;

			questionJsonObj = assessmentQuestionList.getJSONObject(questionNo);
			String question = questionJsonObj.getString("Question");
			String questionId = questionJsonObj.getString("Id");

			String opt1 = questionJsonObj.getString("Option1");
			String opt2 = questionJsonObj.getString("Option2");
			String opt3 = questionJsonObj.getString("Option3");
			String opt4 = questionJsonObj.getString("Option4");

			String contentType = questionJsonObj.getString("ContentType");
			String base64Data = questionJsonObj.getString("Base64Data");

			final int time_interval = questionJsonObj.getInt("QuestionTime") + 1;
			// timerTxtView.setText(""+time_interval);

			if (contentType.equalsIgnoreCase("image/png") || contentType.equalsIgnoreCase("image/jpeg") ) {
				questionTxtView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
				queImgView.setVisibility(View.VISIBLE);
				/*new DownloadImageTask(queImgView)
						.execute("http://gskquizadmin.oh22works.com/Media/"
								+ base64Data);*/
				
				new DownloadImageTask(queImgView)
				.execute(URLGenerator.DOMAIN +"/admin/Media/"
						+ base64Data);
			} else if (contentType.equalsIgnoreCase("video/mp4")) {
				questionTxtView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
				videoView.setVisibility(View.VISIBLE);
				progressBar.setVisibility(View.VISIBLE);
				try {
					// Start the MediaController
					MediaController mediacontroller = new MediaController(
							CacheMemory.mActivity);
					mediacontroller.setAnchorView(videoView);

					/*Uri video = Uri
							.parse("http://gskquizadmin.oh22works.com/Media/"
									+ base64Data);*/
					Uri video = Uri
							.parse(URLGenerator.DOMAIN + "/admin/Media/"
									+ base64Data);

					videoView.setVideoURI(video);

				} catch (Exception e) {
					Log.e("Error", e.getMessage());
					e.printStackTrace();
				}

				videoView.requestFocus();
				progressBar.setVisibility(View.VISIBLE);
				videoView.setOnPreparedListener(new OnPreparedListener() {
					// Close the progress bar and play the video
					public void onPrepared(MediaPlayer mp) {

						videoView.start();

						mp.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {
							@Override
							public void onVideoSizeChanged(MediaPlayer mp,
									int arg1, int arg2) {
								// TODO Auto-generated method stub
								progressBar.setVisibility(View.GONE);
								mp.start();
							}
						});

					}

				});

				videoView
						.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

							@Override
							public void onCompletion(MediaPlayer mp) {
								// Start Timer

								if (myCountDownTimer != null)
									myCountDownTimer.cancel();
								myCountDownTimer = new MyCountDownTimer(
										time_interval * 1000, 1000);
								myCountDownTimer.start();

							}
						});

			} else if (contentType.equalsIgnoreCase("")) {
				questionTxtView.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
				if (myCountDownTimer != null)
					myCountDownTimer.cancel();
				myCountDownTimer = new MyCountDownTimer(time_interval * 1000,
						1000);
				myCountDownTimer.start();
			}

			currentCorrectOption = questionJsonObj.getInt("CorrectOption");

			currentpoint = questionJsonObj.getInt("Points");
			if (this.isChallengeFriend)
				friendPointperQuest = questionJsonObj
						.getInt("friendPointperQuest");
			optionList = new ArrayList<String>();
			if (!opt1.equals(""))
				optionList.add(opt1);
			if (!opt2.equals(""))
				optionList.add(opt2);
			if (!opt3.equals(""))
				optionList.add(opt3);
			if (!opt4.equals(""))
				optionList.add(opt4);
			questionTxtView.setText("" + question);
			questionTxtView.setTag(questionId);

			Log.v("", "optionList : " + optionList);
			Log.v("", "this.getActivity() : " + this.getActivity());
			Log.v("", "R.layout.epoll_item : " + R.layout.epoll_item);

			// arrayAdapter = new ArrayAdapter(CacheMemory.mActivity ,
			// R.layout.epoll_item,
			// optionList);
			arrayAdapter = new AnswerAdapter(CacheMemory.mActivity,
					R.layout.quiz_ans_item, optionList);
		/*	Debug for correct answer
			arrayAdapter.setCurrentCorrectOption(currentCorrectOption - 1);*/
			optionListView.setAdapter(arrayAdapter);

			switch (questionNo + 1) {
			case 1:
				oneTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 2:
				oneTxtView.setBackgroundResource(R.drawable.green_circle);
				twoTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 3:
				oneTxtView.setBackgroundResource(R.drawable.green_circle);
				twoTxtView.setBackgroundResource(R.drawable.green_circle);
				threeTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 4:
				oneTxtView.setBackgroundResource(R.drawable.green_circle);
				twoTxtView.setBackgroundResource(R.drawable.green_circle);
				threeTxtView.setBackgroundResource(R.drawable.green_circle);
				fourTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 5:
				oneTxtView.setBackgroundResource(R.drawable.green_circle);
				twoTxtView.setBackgroundResource(R.drawable.green_circle);
				threeTxtView.setBackgroundResource(R.drawable.green_circle);
				fourTxtView.setBackgroundResource(R.drawable.green_circle);
				fiveTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 6:

				fiveTxtView.setBackgroundResource(R.drawable.green_circle);
				sixTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 7:
				sixTxtView.setBackgroundResource(R.drawable.green_circle);
				sevenTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 8:
				sevenTxtView.setBackgroundResource(R.drawable.green_circle);
				eightTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 9:
				eightTxtView.setBackgroundResource(R.drawable.green_circle);
				nineTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 10:
				nineTxtView.setBackgroundResource(R.drawable.green_circle);
				tenTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;

			default:
				break;
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	private void captureAns() {
		/*
		 * String selectedAns = optionList.get(optionListView
		 * .getCheckedItemPosition());
		 * 
		 * int currentSelection = (optionListView.getCheckedItemPosition() + 1);
		 */
		int currentSelection = currentPosition + 1;
		if (currentSelection == currentCorrectOption) {
			correctAns++;
			score = score + currentpoint;
		}
	}

	public class MyCountDownTimer extends CountDownTimer {
		long min, sec;

		public MyCountDownTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			currentQueNo++;
			if (currentQueNo < totalQuestions) {
				setValuesfor(currentQueNo);
			} else {
				Toast.makeText(CacheMemory.mActivity,
						" Quiz finished Score " + score, Toast.LENGTH_SHORT)
						.show();

				percentage = (correctAns * 100.0) / totalQuestions;
				Log.v(TAG, "" + percentage);

				submiteQuiz(getQuizId(), String.valueOf(score),
						String.valueOf(currentpoint));

			}
		}

		@Override
		public void onTick(long milliUntilFinished) {

			long totalSecs = milliUntilFinished / 1000;

			min = totalSecs / 60;
			sec = totalSecs % 60;

			// timerTxtView.setText("Time Remaining : " + min + ":" + sec);
			timerTxtView.setText("" + totalSecs);

		}

		public long getMin() {
			return min;
		}

		public long getSec() {
			return sec;
		}

	}

	public class SelectionLockTimer extends CountDownTimer {
		int position;

		public SelectionLockTimer(int position, long millisInFuture,
				long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
			this.position = position;
		}

		@Override
		public void onFinish() {

			Log.v("SelectionLockTimer", "onFinish : this.position "
					+ this.position);
			arrayAdapter.setSelectedPosition(-1);
			optionListView.setOnItemClickListener(QuestionListFragment.this);
			QuestionListFragment.this.onNext(this.position);

		}

		@Override
		public void onTick(long milliUntilFinished) {

		}

	}

	public String getQuizId() {
		return quizId;
	}

	public void setQuizId(String quizId) {
		this.quizId = quizId;
	}

	public boolean isChallengeFriend() {
		return isChallengeFriend;
	}

	public void setChallengeFriend(boolean isChallengeFriend) {
		this.isChallengeFriend = isChallengeFriend;
	}

	public String getFriendId() {
		return friendId;
	}

	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		Log.v("QuestionListFragment",
				"QuestionListFragment : - Destroy-Destroy-Destroy : myCountDownTimer : "
						+ myCountDownTimer);
	
		if (myCountDownTimer != null) {
			myCountDownTimer.cancel();
			Log.v("QuestionListFragment", "--- TimerCancelled ---");
		}
	}

	public boolean isChallengee() {
		return isChallengee;
	}

	public void setChallengee(boolean isChallengee) {
		this.isChallengee = isChallengee;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v == closeBtn) {

			getActivity().dispatchKeyEvent(
					new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
			getActivity().dispatchKeyEvent(
					new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));

		}
	}

}
