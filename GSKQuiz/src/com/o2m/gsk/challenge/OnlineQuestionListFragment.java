package com.o2m.gsk.challenge;

import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.GSKHomeActivity;
import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.AssessmentType;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class OnlineQuestionListFragment extends Fragment implements
		OnItemClickListener, OnClickListener {

	private String TAG = "OnlineQuestionListFragment";
	private ListView optionListView;
	private TextView questionTxtView, timerTxtView, myScoreTxtView,
			oppoNameTxtView, oScoreTxtView;
	private TextView oneTxtView, twoTxtView, threeTxtView, fourTxtView,
			fiveTxtView, sixTxtView, sevenTxtView, eightTxtView, nineTxtView,
			tenTxtView;
	private TextView oneoppoTxtView, twooppoTxtView, threeoppoTxtView,
			fouroppoTxtView, fiveoppoTxtView, sixoppoTxtView, sevenoppoTxtView,
			eightoppoTxtView, nineoppoTxtView, tenoppoTxtView;
	private Button nextBtn, closeBtn;
	private JSONObject ssObj;

	int totalQuestions, currentQueNo = 0, currentCorrectOption = 0,
			correctAns = 0, score = 0, currentpoint = 0, counter = 0,
			currentPosition = 0;

	int opponentScore, opponenetQueCount;

	double percentage = 0.0;
	OnlineAnswerAdapter arrayAdapter;
	ArrayList<String> optionList, mycorrectAnsList, oppocorrectAnsList;
	boolean showAns, isOpponentAttempted = false, isChallenngee = false;
	String asstId, challengeId = null;
	MyCountDownTimer myCountDownTimer;
	AssessmentType assessmentType;
	MyCountDown10SecTimer myCountDown10SecTimer;
	TimeOutTimer timeOutTimer;

	public void setShowAns(boolean showAns) {
		this.showAns = showAns;
	}

	private JSONArray assessmentQuestionList;
	private String quizId = null, opponentName = null;
	private AlphaAnimation animation;

	public OnlineQuestionListFragment(JSONArray questionArray) {
		assessmentQuestionList = questionArray;
		totalQuestions = assessmentQuestionList.length();
		mycorrectAnsList = new ArrayList<String>();
		oppocorrectAnsList = new ArrayList<String>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.que_online_list_fragment,
				container, false);
		optionListView = (ListView) rootView
				.findViewById(R.id.assessmentOptionList);
		questionTxtView = (TextView) rootView
				.findViewById(R.id.assessmentQuestion);

		timerTxtView = (TextView) rootView.findViewById(R.id.timeLeft);

		oneTxtView = (TextView) rootView.findViewById(R.id.oneTxt);
		twoTxtView = (TextView) rootView.findViewById(R.id.twoTxt);
		threeTxtView = (TextView) rootView.findViewById(R.id.threeTxt);
		fourTxtView = (TextView) rootView.findViewById(R.id.fourTxt);
		fiveTxtView = (TextView) rootView.findViewById(R.id.fiveTxt);
		sixTxtView = (TextView) rootView.findViewById(R.id.sixTxt);
		sevenTxtView = (TextView) rootView.findViewById(R.id.sevenTxt);
		eightTxtView = (TextView) rootView.findViewById(R.id.eightTxt);
		nineTxtView = (TextView) rootView.findViewById(R.id.nineTxt);
		tenTxtView = (TextView) rootView.findViewById(R.id.tenTxt);

		oneoppoTxtView = (TextView) rootView.findViewById(R.id.oppooneTxt);
		twooppoTxtView = (TextView) rootView.findViewById(R.id.oppotwoTxt);
		threeoppoTxtView = (TextView) rootView.findViewById(R.id.oppothreeTxt);
		fouroppoTxtView = (TextView) rootView.findViewById(R.id.oppofourTxt);
		fiveoppoTxtView = (TextView) rootView.findViewById(R.id.oppofiveTxt);
		sixoppoTxtView = (TextView) rootView.findViewById(R.id.opposixTxt);
		sevenoppoTxtView = (TextView) rootView.findViewById(R.id.opposevenTxt);
		eightoppoTxtView = (TextView) rootView.findViewById(R.id.oppoeightTxt);
		nineoppoTxtView = (TextView) rootView.findViewById(R.id.opponineTxt);
		tenoppoTxtView = (TextView) rootView.findViewById(R.id.oppotenTxt);

		myScoreTxtView = (TextView) rootView.findViewById(R.id.myScore);
		oppoNameTxtView = (TextView) rootView.findViewById(R.id.opponentName);
		oScoreTxtView = (TextView) rootView.findViewById(R.id.opponentScore);

		nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
		nextBtn.setOnClickListener(this);

		closeBtn = (Button) rootView.findViewById(R.id.closeBtn);
		closeBtn.setOnClickListener(this);

		nextBtn.setVisibility(View.GONE);

		optionListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		optionListView.setOnItemClickListener(this);

		if (opponentName != null)
			oppoNameTxtView.setText("" + opponentName);

		setValuesfor(0);

		startBlinkFor(null, oneoppoTxtView);
		return rootView;
	}

	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {

		currentPosition = position;

		arrayAdapter = new OnlineAnswerAdapter(CacheMemory.mActivity,
				R.layout.quiz_ans_item, optionList);
		arrayAdapter.setSelectedPosition(position);

		optionListView.setAdapter(arrayAdapter);

		optionListView.setOnItemClickListener(null);

		/*
		 * if (optionListView.getCheckedItemPosition() ==
		 * ListView.INVALID_POSITION) { Toast.makeText(this.getActivity(),
		 * " Please select answer then next ", Toast.LENGTH_SHORT) .show();
		 * return; } else {
		 */
		this.captureAns();
		// }

		currentQueNo++;

		if (currentQueNo < totalQuestions) {
			this.SaveAndGetScoreDetails();
		} else {
			Toast.makeText(this.getActivity(), " Quiz finished Score " + score,
					Toast.LENGTH_SHORT).show();

			percentage = (correctAns * 100.0) / totalQuestions;
			Log.v(TAG, "" + percentage);
			this.SaveAndGetScoreDetails();
		}

	}

	@Override
	public void onClick(View v) {

		if (v == closeBtn) {

			getActivity().dispatchKeyEvent(
					new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK));
			getActivity().dispatchKeyEvent(
					new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK));

		} else if (v == nextBtn) {

			String tag = (String) v.getTag();
			if (tag.equalsIgnoreCase("1")) {
				Toast.makeText(this.getActivity(),
						" Waiting for opponent to attempt ", Toast.LENGTH_SHORT)
						.show();
				return;
			}

			currentQueNo++;

			if (optionListView.getCheckedItemPosition() == ListView.INVALID_POSITION) {
				Toast.makeText(this.getActivity(),
						" Please select answer then next ", Toast.LENGTH_SHORT)
						.show();
				return;
			} else {
				this.captureAns();
			}

			if (currentQueNo < totalQuestions) {
				this.SaveAndGetScoreDetails();
			} else {
				Toast.makeText(this.getActivity(),
						" Quiz finished Score " + score, Toast.LENGTH_SHORT)
						.show();

				percentage = (correctAns * 100.0) / totalQuestions;
				Log.v(TAG, "" + percentage);
				this.SaveAndGetScoreDetails();
			}
		}

	}

	private void SaveAndGetScoreDetails() {
		new ApplicationAsk(getActivity(), "checking", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {

				try {

					JSONObject saveScoreResponseObj = new JSONObject(resp);
					String status = saveScoreResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {
						JSONObject resObj = saveScoreResponseObj.getJSONArray(
								"data").getJSONObject(0);
						int oppoquestcount = resObj.getInt("OppoQuestCount");
						OnlineQuestionListFragment.this.opponenetQueCount = oppoquestcount;
						int myQuestCount = resObj.getInt("ChallQuestCount");

						OnlineQuestionListFragment.this.displayRunningQuestion(
								myQuestCount, oppoquestcount);

						if (challengeId == null) {
							challengeId = resObj.getString("ChallengerId");
						}
						if (quizId == null) {
							quizId = resObj.getString("QuizId");
						}

						myScoreTxtView.setText("Score : "
								+ resObj.getString("MyScore"));
						oppoNameTxtView.setText(""
								+ resObj.getString("OpponentName"));
						oScoreTxtView.setText("Score : "
								+ resObj.getString("OpponentScore"));

						int currentopponentScore = Integer.valueOf(resObj
								.getString("OpponentScore"));
						if (currentopponentScore > OnlineQuestionListFragment.this.opponentScore)
							isOpponentAttempted = true;

						OnlineQuestionListFragment.this.opponentScore = currentopponentScore;
						String OppoAnswer = resObj.getString("OppoAnswer");
						oppocorrectAnsList = new ArrayList<String>(Arrays
								.asList(OppoAnswer.split(",")));

						Log.v("oppocorrectAnsList", "oppocorrectAnsList"
								+ oppocorrectAnsList.toString());
						Log.v("mycorrectAnsList", "mycorrectAnsList"
								+ mycorrectAnsList.toString());

						if (oppoquestcount == myQuestCount
								&& myQuestCount == totalQuestions) {
							ssObj = resObj;
							OnlineQuestionListFragment.this.submitQuiz(
									getQuizId(), String.valueOf(score),
									String.valueOf(currentpoint));
						} else if (oppoquestcount >= myQuestCount) {
							nextBtn.setTag("0");
							nextBtn.setText("NEXT");
							OnlineQuestionListFragment.this
									.setValuesfor(currentQueNo);
						} else {
							// waiting for the opponent
							nextBtn.setTag("1");
							nextBtn.setText("WAITING");

							if (myCountDown10SecTimer != null) {
								myCountDown10SecTimer.cancel();
							}
							myCountDown10SecTimer = new MyCountDown10SecTimer(
									1000 * 10, 1000 * 10);
							myCountDown10SecTimer.start();

						}

					} else {

						Toast.makeText(getActivity(),
								"" + saveScoreResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {

				JSONObject saveScoreReq = new JSONObject();
				try {
					saveScoreReq.put("ConnectionId", CacheMemory
							.getConnectionId(OnlineQuestionListFragment.this
									.getActivity()));
					saveScoreReq.put("MyQuestCount", currentQueNo);
					saveScoreReq.put("MyScore", score);
					saveScoreReq.put("MyAnswer", mycorrectAnsList.toString());

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String saveScoreURL = URLGenerator
						.getSaveAndGetScoreDetailsUrl();
				CommunicationService commService = new CommunicationService();

				resp = commService.sendSyncRequestWithToken(saveScoreURL,
						saveScoreReq.toString(), CacheMemory
								.getToken(OnlineQuestionListFragment.this
										.getActivity()),
						OnlineQuestionListFragment.this.getActivity());// preferences.getString("Token"));

			}
		}).execute();

	}

	private void submitQuiz(final String quizId, final String score,
			final String points) {
		// TODO Auto-generated method stub

		if (timeOutTimer != null)
			timeOutTimer.cancel();

		if (myCountDown10SecTimer != null)
			myCountDown10SecTimer.cancel();

		if (myCountDownTimer != null)
			myCountDownTimer.cancel();

		new ApplicationAsk(getActivity(), "Saving quiz",
				new ApplicationService() {
					private String resp;

					@Override
					public void postExecute() {
						Log.v("", " onPostExecute Subtopic Response : " + resp);

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

								String name = CacheMemory
										.getLevelName(OnlineQuestionListFragment.this
												.getActivity());
								OnlineChallengeWinnerlFragment fragment = new OnlineChallengeWinnerlFragment(
										name, ssObj);
								fragment.setQueAnsArray(assessmentQuestionList);
								FragmentManager fragmentManager = getActivity()
										.getFragmentManager();
								FragmentTransaction ft = fragmentManager
										.beginTransaction();

								// ft.addToBackStack("OnlineChallengeWinnerlFragment");
								ft.replace(R.id.content_frame, fragment,
										"OnlineChallengeWinnerlFragment");
								ft.commit();
							} else {
								Toast.makeText(
										getActivity(),
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {
						JSONObject SaveSelfQuizReq = new JSONObject();
						try {

							SaveSelfQuizReq.put("QuizId", quizId);
							SaveSelfQuizReq.put("Score", score);
							SaveSelfQuizReq.put("ChallengeId", challengeId);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						String subtopicURL = URLGenerator
								.getSaveOnlineQuizResultUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								subtopicURL,
								SaveSelfQuizReq.toString(),
								CacheMemory
										.getToken(OnlineQuestionListFragment.this
												.getActivity()),
								OnlineQuestionListFragment.this.getActivity());
					}
				}).execute();

	}

	private void setValuesfor(int questionNo) {

		questionTxtView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		optionListView.setOnItemClickListener(this);
		try {
			if (questionNo != 0) {

			}

			JSONObject questionJsonObj;

			questionJsonObj = assessmentQuestionList.getJSONObject(questionNo);
			String question = questionJsonObj.getString("Question");
			String questionId = questionJsonObj.getString("Id");

			String opt1 = questionJsonObj.getString("Option1");
			String opt2 = questionJsonObj.getString("Option2");
			String opt3 = questionJsonObj.getString("Option3");
			String opt4 = questionJsonObj.getString("Option4");

			int time_interval = questionJsonObj.getInt("QuestionTime") + 1;
			if (myCountDownTimer != null)
				myCountDownTimer.cancel();
			myCountDownTimer = new MyCountDownTimer(time_interval * 1000, 1000);
			myCountDownTimer.start();

			currentCorrectOption = questionJsonObj.getInt("CorrectOption");
			currentpoint = questionJsonObj.getInt("Points");
			optionList = new ArrayList<String>();
			if (!opt1.equals(""))
				optionList.add(opt1);
			if (!opt2.equals(""))
				optionList.add(opt2);
			if (!opt3.equals(""))
				optionList.add(opt3);
			if (!opt4.equals(""))
				optionList.add(opt4);
			int displayQueNo = (currentQueNo + 1);
			questionTxtView.setText("" + question);
			questionTxtView.setTag(questionId);

			arrayAdapter = new OnlineAnswerAdapter(CacheMemory.mActivity,
					R.layout.quiz_ans_item, optionList);

			optionListView.setAdapter(arrayAdapter);

			switch (displayQueNo) {
			case 1:
				oneTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 2:

				twoTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 3:

				threeTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 4:

				fourTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 5:

				fiveTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 6:

				sixTxtView.setBackgroundResource(R.drawable.orange_circle);
				break;
			case 7:

				sevenTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 8:

				eightTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 9:

				nineTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;
			case 10:

				tenTxtView.setBackgroundResource(R.drawable.orange_circle);

				break;

			default:
				break;
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void captureAns() {
		/*
		 * String selectedAns = optionList.get(optionListView
		 * .getCheckedItemPosition());
		 * 
		 * int currentSelection = (optionListView.getCheckedItemPosition() + 1);
		 */
		int currentSelection = currentPosition + 1;
		;
		Log.v("", "oppocorrectAnsList : " + oppocorrectAnsList
				+ " currentQueNo : " + currentQueNo);

		if (oppocorrectAnsList.size() > (currentQueNo)) {
			String oppoAns = oppocorrectAnsList.get(currentQueNo);
			if (oppoAns.equalsIgnoreCase("1")) {
				currentpoint = 0;
				Log.v("", "Opponent Answered this question");
			} else {
				Log.v("", "Opponent Answered this question wrongly");
			}
		} else {

			Log.v("", "Opponent not attempted question ");
		}
		/*
		 * if (isOpponentAttempted) currentpoint = 0;
		 */

		if (currentSelection == currentCorrectOption) {
			correctAns++;
			score = score + currentpoint;
			mycorrectAnsList.add("1");
		} else {
			mycorrectAnsList.add("0");
		}

		isOpponentAttempted = false;

	}

	public class MyCountDownTimer extends CountDownTimer {
		long min, sec;

		public MyCountDownTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// Start Waiting Timer
			if (timeOutTimer != null)
				timeOutTimer.cancel();

			timeOutTimer = new TimeOutTimer(1000 * 25, 1000 * 25,
					OnlineQuestionListFragment.this.currentQueNo);
			timeOutTimer.start();
		}

		@Override
		public void onTick(long milliUntilFinished) {

			long totalSecs = milliUntilFinished / 1000;

			min = totalSecs / 60;
			sec = totalSecs % 60;

			// timerTxtView.setText("Time Remaining : " + min + ":" + sec);
			timerTxtView.setText("" + totalSecs);

		}

		public long getMin() {
			return min;
		}

		public long getSec() {
			return sec;
		}

	}

	public class MyCountDown10SecTimer extends CountDownTimer {

		public MyCountDown10SecTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
			// OnlineQuestionListFragment.this.counter++;
		}

		@Override
		public void onFinish() {

			// Log.v("", "Counter " + OnlineQuestionListFragment.this.counter);

			OnlineQuestionListFragment.this.SaveAndGetScoreDetails();

		}

		@Override
		public void onTick(long milliUntilFinished) {

		}

	}

	public class TimeOutTimer extends CountDownTimer {

		int questionNo;

		public TimeOutTimer(long millisInFuture, long countDownInterval,
				int _questionNo) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
			questionNo = _questionNo;
		}

		@Override
		public void onFinish() {

			Log.v("", "this.currentQueNo : "
					+ OnlineQuestionListFragment.this.currentQueNo
					+ " questionNo : " + questionNo);

			if (OnlineQuestionListFragment.this.currentQueNo == questionNo) {

				Log.v("", " Quiz Time Out ");

				if (myCountDown10SecTimer != null)
					myCountDown10SecTimer.cancel();

				if (myCountDownTimer != null)
					myCountDownTimer.cancel();

				OnlineQuestionListFragment.this.showAlertAndPopBack();
			}

		}

		@Override
		public void onTick(long milliUntilFinished) {

		}

	}

	public void showAlertAndPopBack() {
		// TODO Auto-generated method stub

		if (getActivity() != null) {

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

			// 2. Chain together various setter methods to set the dialog
			// characteristics
			builder.setMessage("Opponet Left").setTitle("");

			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// User clicked OK button
							CacheMemory.setOnlineChallengeStatus(OnlineQuestionListFragment.this.getActivity(),
									CacheMemory.ONLINE_CHALLENGE_NO);
							if (isChallenngee()) {
								OnlineQuestionListFragment.this.getActivity()
										.getFragmentManager().popBackStack();
							} else {
								OnlineQuestionListFragment.this
										.getActivity()
										.getFragmentManager()
										.popBackStack(
												"MapFragment",
												FragmentManager.POP_BACK_STACK_INCLUSIVE);
							}
						}

					});

			AlertDialog dialog = builder.create();
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	public String getQuizId() {
		return quizId;
	}

	public void setQuizId(String quizId) {
		this.quizId = quizId;
	}

	public String getOpponentName() {
		return opponentName;
	}

	public void setOpponentName(String opponentName) {
		this.opponentName = opponentName;
	}

	public void displayRunningQuestion(int my, int oppo) {

		switch (oppo) {
		case 1:

			oneoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(oneoppoTxtView, twooppoTxtView);

			break;
		case 2:

			oneoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			twooppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(twooppoTxtView, threeoppoTxtView);
			break;
		case 3:

			twooppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			threeoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(threeoppoTxtView, fouroppoTxtView);
			break;
		case 4:

			threeoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			fouroppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(fouroppoTxtView, fiveoppoTxtView);
			break;
		case 5:

			fouroppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			fiveoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(fiveoppoTxtView, sixoppoTxtView);
			break;
		case 6:

			fiveoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			sixoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(sixoppoTxtView, sevenoppoTxtView);
			break;
		case 7:

			sixoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			sevenoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(sevenoppoTxtView, eightoppoTxtView);
			break;
		case 8:

			sevenoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			eightoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(eightoppoTxtView, nineoppoTxtView);
			break;
		case 9:

			eightoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			nineoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			startBlinkFor(nineoppoTxtView, tenoppoTxtView);
			break;
		case 10:

			nineoppoTxtView.setBackgroundResource(R.drawable.orange_circle);
			tenoppoTxtView.setAlpha(1);
			tenoppoTxtView.setBackgroundResource(R.drawable.orange_circle);

			break;

		default:
			break;
		}

	}

	private void startBlinkFor(TextView prevTxtView, TextView oneoppoTxtView2) {
		// TODO Auto-generated method stub

		if (animation != null)
			animation.cancel();

		animation = new AlphaAnimation(1, 0.5f);
		animation.setDuration(500);
		animation.setInterpolator(new LinearInterpolator());
		animation.setRepeatCount(Animation.INFINITE);
		animation.setRepeatMode(Animation.REVERSE);
		oneoppoTxtView2.startAnimation(animation);

		AlphaAnimation animation1 = new AlphaAnimation(1, 0.9f);
		animation1.setDuration(100);
		animation1.setInterpolator(new LinearInterpolator());
		animation1.setRepeatCount(1);
		animation1.setRepeatMode(Animation.REVERSE);

		if (prevTxtView != null)
			prevTxtView.startAnimation(animation1);

	}

	public boolean isChallenngee() {
		return isChallenngee;
	}

	public void setChallenngee(boolean isChallenngee) {
		this.isChallenngee = isChallenngee;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		Log.v("OnlineQuestionListFragmrnt", "OnDestroy");

		if (timeOutTimer != null)
			timeOutTimer.cancel();

		if (myCountDown10SecTimer != null)
			myCountDown10SecTimer.cancel();

		if (myCountDownTimer != null)
			myCountDownTimer.cancel();

		Log.v("OnlineQuestionListFragmrnt", "All Timers Cancelled");
	}

}
