package com.o2m.gsk.challenge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class QuizListFragment extends Fragment implements OnItemClickListener {

	private ListView topicList;
	private JSONArray topiclistJsonArray;
	protected String TAG;
	
	 public QuizListFragment(JSONArray _topiclistJsonArray) {
         // Empty constructor required for fragment subclasses
		 this.topiclistJsonArray = _topiclistJsonArray;
     }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.quizlist_fragment, container, false);
		topicList = (ListView) rootView.findViewById(R.id.quizlist);
		QuizItemAdapter topicAdapter = new QuizItemAdapter(this.getActivity(), topiclistJsonArray);
		topicList.setAdapter(topicAdapter);
		topicList.setOnItemClickListener(this);
		
		
		return rootView;
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		// TODO Auto-generated method stub
		Log.v(TAG, "onItemClick");
		try {
			final String QuizId = topiclistJsonArray.getJSONObject(position).getString("QuizId");
			
			new ApplicationAsk(getActivity(),"Fetching questions", new ApplicationService() {
				private String resp;

				@Override
				public void postExecute() {
					

					try {

					
						JSONObject dashboardResponseObj = new JSONObject(resp);
						String status = dashboardResponseObj.getString("status");

						if (status.equalsIgnoreCase("success")) {

							JSONArray dataArray = dashboardResponseObj
									.getJSONArray("data");
							
							
							QueListFragment fragment = new QueListFragment(dataArray);  
							 fragment.setQuizId(QuizId);
								FragmentManager fragmentManager = getActivity().getFragmentManager();
						        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
						        
						        FragmentTransaction ft = fragmentManager.beginTransaction();
								//ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,R.anim.slide_in_left, R.anim.slide_out_right) ;
						        ft.addToBackStack("messageList");
								ft.replace(R.id.content_frame, fragment, "messageList");
								
								ft.commit();
							
							   
						} else {
							Toast.makeText(
									QuizListFragment.this.getActivity(),
									""
											+ dashboardResponseObj
											.getString("message"),
											Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {

						e.printStackTrace();
					}

				}

				@Override
				public void execute() {
					JSONObject subTopicReq = new JSONObject();
					try {
						subTopicReq.put("QuizId", QuizId);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
					String subtopicURL = URLGenerator.getQuestionByQuizUrl();
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequestWithToken(subtopicURL,
							subTopicReq.toString() , "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0=" , QuizListFragment.this.getActivity());//preferences.getString("Token"));

					// return resp;

				}
			}).execute();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
