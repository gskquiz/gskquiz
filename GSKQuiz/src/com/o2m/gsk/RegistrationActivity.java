package com.o2m.gsk;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.CryptedConfiguration;
import com.o2m.gsk.utility.URLGenerator;
import com.o2m.gsk.utility.ValidationUtility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationActivity extends Activity implements
		View.OnClickListener {

	private EditText firstnameET, lastnameET, emailET, mobileET, locationET;
	private CheckBox termsandconditionsCB;
	private Button registerBtn;
	private Spinner specialitySpinner;
	private HashMap<String, String> therapyMap;
	private ArrayList<String> locationList = new ArrayList<String>();
	private RadioButton maleRB, femaleRB;
	private TextView privacyTXT;
	private TextView contactTXT , tandcTXT;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registation);
		firstnameET = (EditText) findViewById(R.id.firstNameET);
		lastnameET = (EditText) findViewById(R.id.lastNameET);
		specialitySpinner = (Spinner) findViewById(R.id.speciality_spinner);
		emailET = (EditText) findViewById(R.id.emailIdET);
		mobileET = (EditText) findViewById(R.id.mobileET);
		locationET = (EditText) findViewById(R.id.locationET);
		termsandconditionsCB = (CheckBox) findViewById(R.id.termsCB);
		registerBtn = (Button) findViewById(R.id.registerBtn);
		maleRB = (RadioButton) findViewById(R.id.maleRB);
		femaleRB = (RadioButton) findViewById(R.id.femaleRB);

		registerBtn.setOnClickListener(this);
		therapyMap = new HashMap<String, String>();

		privacyTXT = (TextView) findViewById(R.id.privacylink);
		contactTXT = (TextView) findViewById(R.id.contactuslink);
		tandcTXT = (TextView) findViewById(R.id.tandcTv);
		
		/*privacyTXT.setMovementMethod(LinkMovementMethod.getInstance());
		contactTXT.setMovementMethod(LinkMovementMethod.getInstance());
		tandcTXT.setMovementMethod(LinkMovementMethod.getInstance());*/
		privacyTXT.setOnClickListener(this);
		contactTXT.setOnClickListener(this);
		tandcTXT.setOnClickListener(this);
		
		termsandconditionsCB.setMovementMethod(LinkMovementMethod.getInstance());
		fetchTherapyMaster();

		locationET.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				/*
				 * if(count == 3) { fetchLocationOnline(); }
				 */
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				if (s.length() == 3) {
					fetchLocationOnline();
				}
			}
		});

	}

	protected void fetchLocationOnline() {
		// TODO Auto-generated method stub
		new ApplicationAsk(this, "Fetching location . .", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {
					JSONObject loginResponseObj = new JSONObject(resp);
					String status = loginResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = loginResponseObj
								.getJSONArray("data");

						if (dataObj.length() > 0) {
							for (int i = 0; i < dataObj.length(); i++) {
								JSONObject therapyObj = dataObj
										.getJSONObject(i);
								locationList.add(therapyObj.getString("Name"));
							}
							Log.v("", "" + locationList.toString());

							showLocationList();
						} else {
							Toast.makeText(RegistrationActivity.this,
									"Location Not Found", Toast.LENGTH_LONG)
									.show();
						}

					} else {
						Toast.makeText(RegistrationActivity.this,
								"" + loginResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}
			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject locObj = new JSONObject();
				try {
					locObj.put("Name", "" + locationET.getText().toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String locationURL = URLGenerator.GetLocationUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				//CommunicationService commService = new CommunicationService();
				//resp = commService.sendSyncRequest(locationURL,
				//		locObj.toString());
				
				

				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(locationURL, locObj.toString(),  RegistrationActivity.this);
				
			}
		}).execute();

	}

	protected void showLocationList() {
		// TODO Auto-generated method stub
		AlertDialog.Builder b = new AlertDialog.Builder(this);
		// AlertDialog.Builder b = new Builder(this);
		b.setTitle("Select Location");
		// String[] types = {"By Zip", "By Category"};
		b.setItems(locationList.toArray(new String[0]), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();
				locationET.setText(locationList.get(which));

			}

		});

		b.show();
	}

	private void fetchTherapyMaster() {

		new ApplicationAsk(this, "Fetching therapy . . ", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {
					JSONObject loginResponseObj = new JSONObject(resp);
					// JSONObject loginResponseObj1 = new JSONObject(
					// loginResponseObj.getString("d"));
					String status = loginResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = loginResponseObj
								.getJSONArray("data");

						for (int i = 0; i < dataObj.length(); i++) {
							JSONObject therapyObj = dataObj.getJSONObject(i);
							therapyMap.put(therapyObj.getString("Name"),
									therapyObj.getString("TherapyId"));
						}
						Log.v("", "" + therapyMap.toString());
						String[] mKeys = therapyMap.keySet().toArray(
								new String[therapyMap.size()]);
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								RegistrationActivity.this,
								android.R.layout.simple_spinner_dropdown_item,
								mKeys);
						specialitySpinner.setAdapter(adapter);
						

					} else {
						Toast.makeText(RegistrationActivity.this,
								"" + loginResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}
			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				String loginURL = URLGenerator.getTherapyUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				/*CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(loginURL, "");
*/
	CommunicationService commService = new CommunicationService();
				
				resp = commService.sendSyncRequest(loginURL, "",  RegistrationActivity.this);
			}
		}).execute();

	}

	@Override
	public void onClick(View v) {

		if (v == registerBtn) {
			if (!this.validate())
				return;

			new ApplicationAsk(this, "Please wait . . ", new ApplicationService() {
				private String resp;

				@Override
				public void postExecute() {
					// TODO Auto-generated method stub

					try {
						// resp =
						// "{\"status\":\"failure\", \"message\":\"User Already Registered\",\"data\" :[]}";
						JSONObject registraionResponseObj = new JSONObject(resp);

						String status = registraionResponseObj
								.getString("status");
						String message = registraionResponseObj
								.getString("message");
						JSONArray dataObj = registraionResponseObj
								.getJSONArray("data");
						String userID = "";
						if (dataObj.length() > 0)
							userID = dataObj.getJSONObject(0).getString(
									"UserId");
						Log.v("", "userID : " + userID);

						if (status.equalsIgnoreCase("success")) {

							CryptedConfiguration preferences = new CryptedConfiguration(
									RegistrationActivity.this,
									getResources().getString(R.string.crypted_configuration), getResources().getString(R.string.key),
									true);
							preferences.put("UserId", userID);
							// Get
							String user = preferences.getString("userId");
							VerifyOTP votp = new VerifyOTP(
									RegistrationActivity.this, userID);
							votp.setCancelable(false);
							votp.show();

						} else if (message
								.equalsIgnoreCase("User Confirmation Pending")) {
							CryptedConfiguration preferences = new CryptedConfiguration(
									RegistrationActivity.this,
									getResources().getString(R.string.crypted_configuration), getResources().getString(R.string.key),
									true);
							preferences.put("UserId", userID);
							// Get
							String user = preferences.getString("userId");
							VerifyOTP votp = new VerifyOTP(
									RegistrationActivity.this, userID);
							votp.setCancelable(false);
							votp.show();
						} else if (message.equalsIgnoreCase("Pending")) {
							CryptedConfiguration preferences = new CryptedConfiguration(
									RegistrationActivity.this,
									getResources().getString(R.string.crypted_configuration), getResources().getString(R.string.key),
									true);
							preferences.put("UserId", userID);

							Toast.makeText(RegistrationActivity.this,
									"Confirmation Pending", Toast.LENGTH_LONG)
									.show();

							VerifyOTP votp = new VerifyOTP(
									RegistrationActivity.this , userID);
							votp.setCancelable(false);
							votp.show();
						} else if (message
								.equalsIgnoreCase("User Already Registered")) {

							Toast.makeText(RegistrationActivity.this,
									"User Already Register,Login Now",
									Toast.LENGTH_LONG).show();

							Intent loginIntent = new Intent(
									RegistrationActivity.this,
									LoginActivity.class);
							startActivity(loginIntent);

						} else {
							Toast.makeText(
									RegistrationActivity.this,
									""
											+ registraionResponseObj
													.getString("message"),
									Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {

						e.printStackTrace();
					}

				}

				@Override
				public void execute() {
					// TODO Auto-generated method stub
					JSONObject registerObj = new JSONObject();
					try {
						registerObj.put("fname", firstnameET.getText()
								.toString().trim());
						registerObj.put("lname", lastnameET.getText()
								.toString().trim());
						registerObj.put("email", emailET.getText().toString()
								.trim());
						if (maleRB.isChecked())
							registerObj.put("Gender", "1");
						if (femaleRB.isChecked())
							registerObj.put("Gender", "0");

						Log.v("therapy",
								specialitySpinner.getSelectedItemPosition()
										+ ""
										+ therapyMap.toString()
										+ ""
										+ therapyMap.get(specialitySpinner
												.getSelectedItem().toString()
												.trim()));
						registerObj.put(
								"Therapy",
								therapyMap.get(specialitySpinner
										.getSelectedItem().toString().trim()));
						registerObj.put("address", locationET.getText()
								.toString().trim());
						registerObj.put("contact", mobileET.getText()
								.toString().trim());

					} catch (JSONException e) {

						e.printStackTrace();
					}

					String registerURL = URLGenerator.getRegisterUser();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(registerURL,	registerObj.toString(), RegistrationActivity.this);

				}

			}).execute();

		}
		
		else if(v == tandcTXT)
		{
			Intent webIntent = new Intent(this, LoadWebActivity.class);
        	webIntent.putExtra("screen", 1);
            startActivity(webIntent);
		}
		else if(v == privacyTXT)
        {
        	Intent webIntent = new Intent(this, LoadWebActivity.class);
        	webIntent.putExtra("screen", 2);
            startActivity(webIntent);
        }
        else if(v == contactTXT)
        {
        	Intent webIntent = new Intent(this, LoadWebActivity.class);
        	webIntent.putExtra("screen", 3);
            startActivity(webIntent);
        }
	}

	public boolean validate() {
		String firstName = firstnameET.getText().toString().trim();
		String lastName = lastnameET.getText().toString().trim();
		String specialityName = specialitySpinner.getSelectedItem().toString()
				.trim();
		String mobileNo = mobileET.getText().toString().trim();
		String email = emailET.getText().toString().trim();
		String location = locationET.getText().toString().trim();

		if (firstName.equalsIgnoreCase("")) {
			// Toast.makeText(this, "First name cant be empty",
			// Toast.LENGTH_SHORT).show();
			firstnameET.setError("First name cant be empty");
			return false;
		}

		if (lastName.equalsIgnoreCase("")) {
			// Toast.makeText(this, "Last name cant be empty",
			// Toast.LENGTH_SHORT).show();
			lastnameET.setError("Last name cant be empty");
			return false;
		}

		if (specialityName.equalsIgnoreCase("")) {
			// Toast.makeText(this, "Speciality cant be empty",
			// Toast.LENGTH_SHORT).show();
			// specialitySpinner.setError("Speciality cant be empty");
			return false;
		}

		if (mobileNo.equalsIgnoreCase("") || mobileNo.length() != 10) {
			// Toast.makeText(this, "Please enter valid mobile no.",
			// Toast.LENGTH_SHORT).show();
			mobileET.setError("Please enter valid mobile no.");
			return false;
		}

		if (email.equalsIgnoreCase("")
				|| !ValidationUtility.isValidEmail(email)) {
			// Toast.makeText(this, "Please enter valid email",
			// Toast.LENGTH_SHORT).show();
			emailET.setError("Please enter valid email");
			return false;
		}

		if (location.equalsIgnoreCase("")) {
			// Toast.makeText(this, "location cant be empty",
			// Toast.LENGTH_SHORT).show();
			locationET.setError("location cant be empty");
			return false;
		}

		if (!maleRB.isChecked() && !femaleRB.isChecked()) {
			Toast.makeText(this, "Select your gender", Toast.LENGTH_SHORT)
					.show();
			return false;
		}
		if (!termsandconditionsCB.isChecked()) {
			Toast.makeText(this, "Please accept terms and condition",
					Toast.LENGTH_SHORT).show();
			return false;
		}

		return true;

	}

}
