package com.o2m.gsk.myscore;

import org.json.JSONArray;

import android.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.CacheMemory;

public class MyScoreFragment extends Fragment  {

	
	protected String TAG;
	private JSONArray scoreArray;
	private String totalPoints;
	private TextView topicNameTV, pointsTv;
	private ExpandableListView scoreListView;
	
	public MyScoreFragment(JSONArray scoreObj) {
		// TODO Auto-generated constructor stub
		this.scoreArray = scoreObj;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.myscore_fragment,
				container, false);
		topicNameTV = (TextView) rootView.findViewById(R.id.topicNameTv);
		pointsTv = (TextView) rootView.findViewById(R.id.pointsearnedTv);
		scoreListView = (ExpandableListView) rootView.findViewById(R.id.scoreExList);		
		MyScoreAdapter adapter = new MyScoreAdapter(getActivity(), scoreArray);
		scoreListView.setAdapter(adapter);
		
		topicNameTV.setText(""+CacheMemory.getTherapy(getActivity()));
		if(getTotalPoints() != null)
		{
			Log.v("", "FRESH POINTS");
			pointsTv.setText("Total Points Earned -"+getTotalPoints());
		}
		else
		{
			Log.v("", "CACHE POINTS");
			pointsTv.setText("Total Points Earned -"+CacheMemory.getPoints(getActivity()));
		}
		
		 this.setGroupIndicatorToRight();

		return rootView;
	}

	private void setGroupIndicatorToRight() {
        /* Get the screen width */
        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
 
        //performanceWallListView.setIndicatorBounds(width - getDipsFromPixel(35), width
        //        - getDipsFromPixel(5));

        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
        	scoreListView.setIndicatorBounds(width - getDipsFromPixel(35), width
                    - getDipsFromPixel(5));
       	} else {
       		scoreListView.setIndicatorBoundsRelative(width - getDipsFromPixel(35), width
                       - getDipsFromPixel(5));
       	   //mListView.setIndicatorBoundsRelative(myLeft, myRight);
       	}
        
    }
 
    // Convert pixel to dip
    public int getDipsFromPixel(float pixels) {
        // Get the screen's density scale
        final float scale = getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

	public String getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(String totalPoints) {
		this.totalPoints = totalPoints;
	}
	 
}
