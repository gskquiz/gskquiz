package com.o2m.gsk.myscore;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.o2m.gsk.R;

public class MyScoreAdapter extends BaseExpandableListAdapter {

	Context context;

	ArrayList<String> arrayMessages;

	JSONArray levelJsonArray;
	String unreadCount;

	// private ChannelDetails channelDetails;

	// public MessageAdapter(Context context, ArrayList<String> _arrayMessages)
	public MyScoreAdapter(Context context, JSONArray _arrayFaq) {

		this.context = context;
		this.levelJsonArray = _arrayFaq;
	}

	private static class ChildViewHolder {

		public TextView txtView_childname;
		public TextView txtView_childpoints;

	}

	private static class GroupViewHolder {

		public TextView txtView_parentTitle;
		public TextView txtView_parentPoints;

	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return levelJsonArray.length();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		/*try {
			JSONObject brandObj = (JSONObject) levelJsonArray
					.get(groupPosition);
			JSONArray questionArray = brandObj.getJSONArray("Levels");
			return questionArray.length();
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return 3;
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		try {

			JSONObject performanceObj = (JSONObject) levelJsonArray
					.get(groupPosition);

			return performanceObj;
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View view,
			ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		GroupViewHolder holder = new GroupViewHolder();
		/*if (view == null) {*/

			holder = new GroupViewHolder();
			view = inflater.inflate(R.layout.score_grp_list_item, parent,
					false);
			holder.txtView_parentTitle = (TextView) view
					.findViewById(R.id.childScoreTopicTV);
			holder.txtView_parentPoints = (TextView) view
					.findViewById(R.id.childpoitsTV);

			view.setTag(holder);
		/*} else {
			holder = (GroupViewHolder) view.getTag();
		}*/

		try {

			JSONObject performanceObj = (JSONObject) levelJsonArray
					.get(groupPosition);

			holder.txtView_parentTitle.setText(""
					+ performanceObj.getString("name"));
			holder.txtView_parentPoints.setText(""
					+ performanceObj.getString("points"));

		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*if (groupPosition % 2 == 0) {
			view.setBackgroundColor(Color.parseColor(context.getResources()
					.getString(R.string.gray_gradient_1)));
		}*/

		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View view, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ChildViewHolder holder = new ChildViewHolder();
		/*if (view == null) {*/

			holder = new ChildViewHolder();
			view = inflater.inflate(R.layout.score_child_list_item, parent,
					false);
			holder.txtView_childname = (TextView) view
					.findViewById(R.id.childScoreTopicTV);
			holder.txtView_childpoints = (TextView) view
					.findViewById(R.id.childpoitsTV);

			view.setTag(holder);
		/*} else {
			holder = (ChildViewHolder) view.getTag();
		}*/

		try {

			JSONObject brandObj = (JSONObject) levelJsonArray
					.get(groupPosition);
			JSONArray questionArray = brandObj.getJSONArray("Levels");
			//JSONObject queObj = (JSONObject) questionArray.get(childPosition);

			
			
			if(childPosition == 0)
			{
				for(int i = 0 ; i < questionArray.length(); i++)
				{
					JSONObject queObj = (JSONObject) questionArray.get(i);
					
					holder.txtView_childname.setText("Beginner");
					if(queObj.getString("name").equalsIgnoreCase("Beginner"))
					{						
						holder.txtView_childpoints.setText("" + queObj.getString("points"));
						break;
					}
					else
					{
						holder.txtView_childpoints.setText("0");
					}				
				}
			}
			else if(childPosition == 1)
			{
				for(int i = 0 ; i < questionArray.length(); i++)
				{
					JSONObject queObj = (JSONObject) questionArray.get(i);
					holder.txtView_childname.setText("Intermediate");
					if(queObj.getString("name").equalsIgnoreCase("Intermediate"))
					{
						//holder.txtView_childname.setText("" + queObj.getString("name"));
						holder.txtView_childpoints.setText("" + queObj.getString("points"));
						break;
					}
					else
					{
						holder.txtView_childpoints.setText("0");
					}
									
				}
			}
			else if(childPosition == 2)
			{
				for(int i = 0 ; i < questionArray.length(); i++)
				{
					JSONObject queObj = (JSONObject) questionArray.get(i);
					
					holder.txtView_childname.setText("Advance");
					if(queObj.getString("name").equalsIgnoreCase("Advance"))
					{
						//holder.txtView_childname.setText("" + queObj.getString("name"));
						holder.txtView_childpoints.setText("" + queObj.getString("points"));
						break;
					}
					else
					{
						holder.txtView_childpoints.setText("0");
					}
									
				}
			}
			
			
			
			

		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*if (groupPosition % 2 == 0) {
		view.setBackgroundColor(Color.parseColor(context.getResources()
				.getString(R.string.gray_gradient_1)));
	}*/

		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}

}
