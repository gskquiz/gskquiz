package com.o2m.gsk.notification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.o2m.gsk.GSKHomeActivity;
import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class NotificationListFragment extends Fragment implements
		OnItemClickListener {

	private ListView topicList;
	private static JSONArray topiclistJsonArray;
	private JSONArray filteredtopiclistJsonArray;
	protected String TAG;

	public NotificationListFragment() {
		// Empty constructor required for fragment subclasses
		//topiclistJsonArray = _topiclistJsonArray;
		
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		loadNotifications();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.notificationlist_fragment,
				container, false);
		topicList = (ListView) rootView.findViewById(R.id.notificationlist);

		/*filteredtopiclistJsonArray = new JSONArray();
		for (int i = 0; i < topiclistJsonArray.length(); i++) {

			JSONObject nTObj;
			try {
				nTObj = topiclistJsonArray.getJSONObject(i);
				if (!nTObj.getString("Type").equalsIgnoreCase("5")) {

					filteredtopiclistJsonArray.put(nTObj);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		NotificationItemAdapter topicAdapter = new NotificationItemAdapter(
				this.getActivity(), filteredtopiclistJsonArray);
		topicList.setAdapter(topicAdapter);*/
		topicList.setOnItemClickListener(this);

		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {
		// TODO Auto-generated method stub
	
		try {

			JSONObject nTObj = (JSONObject) topiclistJsonArray.get(position);
			// Type
			/*
			 * 1- Friend Request 2 - Challenge Quiz 3 - Webinars
			 */

			if (nTObj.getString("Type").equalsIgnoreCase("1")) {
				RequestAcceptDialog rad = new RequestAcceptDialog(
						getActivity(), nTObj);
				rad.show();
			} else if (nTObj.getString("Type").equalsIgnoreCase("2")) {
				RequestAcceptDialog rad = new RequestAcceptDialog(
						getActivity(), nTObj);
				rad.show();
			}

		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	private void loadNotifications() {
		// TODO Auto-generated method stub
		new ApplicationAsk(this.getActivity(), "Fetching notifications . . ",
				new ApplicationService() {

			private String resp;

			@Override
			public void postExecute() {

				try {

					JSONObject notificationResponseObj = new JSONObject(
							resp);
					String status = notificationResponseObj
							.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray notificationArray = notificationResponseObj
								.getJSONArray("data");
						//notificationArray = new JSONArray();
						if (notificationArray.length() > 0) {
							
							NotificationListFragment.topiclistJsonArray = notificationArray;
							filteredtopiclistJsonArray = new JSONArray();
							for (int i = 0; i < topiclistJsonArray.length(); i++) {

								JSONObject nTObj;
								try {
									nTObj = topiclistJsonArray.getJSONObject(i);
									if (!nTObj.getString("Type").equalsIgnoreCase("5")) {

										filteredtopiclistJsonArray.put(nTObj);
									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

							NotificationItemAdapter topicAdapter = new NotificationItemAdapter(
									NotificationListFragment.this.getActivity(), filteredtopiclistJsonArray);
							topicList.setAdapter(topicAdapter);
							
							
							
							
						} else {
							Toast.makeText(NotificationListFragment.this.getActivity(),
									"No notifications to display",
									Toast.LENGTH_LONG).show();
						}
					} else {
						Toast.makeText(
								NotificationListFragment.this.getActivity(),
								""
										+ notificationResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {

				String notificationURL = URLGenerator
						.getMyNotificationUrl();
				
				JSONObject param = new JSONObject();
				try {
					param.put("IsRead", "1");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(
						notificationURL, param.toString(),
						CacheMemory.getToken(NotificationListFragment.this.getActivity()), NotificationListFragment.this.getActivity());

				

			}
		}).execute();

	}
	
}
