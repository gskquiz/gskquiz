package com.o2m.gsk.notification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.challenge.ChallengeeQuestionListFragment;
import com.o2m.gsk.challenge.OnlineQuestionListFragment;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class RequestAcceptDialog extends Dialog implements
		android.view.View.OnClickListener {

	public Activity activity;
	public Dialog dialog;
	public Button acceptBtn, cancelBtn;
	private TextView msgTxt;
	private JSONObject nTObj;

	public RequestAcceptDialog(Activity context, JSONObject _nTObj) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;
		this.nTObj = _nTObj;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.request_accept_dialog);

		acceptBtn = (Button) findViewById(R.id.acceptBtn);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
		msgTxt = (TextView) findViewById(R.id.msgTxT);
		acceptBtn.setOnClickListener(this);

		cancelBtn.setOnClickListener(this);

		try {
			msgTxt.setText("" + nTObj.getString("Message"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == acceptBtn) {

			String challengeId = null;

			try {

				challengeId = nTObj.getString("ChallengeId");

				if(nTObj.getString("Type").equalsIgnoreCase("1"))
				{
					if (challengeId != null)
						acceptRequest(challengeId);

				}
				else if(nTObj.getString("Type").equalsIgnoreCase("2"))
				{
					
					if(challengeId != null)
						acceptFriendRequest(challengeId);
					
					
					
				}

				else if(nTObj.getString("Type").equalsIgnoreCase("5"))
				{
					
					if(challengeId != null)
						acceptQuizChallengeRequest(challengeId);
					
				}


			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		else if (arg0 == cancelBtn) {

			
			
			
			 try {
				if(nTObj.getString("Type").equalsIgnoreCase("5"))
					{
						String challengeId = null;
						challengeId = nTObj.getString("ChallengeId");
						if(challengeId != null)
							acceptCancelOnlineQuizChallengeRequest(challengeId);
						
					}
				else
				{
					CacheMemory.setOnlineChallengeStatus(RequestAcceptDialog.this.activity, CacheMemory.ONLINE_CHALLENGE_NO);
					this.dismiss();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}

	}

	private void acceptRequest(final String challengeId) {
		// TODO Auto-generated method stub
		new ApplicationAsk(activity,"Accepting request", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {

					JSONObject acceptResponseObj = new JSONObject(resp);
					String status = acceptResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						RequestAcceptDialog.this.dismiss();
						Toast.makeText(activity,
								"Succefully Accepted" ,
								Toast.LENGTH_LONG).show();
						
						activity.getFragmentManager()
						.popBackStack();
						
						

					} else {
						Toast.makeText(activity,
								"" + acceptResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject acceptObj = new JSONObject();
				try {

					acceptObj.put("Id", challengeId);

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String validateURL = URLGenerator.getAcceptRequestUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(validateURL,
						acceptObj.toString(), CacheMemory.getToken(activity), activity);
			}

		}).execute();
	}
	
	private void acceptFriendRequest(final String challengeId) {
		// TODO Auto-generated method stub
		new ApplicationAsk(activity, "Accepting friend", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {

					JSONObject acceptResponseObj = new JSONObject(resp);
					String status = acceptResponseObj.getString("status");
					String friendScore = "", friendName = "";
					
					if (status.equalsIgnoreCase("success")) {

						RequestAcceptDialog.this.dismiss();
						Toast.makeText(activity,
								"Succefully Accepted" ,
								Toast.LENGTH_LONG).show();
						
						JSONArray dataArray = acceptResponseObj
								.getJSONArray("data");
						
						if(dataArray.length() > 0)
						{
							JSONObject queJsonObj = dataArray.getJSONObject(0);
							friendName = queJsonObj.getString("ChallengerName");
							friendScore = queJsonObj.getString("Score");
									
						}
						
						
						ChallengeeQuestionListFragment fragment = new ChallengeeQuestionListFragment(
								dataArray);
						fragment.setQuizId(challengeId);
						fragment.setChallengeFriend(true);
						fragment.setChallengee(true);
						fragment.setFriendScore(friendScore);
						fragment.setFriendName(friendName);
						FragmentManager fragmentManager = activity.getFragmentManager();
						FragmentTransaction ft = fragmentManager
								.beginTransaction();

						//ft.addToBackStack("ChallengeeQuestionListFragment");
						ft.replace(R.id.content_frame, fragment,
								"QueListFragment");

						ft.commit();
						
						

					} else {
						Toast.makeText(activity,
								"" + acceptResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject acceptObj = new JSONObject();
				try {

					acceptObj.put("QuizId", challengeId);

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String validateURL = URLGenerator.getQuestionByQuizChallengeAcceptUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(validateURL,
						acceptObj.toString(), CacheMemory.getToken(activity), activity);
			}

		}).execute();
	}
	
	
	private void acceptQuizChallengeRequest(final String challengeId) {
		// TODO Auto-generated method stub
		new ApplicationAsk(activity, "Accepting challenge", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {

					JSONObject acceptResponseObj = new JSONObject(resp);
					String status = acceptResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {


						JSONObject dataObj = acceptResponseObj
								.getJSONArray("data").getJSONObject(0);
						
						String connectionId = dataObj.getString("ConnectionId");
						String quizId = dataObj.getString("QuizId");
						String OpponentName = dataObj.getString("OpponentName");
						
						CacheMemory.setConnectionId(activity, connectionId);
						RequestAcceptDialog.this.loadOnlineQuiz(connectionId, quizId , OpponentName);
						Toast.makeText(activity,
								"Successfully Accepted" ,
								Toast.LENGTH_LONG).show();
											
						

					} else {
						Toast.makeText(activity,
								"" + acceptResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject acceptObj = new JSONObject();
				try {

					acceptObj.put("ConnectionId", challengeId);

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String validateURL = URLGenerator.getAcceptQuizRequestUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(validateURL,
						acceptObj.toString(), CacheMemory.getToken(activity), activity);
			}

		}).execute();
	}
	
	private void acceptCancelOnlineQuizChallengeRequest(final String challengeId) {
		// TODO Auto-generated method stub
		new ApplicationAsk(activity, "Challenge cancelled", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {

					JSONObject acceptResponseObj = new JSONObject(resp);
					String status = acceptResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						CacheMemory.setOnlineChallengeStatus(RequestAcceptDialog.this.activity, CacheMemory.ONLINE_CHALLENGE_NO);
						RequestAcceptDialog.this.dismiss();
						/*
						JSONObject dataObj = acceptResponseObj
								.getJSONArray("data").getJSONObject(0);
						
						String connectionId = dataObj.getString("ConnectionId");
						String quizId = dataObj.getString("QuizId");
						String OpponentName = dataObj.getString("OpponentName");
						
						CacheMemory.setConnectionId(activity, connectionId);
						RequestAcceptDialog.this.loadOnlineQuiz(connectionId, quizId , OpponentName);
						
						Toast.makeText(activity,
								"Successfully Accepted" ,
								Toast.LENGTH_LONG).show();
						*/					
						

					} else {
						Toast.makeText(activity,
								"" + acceptResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject acceptObj = new JSONObject();
				try {

					acceptObj.put("ConnectionId", challengeId);

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String validateURL = URLGenerator.getCancelAcceptQuizRequestUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(validateURL,
						acceptObj.toString(), CacheMemory.getToken(activity), activity);
			}

		}).execute();
	}
	
	public void loadOnlineQuiz(final String _connectionId , final String _quizId, final String _OpponentName)
	{
		new ApplicationAsk(RequestAcceptDialog.this.activity, "Fetching question",
				new ApplicationService() {
					private String resp;

					@Override
					public void execute() {
						JSONObject gQO = new JSONObject();
						try {
							gQO.put("ConnectionId", _connectionId);
							gQO.put("QuizId", _quizId);
							

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							
						}
						String getQuestiondOnlineURL = URLGenerator
								.getQuestionByQuizChallengeUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								getQuestiondOnlineURL, gQO.toString(), CacheMemory
										.getToken(RequestAcceptDialog.this.activity
												), activity);

					}

					@Override
					public void postExecute() {

						try {
							
							
							JSONObject friendResponseObj = new JSONObject(resp);

							String status = friendResponseObj
									.getString("status");
							if (status.equalsIgnoreCase("success")) {

								JSONArray dataArray = friendResponseObj
										.getJSONArray("data");
								
								OnlineQuestionListFragment oqlf = new OnlineQuestionListFragment(dataArray);
								oqlf.setChallenngee(true);
								oqlf.setOpponentName(_OpponentName);
								FragmentTransaction ft = RequestAcceptDialog.this.activity.getFragmentManager().beginTransaction();
								//ft.addToBackStack("OnlineQuestionListFragment");
								ft.replace(R.id.content_frame, oqlf, "OnlineQuestionListFragment");

								ft.commit();
								
								RequestAcceptDialog.this.dismiss();	
							
								
							} else {
								Toast.makeText(
										RequestAcceptDialog.this.activity
												,
										""
												+ friendResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}
					}
				}).execute();
	}


}
