package com.o2m.gsk.notification;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.gsk.R;

public class NotificationItemAdapter extends BaseAdapter
{

	Context context;

	ArrayList<String> arrayMessages;

	JSONArray nTJsonArray;
	String unreadCount;

	public NotificationItemAdapter(Context context, JSONArray _arrayMessages)
	{

		this.context = context;
		this.nTJsonArray = _arrayMessages;
	}

	@Override
	public int getCount()
	{

		return nTJsonArray.length();
	}

	@Override
	public Object getItem(int position)
	{

		try {
			return nTJsonArray.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{

			holder = new ViewHolder();
			view = inflater.inflate(R.layout.notification_list_item, parent, false);
			holder.txtView_msgTitle = (TextView) view.findViewById(R.id.notificationMSg);
			holder.categoryImageView = (ImageView) view.findViewById(R.id.categoryImageView);
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		

		
		
		try {
			
			JSONObject nTObj = (JSONObject) nTJsonArray.get(position);
			
			holder.txtView_msgTitle.setText("" + nTObj.getString("Message"));
			//Type
			/*
			 * 1- Friend Request
			   2 - Challenge Quiz
			   3 - Webinars
			   
			 */			
			
			if(nTObj.getString("Type").equalsIgnoreCase("2"))
			{
				holder.categoryImageView.setBackgroundResource(R.drawable.computer);
			}
			
			
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
				
		if(position %2 == 0)
		{
			view.setBackgroundColor(Color.parseColor(context.getResources().getString(R.string.gray_gradient_1)));
		}
		
		
		return view;

	}

	private static class ViewHolder
	{

		public TextView txtView_msgTitle;
		public ImageView categoryImageView;

	}

}
