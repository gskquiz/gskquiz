package com.o2m.gsk.utility;

public interface ApplicationService
{
	public void execute();

	public void postExecute();
}
