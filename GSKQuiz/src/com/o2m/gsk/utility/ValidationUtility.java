package com.o2m.gsk.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.Context;

public class ValidationUtility {

	public final static boolean isValidEmail(CharSequence target) {
	    if (target == null) {
	        return false;
	    } else {
	        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	    }
	}
	
	//^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}
	
	public final static boolean isValidPassword(String target) {
	    if (target == null) {
	        return false;
	    } else {
	        return target.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{4,}");
	    }
	}
	
	public static String readFromAssets(Context context, String filename)  {
	   

	    // do reading, usually loop until end of file reading  
	    StringBuilder sb = new StringBuilder(); 
		try {
			 BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(filename)));
			//sb = new StringBuilder();
			String mLine = reader.readLine();
			while (mLine != null) {
			    sb.append(mLine); // process line
			    mLine = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return sb.toString();
	}
	
}
