package com.o2m.gsk.utility;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.X509TrustManager;

import android.util.Log;

import com.mobicule.android.component.logging.MobiculeLogger;

public class MobiCustomSSLTrustManager implements X509TrustManager {
	private static final String TAG = MobiCustomSSLTrustManager.class
			.getSimpleName();

	private static MobiCustomSSLTrustManager instance;

	private ArrayList<X509Certificate> x509Certificates;

	private boolean isSSLPinningEnable;

	private boolean isLogEnabled;

	// CONSTANTS
	public static final String NO_SERVER_CERTIFICATES = "Server Does Not Sent Ssl Certificates";
	public static final String NO_DEVICE_CERTIFICATES = "Device Certificates Not Configured.";

	public static final String INVALID_CA_CERTIFICATES = "Invalid CA Certificates";

	// END

	private MobiCustomSSLTrustManager(
			ArrayList<X509Certificate> x509Certificates,
			boolean isSSLPinningEnable, boolean isLogEnabled) {
		this.x509Certificates = x509Certificates;
		this.isSSLPinningEnable = isSSLPinningEnable;
		this.isLogEnabled = isLogEnabled;
	}

	public static MobiCustomSSLTrustManager getInstance(
			ArrayList<X509Certificate> x509Certificates,
			boolean isSSLPinningEnable, boolean isLogEnabled) {
		if (instance == null) {
			instance = new MobiCustomSSLTrustManager(x509Certificates,
					isSSLPinningEnable, isLogEnabled);
		}

		return instance;
	}

	@Override
	public void checkClientTrusted(
			X509Certificate[] paramArrayOfX509Certificate, String paramString)
			throws CertificateException {

	}

	@Override
	public void checkServerTrusted(X509Certificate[] chain, String authType)
			throws CertificateException {

		boolean isCertificateMatched = false;

		if (!isSSLPinningEnable) {
			isCertificateMatched = true;
			if (isLogEnabled) {
				MobiculeLogger.debug("SSL pinning bypassed \n SSLpinning : "
						+ isSSLPinningEnable);
			}

		} else {

			if (chain == null)
				throw new IllegalArgumentException(NO_SERVER_CERTIFICATES);
			if (!(chain.length > 0)) {
				throw new IllegalArgumentException(NO_SERVER_CERTIFICATES);
			}

			if (this.x509Certificates == null) {
				throw new IllegalArgumentException(NO_DEVICE_CERTIFICATES);
			}
			
			Log.v("", "Server : " + chain.length + "Device " + this.x509Certificates.size());
			
			for (int i = 0; i < chain.length; i++) {
				X509Certificate serverCertificate = chain[i];

				for (X509Certificate deviceCertificate : this.x509Certificates) {
					if (serverCertificate.equals(deviceCertificate)) {
						isCertificateMatched = true;
						if (isLogEnabled) {
							MobiculeLogger.debug("Certificate Matched: "
									+ isCertificateMatched);
						}

					}
				}

			}
		}
		// ***IF CA NOT matched, then Terminate Hand shaking process
		if (!isCertificateMatched)
			throw new CertificateException(INVALID_CA_CERTIFICATES);

	}

	public X509Certificate[] getAcceptedIssuers() {

		if (isLogEnabled) {
			MobiculeLogger.debug(TAG, "getAcceptedIssuers()");
		}
		return null;
	}

}
