package com.o2m.gsk.utility;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

public class SSLCertificate {
	
	private static SSLCertificate sslCertificate = null;
	ArrayList<X509Certificate> devicePinnedCertificates = new ArrayList<X509Certificate>();
	Context context;
	//private String[] pinnedCertificates = {"AWS_Intermediate.cer", "AWS_Root.cer"};
	private String[] pinnedCertificates = {"AWS_Root.cer"};
	
	
	private  SSLCertificate()
	{
		
	}
	
	public static synchronized SSLCertificate getInstance(Context _context) {
		
		if(sslCertificate == null)
		{
			sslCertificate = new SSLCertificate();
			sslCertificate.context = _context;
			return sslCertificate;
		}
		
		return sslCertificate;
	}
	
	public ArrayList<X509Certificate> getDeviceCertificate()
			 {
		
		if(devicePinnedCertificates.size() > 0)
			return devicePinnedCertificates;
		try
		{
		X509Certificate deviceCertificate = null;
		//ArrayList<X509Certificate> devicePinnedCertificates = new ArrayList<X509Certificate>();
		AssetManager assetManager = context.getResources().getAssets();
		InputStream inputStream = null;
		InputStream caInput;
		CertificateFactory certificateFactory = CertificateFactory
				.getInstance("X509");
		if (this.pinnedCertificates != null) {
			if (this.pinnedCertificates.length != 0) {
				for (int index = 0; index < this.pinnedCertificates.length; index++) {
					inputStream = assetManager
							.open(this.pinnedCertificates[index]);
					caInput = new BufferedInputStream(inputStream);
					deviceCertificate = (X509Certificate) certificateFactory
							.generateCertificate(caInput);
					devicePinnedCertificates.add(deviceCertificate);
				}
			}
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return devicePinnedCertificates;
	}

}
