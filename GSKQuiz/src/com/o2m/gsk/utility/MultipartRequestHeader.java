package com.o2m.gsk.utility;

import java.util.HashMap;

public class MultipartRequestHeader {
	private HashMap<String, Object> dataParams;
	private HashMap<String, String> fileParams;

	public MultipartRequestHeader() {
		dataParams = new HashMap<String, Object>();
		fileParams = new HashMap<String, String>();
	}

	public HashMap<String, Object> getDataParams() {
		return dataParams;
	}

	public void addDataParameteres(String name, Object value) {
		dataParams.put(name, value);
	}

	public HashMap<String, String> getFileData() {
		return fileParams;
	}

	public void addFileParameteres(String fileName, String filePath) {
		fileParams.put(fileName, filePath);
	}
}
