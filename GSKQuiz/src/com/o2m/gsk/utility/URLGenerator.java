package com.o2m.gsk.utility;

public class URLGenerator {

	// STAGING
	//public static final String DOMAIN = "http://stagingiis7.gsk-qzone.com";
	// UAT
	//public static final String DOMAIN = "http://gskquizapp.oh22works.com";
	// PRE-PROD
	public static final String DOMAIN = "https://wwwnewiis7.gsk-qzone.com";
	//PROD
	//public static final String DOMAIN = "https://www.gsk-qzone.com";
	
	// UAT
	//static final String BASE_URL = "http://gskquizapp.oh22works.com/api/QuizApp";
	// STAGING
	static final String BASE_URL = DOMAIN + "/api/QuizApp";
	// PRE-PROD
	//static final String BASE_URL = "https://wwwnewiis7.gsk-qzone.com/api/QuizApp";

	public static String getTherapyUrl() {

		return BASE_URL + "/" + "GetTherapy";

	}
	///GetLocation

	public static String GetLocationUrl() {

		return BASE_URL + "/" + "GetLocation";

	}
	
	public static String getUpdateProfileUrl() {

		return BASE_URL + "/" + "UpdateProfile";

	}
	
	public static String getChangePasswordUrl() {

		return BASE_URL + "/" + "ChangePassword";

	}
	public static String getDeleteUnDeleteFriendUrl() {

		return BASE_URL + "/" + "DeleteUnDeleteFriend";

	}
	///DeleteUnDeleteFriend

	// UpdateProfile
	public static String getRegisterUser() {

		return BASE_URL + "/" + "RegisterUser";

	}
	
	public static String getLocation() {

		return BASE_URL + "/" + "GetLocation";

	}

	public static String getfriendslist() {

		return BASE_URL + "/" + "FriendList";

	}
	//FriendListForChallenge
	public static String getFriendListForChallengeURL() {

		return BASE_URL + "/" + "FriendListForChallenge";

	}

	public static String getInviteelist() {

		return BASE_URL + "/" + "InviteeList";

	}

	// InviteeList
	// ValidateOTP
	public static String getValidateOTPURL() {

		return BASE_URL + "/" + "ValidateOTP";

	}

	public static String getResendOTPURL() {

		return BASE_URL + "/" + "ResendOTP";

	}

	public static String getSavePasswordURL() {

		return BASE_URL + "/" + "SavePassword";

	}

	public static String getForgotPasswordURL() {

		return BASE_URL + "/" + "ForgotPassword";

	}

	public static String getLoginUrl() {

		return BASE_URL + "/" + "ValidateUser";

	}

	// /GetAllDashboardData

	public static String getGetAllDashboardDataUrl() {

		return BASE_URL + "/" + "GetAllDashboardData";

	}

	public static String getFeedbacksUrl() {

		return BASE_URL + "/" + "Feedbacks";

	}

	// Feedbacks

	public static String getMyAccountUrl() {

		return BASE_URL + "/" + "UserDetails";

	}
	
	public static String getMyNotificationUrl() {

		return BASE_URL + "/" + "GetNotification";

	}
	
	public static String getAcceptRequestUrl() {

		return BASE_URL + "/" + "AcceptRequest";

	}
	//QuestionByQuizChallenge
	public static String getQuestionByQuizChallengeAcceptUrl() {

		return BASE_URL + "/" + "QuestionByQuizChallengeAccept";

	}
	
	public static String getSaveOnlineQuizURL() {

		return BASE_URL + "/" + "SaveOnlineQuiz";

	}
	
	//AcceptRequest

	public static String getSaveFeedbacksUrl() {

		return BASE_URL + "/" + "SaveFeedback";

	}

	public static String getAddFriendUrl() {

		return BASE_URL + "/" + "AddFriend";

	}

	public static String getMyScoreUrl() {

		return BASE_URL + "/" + "MyScores";

	}

	public static String getGetSubTopicUrl() {

		return BASE_URL + "/" + "GetAllSubTopic";

	}

	public static String getQuizListUrl() {

		return BASE_URL + "/" + "QuizLevelList";

	}

	// QuestionByQuiz
	public static String getQuestionByQuizUrl() {

		return BASE_URL + "/" + "QuestionByQuiz";

	}

	public static String getQuestionByQuizChallengeUrl() {

		return BASE_URL + "/" + "QuestionByQuizChallenge";

	}
	//QuestionByQuizChallengeNew
	public static String getQuestionByQuizChallengeNewUrl() {

		return BASE_URL + "/" + "QuestionByQuizChallengeNew";

	}

	// QuestionByQuizChallenge)
	// SaveSelfQuiz
	/*public static String getSaveAnswersChallengerUrl() {

		return BASE_URL + "/" + "SaveAnswersChallenger";

	}*/
	public static String getSaveSelfQuizUrl() {

		return BASE_URL + "/" + "SaveSelfQuiz";

	}
	//SaveOnlineQuizResult
	public static String getSaveOnlineQuizResultUrl() {

		return BASE_URL + "/" + "SaveOnlineQuizResult";

	}
	
	public static String getSelectOpponentUrl() {

		return BASE_URL + "/" + "SelectOpponent";

	}

	//CheckConnectionStatus
	public static String getCheckConnectionStatusUrl() {

		return BASE_URL + "/" + "CheckConnectionStatus";

	}
	
	//AcceptQuizRequest
	public static String getAcceptQuizRequestUrl() {

		return BASE_URL + "/" + "AcceptQuizRequest";

	}
	
	public static String getCancelAcceptQuizRequestUrl() {

		return BASE_URL + "/" + "RejectQuizRequest";

	}
	
	///RejectQuizRequest
	
	//UpdateOnlineStatus
	
	public static String getUpdateOnlineStatusUrl() {

		return BASE_URL + "/" + "UpdateOnlineStatus";

	}
	
	public static String getGetQuestionsOnlineUrl() {

		return BASE_URL + "/" + "GetQuestionsOnline";

	}
	
	public static String getLogoutUrl() {

		return BASE_URL + "/" + "Logout";

	}
	
	//SaveAndGetScoreDetails
	public static String getSaveAndGetScoreDetailsUrl() {

		return BASE_URL + "/" + "SaveAndGetScoreDetails";

	}
	
	public static String getSaveAnswersChallengerUrl() {

		return BASE_URL + "/" + "SaveAnswersChallenger";

	}
	public static String getSaveAnswersChallengeeUrl() {

		return BASE_URL + "/" + "SaveAnswersChallengee";

	}
	
	//SaveAnswersChallengee)

	// /SaveAnswersChallenger

	public static String WOW_Check_feedback_display() {

		return BASE_URL + "/" + "WOW_Check_feedback_display";

	}

	public static String getAllDivision() {

		return BASE_URL + "/" + "GetDivision";

	}

	public static String getDivision_L5_L4_User() {

		return BASE_URL + "/" + "getdivision_L5_L4_User";

	}

	// GetUserListForEmployeeID {�p_employee_id� : �8081�,
	// �p_div_code� : �E�, �p_level_code�: �L1�}
	public static String getUserListForEmployeeUrl() {

		return BASE_URL + "/" + "GetUserListForEmployeeID";

	}

	// getCommentsBySLID
	public static String getCommentsForSharedLearning() {

		return BASE_URL + "/" + "getCommentsBySLID";

	}

	// AddSLComments

	public static String getAddCommentsForSharedLearning() {

		return BASE_URL + "/" + "AddSLComments";

	}

	public static String getAddSharedLearningUrl() {

		return BASE_URL + "/" + "AddSharedLearning";

	}

	// GetInboxMessage_new, GetOtherMessage

	public static String getInboxMessages() {

		return BASE_URL + "/" + "GetInboxMessage_new";

	}

	public static String getOtherMessages() {

		return BASE_URL + "/" + "GetOtherMessage";

	}

	// Update_Message_flag
	public static String getUpdateMessageStatus() {

		return BASE_URL + "/" + "Update_Message_flag";

	}

	// getCaseStudyByDivDesg

	public static String getCaseStudy() {

		return BASE_URL + "/" + "getCaseStudyByDivDesg";

	}

	// AddStarToCaseStudy
	public static String AddStarToCaseStudy() {

		return BASE_URL + "/" + "AddStarToCaseStudy";

	}

	// CS_AddLikeCountForComment

	public static String AddLikeToCaseStudyComment() {

		return BASE_URL + "/" + "CS_AddLikeCountForComment";

	}

	// AddCSComment
	public static String AddCSComment() {

		return BASE_URL + "/" + "AddCSComment";

	}

	// GetEpollFrontend

	public static String getEPoll() {

		return BASE_URL + "/" + "GetEpollFrontend";

	}

	//
	public static String getsubmitEPollResult() {

		return BASE_URL + "/" + "GetEpollResult";

	}

	// GetWocktubeForFrontend

	public static String getWockTube() {

		return BASE_URL + "/" + "GetWocktubeForFrontend";

	}

	// GetFAQForFrontendByDivision

	public static String getFAQ() {

		return BASE_URL + "/" + "GetFAQForFrontendByDivision";

	}

	// App_GetPerformanceWall

	public static String getPerformanceWall() {

		return BASE_URL + "/" + "App_GetPerformanceWall";

	}

	// GetSharedLearningForFrontend

	public static String getSharedLearning() {

		return BASE_URL + "/" + "GetSharedLearningForFrontend";

	}

	// AddLikeCountForSL

	public static String AddLikeCountForSL() {

		return BASE_URL + "/" + "AddLikeCountForSL";

	}

	// App_MilestoneAllCertificate

	public static String getCurriculumAssistance() {

		return BASE_URL + "/" + "App_MilestoneAllCertificate";

	}

	// Milestone_PendingIndividualAsst
	public static String getPendingAssistance() {

		return BASE_URL + "/" + "Milestone_PendingIndividualAsst";

	}

	// Milestone_PendingMISCAsst

	public static String getMilestone_PendingMISCAsst() {

		return BASE_URL + "/" + "Milestone_PendingMISCAsst";

	}

	// Milestone_PendingCourseAsst

	public static String getMilestone_PendingCourseAsst() {

		return BASE_URL + "/" + "Milestone_PendingCourseAsst";

	}

	// Milestone_PendingWocktubeAsst
	public static String getMilestone_PendingWocktubeAsst() {

		return BASE_URL + "/" + "Milestone_PendingWocktubeAsst";

	}

	public static String getInductionAssessment() {

		return BASE_URL + "/" + "mobile_Induction_GetAssessment";

	}

	public static String getInductionAssessmentQuestion() {

		return BASE_URL + "/" + "mobile_Induction_Get_Assessment_questions";

	}

	public static String submitInductionAssessmentResult() {

		return BASE_URL + "/" + "mobile_Induction_Insert_Assessment_Result";

	}

	public static String getIndividualAssessment() {

		return BASE_URL + "/" + "mobile_individual_GetAssessment";

	}

	public static String getPREPOSTAssessment() {

		return BASE_URL + "/" + "mobile_PREPOST_GetAssessment";

	}

	public static String getIndividualAssessmentQuestion() {

		return BASE_URL + "/" + "mobile_Induction_Get_Assessment_questions";

	}

	public static String submitIndividualAssessmentResult() {

		return BASE_URL + "/" + "mobile_individual_Insert_Assessment_Result";

	}

	public static String getWocktubeAssessment() {

		return BASE_URL + "/" + "mobile_wocktube_GetAssessment";

	}

	public static String getWocktubeAssessmentQuestion() {

		return BASE_URL + "/" + "mobile_Induction_Get_Assessment_questions";

	}

	public static String submitWocktubeAssessmentResult() {

		return BASE_URL + "/" + "mobile_wocktube_Assessment_Result";

	}

	public static String getCertificationAssessment() {

		return BASE_URL + "/" + "mobile_certification_assessment";

	}

	public static String submitCertificateAssessmentResult() {

		return BASE_URL + "/" + "mobile_certification_Insert_Assessment_Result";

	}

	public static String getCourseAssessment() {

		return BASE_URL + "/" + "mobile_My_Learning_GetAssessment";

	}

	public static String submitCourseAssessmentResult() {

		return BASE_URL + "/" + "mobile_My_Learning_Insert_Assessment_Result";

	}

	public static String getMISCAssessment() {

		return BASE_URL + "/" + "mobile_Misc_GetAssessment";

	}

	// mobile_Misc_questions_by_assessment_id

	public static String getMISCQue() {

		return BASE_URL + "/" + "mobile_Misc_questions_by_assessment_id";

	}

	public static String submitMISCAssessmentResult() {

		return BASE_URL + "/" + "mobile_Misc_Insert_Assessment_Result";

	}

	// http://training.wockhardt.com/Webservice.asmx/SaveSLImageFile_New

	public static String imageUpload() {

		return "http://training.wockhardt.com/Webservice.asmx/SaveSLImageFile_New";

	}
}
