package com.o2m.gsk.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

import android.content.Context;
import android.util.Log;

public class CommunicationService {

	private BasicHttpParams httpParams;
	private DefaultHttpClient client;
	private String TAG = "CommunicationService";
	private HttpPost request;
	private BasicHttpResponse httpresponse;
	private BufferedReader in;

	public String sendSyncRequest(String postUrl, String postData,
			Context context) {
		return this.sendSyncRequestWithToken(postUrl, postData, "", context);
		/*
		 * Log.i(TAG, "postUrl : " + postUrl); Log.i(TAG, "postData : " +
		 * postData); StringBuffer sb = new StringBuffer("");
		 * 
		 * try { httpParams = new BasicHttpParams();
		 * HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
		 * HttpConnectionParams.setSoTimeout(httpParams, 60000); client = new
		 * DefaultHttpClient(httpParams);
		 * 
		 * 
		 * request = new HttpPost(postUrl); request.addHeader("Content-type",
		 * "application/json"); request.addHeader("Authorization",
		 * "Basic Z3Nrc3RhZ2luZzc6SGVhbHRoNEFsbA==");
		 * //request.addHeader("Content-type",
		 * "application/x-www-form-urlencoded");
		 * 
		 * request.setEntity(new StringEntity(postData));
		 * 
		 * httpresponse = (BasicHttpResponse) client.execute(request);
		 * 
		 * 
		 * in = new BufferedReader(new
		 * InputStreamReader(httpresponse.getEntity().getContent()));
		 * 
		 * String line = "";
		 * 
		 * while ((line = in.readLine()) != null) { sb.append(line); }
		 * in.close();
		 * 
		 * } catch (UnsupportedEncodingException e) { Log.e(TAG,
		 * " sendRequest Exception 1 " + e.getMessage()); } catch
		 * (ClientProtocolException e) { Log.e(TAG, " sendRequest Exception 2 "
		 * + e.getMessage()); } catch (IllegalStateException e) { Log.e(TAG,
		 * " sendRequest Exception 3 " + e.getMessage()); } catch (IOException
		 * e) { Log.e(TAG, " sendRequest Exception 4 " + e.getMessage()); }
		 * 
		 * 
		 * String response = sb.toString(); //response = response.trim(); //
		 * response = response.substring(1, response.length()-1); // response =
		 * response.replace("\\", ""); Log.i(TAG,
		 * "response from server   :::::::     " + response);
		 * 
		 * 
		 * 
		 * 
		 * return response;
		 */
	}

	public String sendSyncRequestWithToken(String postUrl, String postData,
			String token, Context context) {

		Log.i(TAG, "postUrl : " + postUrl);
		Log.i(TAG, "postData : " + postData);
		Log.i(TAG, "token : " + token);

		if (postUrl.contains("https")) {
			
			return this.sendSSLRequest(postUrl, postData, token, context);
			

		} else {

			StringBuffer sb = new StringBuffer("");

			try {
				httpParams = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
				HttpConnectionParams.setSoTimeout(httpParams, 60000);
				client = new DefaultHttpClient(httpParams);

				request = new HttpPost(postUrl);
				request.addHeader("Content-type", "application/json");
				request.addHeader("Token", token);
				request.addHeader("Authorization",
						"Basic Z3Nrc3RhZ2luZzc6SGVhbHRoNEFsbA==");

				// request.addHeader("Content-type",
				// "application/x-www-form-urlencoded");

				request.setEntity(new StringEntity(postData));

				httpresponse = (BasicHttpResponse) client.execute(request);

				in = new BufferedReader(new InputStreamReader(httpresponse
						.getEntity().getContent()));

				String line = "";

				while ((line = in.readLine()) != null) {
					sb.append(line);
				}
				in.close();

			} catch (UnsupportedEncodingException e) {
				Log.e(TAG, " sendRequest Exception 1 " + e.getMessage());
			} catch (ClientProtocolException e) {
				Log.e(TAG, " sendRequest Exception 2 " + e.getMessage());
			} catch (IllegalStateException e) {
				Log.e(TAG, " sendRequest Exception 3 " + e.getMessage());
			} catch (IOException e) {
				Log.e(TAG, " sendRequest Exception 4 " + e.getMessage());
			}

			String response = sb.toString();
			// response = response.trim();
			// response = response.substring(1, response.length()-1);
			// response = response.replace("\\", "");
			Log.i(TAG, "Status Code : "
					+ httpresponse.getStatusLine().getStatusCode()
					+ "\nresponse from server   :::::::     " + response);

			return response;
		}
		
	}

	public String sendSSLRequest(String postUrl, String postData, String token,
			Context _context) {

		boolean isLogEnabled = true;
		int timeout = 120 * 1000;
		boolean isSSLPinningEnable = true;
		ArrayList<X509Certificate> x509Certificates = SSLCertificate
				.getInstance(_context).getDeviceCertificate();
		boolean allowInvalidHostName = false;

		Log.i(TAG, "postUrl : " + postUrl);
		Log.i(TAG, "postData : " + postData);
		Log.i(TAG, "token : " + token);

		HttpsURLConnection httpsconnection = null;

		int responseCode = 0;

		if (allowInvalidHostName) { // allow Invalid Hosts..
			// Host name verifier
			if (isLogEnabled) {
				Log.v("", "allowInvalidHostName Enabled");
			}
			HttpsURLConnection
			.setDefaultHostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String hostname,
						SSLSession session) {
					return true;
				}
			});
			// End
		}
		// String requestBody = "";

		/*
		 * HashMap<String, String> headerFields = null; if (reqHeader != null) {
		 * requestBody = reqHeader.getRequestBody(); headerFields =
		 * reqHeader.getHeaderFields(); } Response responseObj = new
		 * Response(FAILURE, UNKNOWN_ERROR, false, "", 0);
		 */String response = "";
		 StringBuffer responseBuffer = new StringBuffer("");

		 try {

			 URL url = new URL(postUrl);
			 httpsconnection = (HttpsURLConnection) url.openConnection();
			 // httpsconnection.setRequestProperty(field, newValue)
			 httpsconnection.setRequestProperty("Content-type",
					 "application/json");
			 if (!token.isEmpty() && !token.trim().equalsIgnoreCase(""))
				 httpsconnection.setRequestProperty("Token", token);
			 httpsconnection.setRequestProperty("Authorization",
					 "Basic Z3Nrc3RhZ2luZzc6SGVhbHRoNEFsbA==");

			 /*
			  * if (headerFields != null) { if (headerFields.size() != 0) { for
			  * (String headerKey : headerFields.keySet()) {
			  * httpsconnection.setRequestProperty(headerKey,
			  * headerFields.get(headerKey));
			  * 
			  * if (isLogEnabled) { MobiculeLogger.debug(HEADER_KEY, headerKey);
			  * MobiculeLogger.debug(HEADER_VALUE, headerFields.get(headerKey));
			  * } } } }
			  */
			 // check whether user provided timeout time is
			 // less or equals to zero
			 httpsconnection.setReadTimeout(timeout);
			 // Sets the maximum time to wait for an input stream read to
			 // complete
			 // before giving up. Reading will fail with a
			 // SocketTimeoutException
			 // if the timeout
			 // elapses before data becomes available. The default value of 0
			 // disables read timeouts;
			 // read attempts will block indefinitely.
			 httpsconnection.setConnectTimeout(timeout);

			 httpsconnection.setDoInput(true);

			 /*
			  * if (isLogEnabled) { MobiculeLogger.debug(URL, urlString);
			  * MobiculeLogger.debug(TIMEOUT_VALUE, "" + timeout); }
			  */

			 /**
			  * Hand-shake before https request.. i.e before writing into stream
			  * in case of Post request.
			  */
			 addSSLHandshakeConfigurations(httpsconnection, x509Certificates,
					 isSSLPinningEnable, isLogEnabled);

			 httpsconnection.setDoOutput(true);
			 // httpsconnection.setRequestMethod();

			 /*
			  * // decide get or post request depending on body. if
			  * (!requestBody.equalsIgnoreCase("")) {
			  * 
			  * if (isLogEnabled) { MobiculeLogger.debug(REQUESTBODY,
			  * requestBody); }
			  * 
			  * httpsconnection.setDoOutput(true);
			  * httpsconnection.setRequestMethod(POST);
			  *//**
			  * Check if isCommpression is enabled?
			  */
			 /*
			  * 
			  * if (isCompressionEnabled) {
			  * httpsconnection.setRequestProperty(COMPRESSED_HEADER_KEY,
			  * COMPRESSED_ENABLE_VALUE); compressedRequest = NetworkUtility
			  * .requestCompression(requestBody); if (isLogEnabled) {
			  * MobiculeLogger.debug(COMPRESSION_ENABLED); }
			  *//**
			  * logic to generate digest goes here
			  */
			 /*
			  * if (isDigestEnabled) {
			  * 
			  * String digestValue = getRequestDigest(requestBody, context,
			  * isLogEnabled, packageName);
			  * 
			  * httpsconnection.setRequestProperty(DIGEST_HEADER_KEY,
			  * digestValue); if (isLogEnabled) {
			  * MobiculeLogger.debug(REQUESTDIGEST, digestValue); }
			  * 
			  * }
			  * 
			  * writeCompressedRequestInNetworkConnection(httpsconnection,
			  * compressedRequest);
			  * 
			  * } else {
			  *//**
			  * logic to generate digest goes here
			  */
			 /*
			  * if (isDigestEnabled) {
			  * 
			  * String digestValue = getRequestDigest(requestBody, context,
			  * isLogEnabled, packageName);
			  * 
			  * httpsconnection.setRequestProperty(DIGEST_HEADER_KEY,
			  * digestValue); if (isLogEnabled) {
			  * MobiculeLogger.debug(REQUESTDIGEST, digestValue); } }
			  * 
			  * 30/12/2015 mail Code replaced for to support UTF-8 ----
			  * 
			  * 
			  * Directed Rohan Pathak DataOutputStream postdataOutputStream = new
			  * DataOutputStream( httpsconnection.getOutputStream());
			  * postdataOutputStream.writeBytes(postData);
			  * postdataOutputStream.flush(); postdataOutputStream.close();
			  * 
			  * End
			  * 
			  * writePlainRequestInNetworkConnection(httpsconnection,
			  * requestBody);
			  * 
			  * } } else { httpsconnection.setRequestMethod(GET); }
			  */

			 if (!postData.trim().equalsIgnoreCase("")) {
				 OutputStream outputStream = httpsconnection.getOutputStream();
				 BufferedWriter writer = new BufferedWriter(
						 new OutputStreamWriter(outputStream, "UTF-8"));
				 writer.write(postData);
				 writer.flush();
				 writer.close();
				 outputStream.close();
			 }
			 httpsconnection.connect();
			 responseCode = httpsconnection.getResponseCode();

			 if (responseCode == HttpsURLConnection.HTTP_OK
					 || responseCode == HttpURLConnection.HTTP_CREATED
					 || responseCode == HttpURLConnection.HTTP_NOT_AUTHORITATIVE) { // success

				 BufferedReader inputBufferedReader = new BufferedReader(
						 new InputStreamReader(httpsconnection.getInputStream(),
								 "UTF-8"));
				 String inputLine = "";
				 while ((inputLine = inputBufferedReader.readLine()) != null) {
					 responseBuffer.append(inputLine);
				 }
				 response = responseBuffer.toString();

				 if (isLogEnabled) {
					 Log.v("SERVER_RESPONSE_MESSAGE", response);
				 }

				 /*
				  * closing inputBufferedReader
				  */
				 inputBufferedReader.close();
			 }

			 return response;

		 } catch (HttpHostConnectException httpHostConnectException) {
			 // This Exception happens when you attempt to open a TCP connection
			 // from an IP address where there is nothing currently listening for
			 // connection.
			 Log.e("", "HttpHostConnectException", httpHostConnectException);

		 } catch (UnknownHostException unknownHostException) {
			 // This Exception occurs when you are trying to connect to a remote
			 // host using Host name but IP address of that Host can not be
			 // resolved.
			 Log.e("", "unknownHostException", unknownHostException);

		 } catch (ConnectTimeoutException connectTimeoutException) {

			 Log.e("", "connectTimeoutException", connectTimeoutException);

		 } catch (SocketTimeoutException socketTimeoutException) {

			 Log.e("", "socketTimeoutException", socketTimeoutException);

		 } catch (ConnectException connectException) {

			 Log.e("", "connectException", connectException);

		 } catch (IOException ioException) {
			 Log.e("", "ioException", ioException);

		 } catch (Exception exception) {

			 Log.e("", "exception", exception);

		 }
		 return "";
	}

	private void addSSLHandshakeConfigurations(HttpsURLConnection connection,
			ArrayList<X509Certificate> x509Certificates,
			boolean isSSLPinningEnable, boolean isLogEnabled)
					throws NoSuchAlgorithmException, KeyManagementException {

		if (isLogEnabled) {
			Log.v("TAG", "addSSLHandshakeConfigurations()");
		}
		// Compare device and server certificates.
		TrustManager[] tm = { MobiCustomSSLTrustManager.getInstance(
				x509Certificates, isSSLPinningEnable, isLogEnabled) };
		/**
		 * Enable TLS version 1.1 and 1.2 for API level 16- 20
		 */
		int currentAPILevel = android.os.Build.VERSION.SDK_INT;
		if (currentAPILevel >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
			/*
			 * if (!_isTLS1_2Enabled) {
			 *//**
			 * Enabling SSL Protocols
			 */
			/*
			 * TLSSocketFactory socketFactory = new TLSSocketFactory(tm);
			 * connection.setSSLSocketFactory(socketFactory); } else {
			 */
			/**
			 * SSL Protocols Already Enabled using Default SSL Protocols
			 */
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, tm, null);
			connection.setSSLSocketFactory(sslContext.getSocketFactory());

		} else {
			/**
			 * Default SSL Protocols
			 */
			SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, tm, null);
			connection.setSSLSocketFactory(sslContext.getSocketFactory());
		}

	}

}
