package com.o2m.gsk.utility;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class URLParams {
	
	static URLParams instance = null; 
	private JSONObject loginResponse, userResponse;
	private String employee_id, user_id, div_code, desg_code, level_code, zone_code,region_code,role_id, doj, old_user_id,name,year, login_level_code ;
	private HashMap<String, String> divMap;

	synchronized public static URLParams getInstance()
	{
		
		
		if(instance == null)
		{
			instance = new URLParams();
		}
		
		return instance;
		
	}
	
	private  URLParams() {
		
	}
	
	public void setLoginResponse(JSONObject loginResponse) {
		
		this.loginResponse = loginResponse;
		
		try {
			
			employee_id = loginResponse.getString("employee_id");
			user_id = loginResponse.getString("user_id");
			div_code = loginResponse.getString("div_code");
			desg_code = loginResponse.getString("desg_code");
			level_code = loginResponse.getString("level_code"); 
			login_level_code = level_code;
			zone_code = loginResponse.getString("zone_code");
			region_code = loginResponse.getString("region_code");
			role_id = loginResponse.getString("role_id");
			name = loginResponse.getString("name");
			doj = loginResponse.getJSONArray("doj_div_level").getJSONObject(0).getString("date");
			//old_user_id = loginResponse.getJSONArray("Old Users").getJSONObject(0).getString("user_id");;
			year = loginResponse.getJSONArray("doj_div_level").getJSONObject(0).getString("year");
			JSONArray olduserArray = loginResponse.getJSONArray("Old Users");
            String temp = "";
            for (int i = 0; i < olduserArray.length(); i++) {
                temp = new StringBuilder(String.valueOf(temp)).append(olduserArray.getJSONObject(i).getString("user_id")).toString();
                if (i != olduserArray.length() - 1) {
                    temp = new StringBuilder(String.valueOf(temp)).append(",").toString();
                }
            }
            this.old_user_id = temp;
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void setUserResponse(JSONObject _userResponse) {
		
		this.userResponse = _userResponse;
		
		try {
			
			employee_id = _userResponse.getString("employee_id");
			user_id = _userResponse.getString("user_id");
			div_code = _userResponse.getString("div_code");
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


	public String getEmployee_id() {
		return employee_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getDiv_code() {
		return div_code;
	}

	public String getDesg_code() {
		return desg_code;
	}

	public String getLevel_code() {
		return level_code;
	}

	public String getZone_code() {
		return zone_code;
	}

	public String getRegion_code() {
		return region_code;
	}

	public String getRole_id() {
		return role_id;
	}

	public String getDoj() {
		//return doj;
		// {"d":"{\"Status\":\"success\",\"message\":\"\",\"data\":\"Table\":[{\"Old_ids\":\"9825\",\"doj_div_level\":\"11/19/2014 12:56:32 PM\"}]}"}
		//11/19/2014 12:56:32 PM
		//9825
		
		//return "11/19/2014 12:56:32 PM";
		return doj;
	}

	public String getOld_user_id() {
		
		//return "9825";
		return old_user_id;
	}

	public HashMap<String, String> getDivMap() {
		return divMap;
	}

	public void setDivMap(HashMap<String, String> divMap) {
		this.divMap = divMap;
	}

	public void setDiv_code(String div_code) {
		this.div_code = div_code;
	}

	public void setLevel_code(String level_code) {
		this.level_code = level_code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getLogin_level_code() {
		return login_level_code;
	}

	public void setLogin_level_code(String login_level_code) {
		this.login_level_code = login_level_code;
	}

}
