package com.o2m.gsk.utility;


import com.o2m.gsk.R;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class CacheMemory {

	public static final String ONLINE_CHALLENGE_YES = "YES";
	public static final String ONLINE_CHALLENGE_NO = "NO";
	public static final boolean DEBUG_MODE = false;
	public static Activity mActivity;
	public static final String PREFS_NAME = "MyApp_Settings";
	public static  String TOKEN = "MyApp_Settings";
	
	public static String getToken(Activity _activity) {

		
		SharedPreferences settings = _activity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		 
	
	     String token = settings.getString("Token", "");
	    return token;
	}

	public static void setToken(Activity _activity, String _token) {

			
		SharedPreferences settings = _activity.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		 
	      Editor editor = settings.edit();
	     editor.putString("Token", _token);
	     editor.commit();
	}

	public static String getUserId(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("UserId");
		return token;

	}

	public static void setUserId(Activity _activity, String _token) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("UserId", _token);

	}

	public static String getTherapy(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("Therapy");
		return token;

	}

	public static void setTherapy(Activity _activity, String _token) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("Therapy", _token);

	}
	
	public static String getPreviousPoints(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("PrevPoints");
		return token;

	}

	public static void setPreviousPoints(Activity _activity, String _prevPoints) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("PrevPoints", _prevPoints);

	}

	public static String getPoints(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("Points");
		return token;

	}

	public static void setPoints(Activity _activity, String _token) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("Points", _token);

	}

	public static String getConnectionId(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("ConnectionId");
		return token;

	}

	public static void setConnectionId(Activity _activity, String _ConnectionId) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("ConnectionId", _ConnectionId);

	}

	public static String getRequestCount(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("RequestCount");
		return token;

	}

	public static void setRequestCount(Activity _activity, String _ConnectionId) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("RequestCount", _ConnectionId);

	}

	public static String getOnlineChallengeStatus(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("OC");
		return token;

	}

	public static void setOnlineChallengeStatus(Activity _activity,
			String _ConnectionId) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("OC", _ConnectionId);

	}
	
	public static String getTopicName(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("topicName");
		return token;

	}

	public static void setTopicName(Activity _activity,
			String _topicName) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("topicName", _topicName);

	}
	
	public static String getUserDetails(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("UserDetails");
		return token;

	}

	public static void setUserDetails(Activity _activity,
			String _userDetails) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("UserDetails", _userDetails);

	}
	
	public static String getLevelName(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("LevelName");
		return token;

	}

	public static void setLevelName(Activity _activity,
			String _userDetails) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("LevelName", _userDetails);

	}
	
	public static String getSubTopicId(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("SubTopicId");
		return token;

	}

	public static void setSubTopicId(Activity _activity,
			String _subTopicId) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("SubTopicId", _subTopicId);

	}

	public static String getOTPToken(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("OTPToken");
		return token;

	}

	public static void setOTPToken(Activity _activity,
			String _OTPToken) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("OTPToken", _OTPToken);

	}
		
	public static String getBronzeFlag(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("BronzeFlag");
		return token;

	}

	public static void setBronzeFlag(Activity _activity,
			String _flag) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("BronzeFlag", _flag);

	}
	
	public static String getSilverFlag(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("SilverFlag");
		return token;

	}

	public static void setSilverFlag(Activity _activity,
			String _flag) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("SilverFlag", _flag);

	}
	
	public static String getGoldFlag(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("GoldFlag");
		return token;

	}

	public static void setGoldFlag(Activity _activity,
			String _flag) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("GoldFlag", _flag);

	}
	
	public static String getPlatinumFlag(Activity _activity) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		String token = cryptedConfiguration.getString("PlatinumFlag");
		return token;

	}

	public static void setPlatinumFlag(Activity _activity,
			String _flag) {

		CryptedConfiguration cryptedConfiguration = new CryptedConfiguration(_activity,
				_activity.getResources().getString(R.string.crypted_configuration), _activity.getResources().getString(R.string.key), true);
		cryptedConfiguration.put("PlatinumFlag", _flag);

	}

}
