package com.o2m.gsk.myaccount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.RegistrationActivity;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class UpdateAccountFragment extends Fragment implements
View.OnClickListener {

	protected String TAG;
	private JSONObject userDetailsObj;

	private EditText firstnameET, lastnameET, emailET, mobileET, locationET;
	//private AutoCompleteTextView specialityET;
	private Button updateProfileBtn, cancelBtn;
	private HashMap<String, String> therapyMap;
	private RadioButton maleRB, femaleRB;
	private Spinner specialitySpinner;
	private ArrayList<String> locationList = new ArrayList<String>();

	public UpdateAccountFragment(JSONObject _userObj) {
		// TODO Auto-generated constructor stub
		this.userDetailsObj = _userObj;

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.update_account_fragment,
				container, false);

		firstnameET = (EditText) rootView.findViewById(R.id.firstNameET);
		lastnameET = (EditText) rootView.findViewById(R.id.lastNameET);
		//specialityET = (AutoCompleteTextView) rootView.findViewById(R.id.spi);
		specialitySpinner = (Spinner) rootView.findViewById(R.id.speciality_spinner);
		emailET = (EditText) rootView.findViewById(R.id.emailIdET);
		mobileET = (EditText) rootView.findViewById(R.id.mobileET);
		locationET = (EditText) rootView.findViewById(R.id.locationET);
		maleRB = (RadioButton) rootView.findViewById(R.id.maleRB);
		femaleRB = (RadioButton) rootView.findViewById(R.id.femaleRB);
		updateProfileBtn = (Button) rootView
				.findViewById(R.id.updateProfileBtn);
		cancelBtn = (Button) rootView.findViewById(R.id.cancelBtn);
		therapyMap = new HashMap<String, String>();

		updateProfileBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);

		try {
			firstnameET.setText("" + this.userDetailsObj.getString("fname"));
			lastnameET.setText("" + this.userDetailsObj.getString("lname"));
		//	specialityET.setText("" + this.userDetailsObj.getString("Therapy"));
			if (this.userDetailsObj.getString("Gender").equalsIgnoreCase("1"))
				maleRB.setChecked(true);
			else
				femaleRB.setChecked(true);

			emailET.setText("" + this.userDetailsObj.getString("email"));
			mobileET.setText("" + this.userDetailsObj.getString("contact"));
			locationET.setText("" + this.userDetailsObj.getString("address"));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		locationET.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				/*
				 * if(count == 3) { fetchLocationOnline(); }
				 */
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				if (s.length() == 3) {
					fetchLocationOnline();
				}
			}
		});

		 this.fetchTherapyMaster();

		return rootView;
	}

	protected void fetchLocationOnline() {
		// TODO Auto-generated method stub
		new ApplicationAsk(UpdateAccountFragment.this.getActivity(),"Fetching location",
				new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {
					JSONObject loginResponseObj = new JSONObject(resp);
					String status = loginResponseObj
							.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = loginResponseObj
								.getJSONArray("data");

						if (dataObj.length() > 0) {
							for (int i = 0; i < dataObj.length(); i++) {
								JSONObject therapyObj = dataObj
										.getJSONObject(i);
								locationList.add(therapyObj
										.getString("Name"));
							}
							Log.v("", "" + locationList.toString());

							showLocationList();
						} else {
							Toast.makeText(
									UpdateAccountFragment.this
									.getActivity(),
									"Location Not Found",
									Toast.LENGTH_LONG).show();
						}

					} else {
						Toast.makeText(
								UpdateAccountFragment.this
								.getActivity(),
								""
										+ loginResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}
			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject locObj = new JSONObject();
				try {
					locObj.put("Name", ""
							+ locationET.getText().toString());
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String locationURL = URLGenerator.GetLocationUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(locationURL,
						locObj.toString(), UpdateAccountFragment.this.getActivity());

			}
		}).execute();

	}

	protected void showLocationList() {
		// TODO Auto-generated method stub
		AlertDialog.Builder b = new AlertDialog.Builder(
				UpdateAccountFragment.this.getActivity());
		// AlertDialog.Builder b = new Builder(this);
		b.setTitle("Select Location");
		// String[] types = {"By Zip", "By Category"};
		b.setItems(locationList.toArray(new String[0]), new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();
				locationET.setText(locationList.get(which));

			}

		});
		b.show();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == cancelBtn) {
			// removeCurrentFragment();
			getFragmentManager().popBackStack();

		} else if (arg0 == updateProfileBtn) {

			try {

				if (!this.userDetailsObj.getString("contact").equals(
						mobileET.getText().toString().trim())) {
					// move to otp verification
					return;
				}

				String contact = mobileET.getText().toString().trim();
				String fname = firstnameET.getText().toString().trim();
				String lname = lastnameET.getText().toString().trim();
				String address = locationET.getText().toString().trim();
				String therapy = therapyMap.get(specialitySpinner
						.getSelectedItem().toString().trim());
				String gender = "1";

				if (femaleRB.isChecked())
					gender = "2";
				this.updateProfile(contact, fname, lname, address, therapy, gender);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private void updateProfile(final String contact, final String fname, final String lname, final String address,
			final String therapy, final String gender) {
		// TODO Auto-generated method stub
		new ApplicationAsk(UpdateAccountFragment.this.getActivity(), "Updating profile",
				new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub

				try {

					JSONObject registraionResponseObj = new JSONObject(
							resp);

					String status = registraionResponseObj
							.getString("status");

					if (status.equalsIgnoreCase("success")) {

						Toast.makeText(
								UpdateAccountFragment.this
								.getActivity(),
								"Profile updated succesfully",
								Toast.LENGTH_LONG).show();

						String userDetails = CacheMemory
								.getUserDetails(getActivity());

						try {
							JSONObject userDetailsObj = new JSONObject(
									userDetails);

							userDetailsObj.put("contact", contact.trim());
							userDetailsObj.put("fname",
									fname.trim());
							userDetailsObj.put("lname",
									lname.trim());
							userDetailsObj.put("address",
									address.trim());
							userDetailsObj.put("Gender", gender);

							CacheMemory.setUserDetails(getActivity(),
									userDetailsObj.toString());

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						getFragmentManager().popBackStack();

					} else {
						Toast.makeText(
								UpdateAccountFragment.this
								.getActivity(),
								""
										+ registraionResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject registerObj = new JSONObject();
				try {
					//registerObj.put("contact", contact.trim());
					registerObj.put("fname", fname.trim());
					registerObj.put("lname", lname.trim());
					registerObj.put("address", address.trim());
					registerObj.put("Gender", gender);

					Log.v("therapy",
							specialitySpinner.getSelectedItemPosition()
									+ ""
									+ therapyMap.toString()
									+ ""
									+ therapyMap.get(specialitySpinner
											.getSelectedItem().toString()
											.trim()));
					registerObj.put("Therapy", therapy);

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String registerURL = URLGenerator.getUpdateProfileUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(
						registerURL, registerObj.toString(),
						CacheMemory.getToken(UpdateAccountFragment.this
								.getActivity()),UpdateAccountFragment.this.getActivity());

			}

		}).execute();

	}

	private void fetchTherapyMaster() {

		new ApplicationAsk(UpdateAccountFragment.this.getActivity(),"Fetching therapy",
				new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {
					JSONObject loginResponseObj = new JSONObject(resp);

					String status = loginResponseObj
							.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = loginResponseObj
								.getJSONArray("data");

						for (int i = 0; i < dataObj.length(); i++) {
							JSONObject therapyObj = dataObj
									.getJSONObject(i);
							therapyMap.put(
									therapyObj.getString("Name"),
									therapyObj.getString("TherapyId"));
						}
						Log.v("", "" + therapyMap.toString());
						String[] mKeys = therapyMap.keySet().toArray(
								new String[therapyMap.size()]);
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(
								UpdateAccountFragment.this
								.getActivity(),
								android.R.layout.select_dialog_item,
								mKeys);
						specialitySpinner.setAdapter(adapter);
						List<String> stringList = new ArrayList<String>(Arrays.asList(mKeys));
						Log.v("SetSelection", "List :" + stringList + "\nTherapy " + UpdateAccountFragment.this.userDetailsObj.getString("Therapy"));
						stringList.indexOf("" + UpdateAccountFragment.this.userDetailsObj.getString("Therapy"));
						//specialitySpinner.setSelection()

					} else {
						Toast.makeText(
								UpdateAccountFragment.this
								.getActivity(),
								""
										+ loginResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}
			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				String loginURL = URLGenerator.getTherapyUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequest(loginURL, "", UpdateAccountFragment.this.getActivity());

			}
		}).execute();

	}

	public void removeCurrentFragment() {
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		Fragment currentFrag = getFragmentManager().findFragmentById(
				R.id.content_frame);

		if (currentFrag != null)
			transaction.remove(currentFrag);

		transaction.commit();

	}

}
