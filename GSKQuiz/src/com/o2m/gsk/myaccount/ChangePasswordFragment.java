package com.o2m.gsk.myaccount;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class ChangePasswordFragment extends Fragment implements OnClickListener {

	protected String TAG;

	private EditText oldPassET, newPassET, confirmPassET;

	private Button updateBtn, cancelBtn;

	public ChangePasswordFragment() {
		// TODO Auto-generated constructor stub

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.change_password_fragment,
				container, false);
		oldPassET = (EditText) rootView.findViewById(R.id.oldPassET);
		newPassET = (EditText) rootView.findViewById(R.id.newPassET);
		confirmPassET = (EditText) rootView.findViewById(R.id.confirmPassET);

		updateBtn = (Button) rootView.findViewById(R.id.updateProfileBtn);
		cancelBtn = (Button) rootView.findViewById(R.id.cancelBtn);
		updateBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);

		return rootView;
	}

	@Override
	public void onClick(View arg0) {
		if (arg0 == updateBtn) {
			// TODO Auto-generated method stub
			final String oldPass = oldPassET.getText().toString().trim();
			final String newPass = newPassET.getText().toString().trim();
			final String confirmPass = confirmPassET.getText().toString()
					.trim();
			if (this.validate(oldPass, newPass, confirmPass)) {
				new ApplicationAsk(ChangePasswordFragment.this.getActivity(), "Saving new password",
						new ApplicationService() {
							private String resp;

							@Override
							public void postExecute() {
								// TODO Auto-generated method stub

								try {

									JSONObject changePassResponseObj = new JSONObject(
											resp);
									String status = changePassResponseObj
											.getString("status");

									if (status.equalsIgnoreCase("success")) {

										Toast.makeText(
												ChangePasswordFragment.this
														.getActivity(),
												"Password updated succesfully",
												Toast.LENGTH_LONG).show();

										getFragmentManager().popBackStack();

									} else {
										Toast.makeText(
												ChangePasswordFragment.this
														.getActivity(),
												""
														+ changePassResponseObj
																.getString("message"),
												Toast.LENGTH_LONG).show();
									}

								} catch (JSONException e) {

									e.printStackTrace();
								}

							}

							@Override
							public void execute() {
								// TODO Auto-generated method stub
								JSONObject changePassObj = new JSONObject();
								try {
									changePassObj.put("OldPass", oldPass);
									changePassObj.put("password", confirmPass);

								} catch (JSONException e) {

									e.printStackTrace();
								}

								String changePassURL = URLGenerator
										.getChangePasswordUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
								CommunicationService commService = new CommunicationService();
								resp = commService.sendSyncRequestWithToken(
										changePassURL,
										changePassObj.toString(),
										CacheMemory
												.getToken(ChangePasswordFragment.this
														.getActivity()),ChangePasswordFragment.this
														.getActivity());

							}

						}).execute();

			}
		}

		if (arg0 == cancelBtn) {
			getFragmentManager().popBackStack();
		}

	}

	private boolean validate(String oldPass, String newPass, String confirmPass) {
		// TODO Auto-generated method stub

		if (oldPass.equalsIgnoreCase("")) {

			oldPassET.setError("Field cant be empty");
			return false;
			
		}
		if (newPass.equalsIgnoreCase("")) {

			newPassET.setError("Field cant be empty");
			return false;
		}
		if (confirmPass.equalsIgnoreCase("")) {
			
			confirmPassET.setError("Field cant be empty");
			return false;
		}

		if (!newPass.equalsIgnoreCase(confirmPass)) {
			Toast.makeText(getActivity(),
					" New and Confirm Password Mismatch ", Toast.LENGTH_SHORT)
					.show();
			return false;
		}

		return true;
	}

	

}
