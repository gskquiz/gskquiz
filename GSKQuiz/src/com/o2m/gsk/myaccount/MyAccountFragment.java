package com.o2m.gsk.myaccount;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.CacheMemory;

public class MyAccountFragment extends Fragment implements OnClickListener {

	protected String TAG;
	private JSONObject userDetailsObj;
	private TextView fullnameTV, specialityTV, genderTV, emailTV,
			mobileNumberTV, locationTV;
	private Button editProfileBtn, changePasswordBtn;
	private ImageView genderIconIV;

	public MyAccountFragment(JSONObject _userObj) {
		// TODO Auto-generated constructor stub
		this.userDetailsObj = _userObj;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.profile_fragment, container,
				false);

		fullnameTV = (TextView) rootView.findViewById(R.id.nameTV);
		specialityTV = (TextView) rootView.findViewById(R.id.specialityTV);
		genderTV = (TextView) rootView.findViewById(R.id.genderTV);
		emailTV = (TextView) rootView.findViewById(R.id.emailTV);
		mobileNumberTV = (TextView) rootView.findViewById(R.id.mobileTV);
		locationTV = (TextView) rootView.findViewById(R.id.locationTV);
		editProfileBtn = (Button) rootView.findViewById(R.id.editProfileBtn);
		changePasswordBtn = (Button) rootView
				.findViewById(R.id.changePasswordBtn);
		genderIconIV = (ImageView) rootView.findViewById(R.id.genderIcon);

		editProfileBtn.setOnClickListener(this);
		changePasswordBtn.setOnClickListener(this);

		try {
			fullnameTV.setText("" + this.userDetailsObj.getString("fname") + " " + this.userDetailsObj.getString("lname") );
			specialityTV.setText("" + this.userDetailsObj.getString("Therapy"));
			if (this.userDetailsObj.getString("Gender").equalsIgnoreCase("1"))
			{
				genderIconIV.setImageResource(R.drawable.avatar_m); 
				genderTV.setText("Male");
			}
			else
			{
				genderIconIV.setImageResource(R.drawable.avatar);
				genderTV.setText("Female");
			}
			emailTV.setText("" + this.userDetailsObj.getString("email"));
			mobileNumberTV.setText(""
					+ this.userDetailsObj.getString("contact"));
			locationTV.setText("" + this.userDetailsObj.getString("address"));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rootView;
	}

	public void refreshValues() {
		try {
			fullnameTV.setText("" + this.userDetailsObj.getString("fname") + " " + this.userDetailsObj.getString("lname") );
			specialityTV.setText("" + this.userDetailsObj.getString("Therapy"));
			if (this.userDetailsObj.getString("Gender").equalsIgnoreCase("1"))
				genderTV.setText("Male");
			else
				genderTV.setText("Female");
			emailTV.setText("" + this.userDetailsObj.getString("email"));
			mobileNumberTV.setText(""
					+ this.userDetailsObj.getString("contact"));
			locationTV.setText("" + this.userDetailsObj.getString("address"));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.v(TAG, "OnResume : MyAccountFragment");

		String userDetails = CacheMemory.getUserDetails(getActivity());

		try {
			this.userDetailsObj = new JSONObject(userDetails);
			refreshValues();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == editProfileBtn) {
			Fragment fragment = new UpdateAccountFragment(this.userDetailsObj);
			FragmentTransaction ft = getFragmentManager().beginTransaction();

			ft.addToBackStack("UpdateAccountFragment");
			ft.replace(R.id.content_frame, fragment, "UpdateAccountFragment");

			ft.commit();

		} else if (arg0 == changePasswordBtn) {

			Fragment fragment = new ChangePasswordFragment();
			FragmentManager fragmentManager = getFragmentManager();
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack("ChangePasswordFragment");
			ft.replace(R.id.content_frame, fragment, "ChangePasswordFragment");

			ft.commit();
		}

	}

}
