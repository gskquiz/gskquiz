	package com.o2m.gsk;

import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.CryptedConfiguration;
import com.o2m.gsk.utility.URLGenerator;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ForgotPassword extends Dialog implements
		android.view.View.OnClickListener {

	public Activity activity;
	public Dialog dialog;
	public Button continueBtn, cancelBtn;
	private EditText forgotPasswordET;

	public ForgotPassword(Activity context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.forgot_password);

		continueBtn = (Button) findViewById(R.id.fscontinuemBtn);
		// resendBtn = (Button) findViewById(R.id.verifyOTPBtn);
		forgotPasswordET = (EditText) findViewById(R.id.fpemailmobileET);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
		continueBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);

		// resendBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == cancelBtn) {
			this.dismiss();
		} else if (arg0 == continueBtn) {

			String email = forgotPasswordET.getText().toString().trim();
			if (email.equalsIgnoreCase("")) {
				/*Toast.makeText(activity, "Please enter valid email",
						Toast.LENGTH_SHORT).show();*/
				forgotPasswordET.setError("Please enter valid email");
				return;
			}

			new ApplicationAsk(activity, "Loading . .", new ApplicationService() {
				private String resp;

				@Override
				public void postExecute() {
					// TODO Auto-generated method stub
					try {

						JSONObject registraionResponseObj = new JSONObject(resp);
						String status = registraionResponseObj
								.getString("status");

						if (status.equalsIgnoreCase("success")) {
							
							
							
							JSONObject dataObj = registraionResponseObj
									.getJSONArray("data").getJSONObject(0);
							String UserId = dataObj.getString("UserId");
							CryptedConfiguration preferences = new CryptedConfiguration(
									activity,
									activity.getResources().getString(R.string.crypted_configuration), activity.getResources().getString(R.string.key),
									true);
							preferences.put("UserId", UserId);
							ForgotPassword.this.dismiss();
							VerifyOTP vp = new VerifyOTP(activity , UserId);
							vp.setCancelable(false);
							vp.show();

						} else {
							Toast.makeText(
									activity,
									""
											+ registraionResponseObj
													.getString("message"),
									Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {

						e.printStackTrace();
					}

				}

				@Override
				public void execute() {
					// TODO Auto-generated method stub
					JSONObject forgotPwdObj = new JSONObject();
					try {
						
						forgotPwdObj.put("email", forgotPasswordET.getText()
								.toString().trim());

					} catch (JSONException e) {

						e.printStackTrace();
					}

					String forgotPasswordURL = URLGenerator
							.getForgotPasswordURL();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(forgotPasswordURL,
							forgotPwdObj.toString(), ForgotPassword.this.activity);
				}

			}).execute();
		}

	}

}
