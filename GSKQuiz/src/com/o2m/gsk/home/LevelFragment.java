package com.o2m.gsk.home;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.challenge.ChallengeListFragment;
import com.o2m.gsk.challenge.QueListFragment;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class LevelFragment extends Fragment implements OnClickListener, OnItemClickListener {

	private Button beginnerBtn, intermediateBtn, advancedBtn;
	private GridView gridMenu;
	private JSONArray levelJsonArray;
	private String subTopicId;
	protected String TAG;
	public static final String BEGINNER = "DD896FBB-A01B-4B7C-BC19-D2397EFAA33B";
	public static final String INTERMEDIATE = "6D698E16-FFEE-4522-BBE8-7714B3795BB3";
	public static final String ADVANCED = "A3BAB958-7068-492C-8136-7501C6B06AEA";

	public LevelFragment(String _subTopicId) {
		// TODO Auto-generated constructor stub
		//this.levelJsonArray = _levelJsonArray;
		this.subTopicId = _subTopicId;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		refreshBeginnerLevel();
		
	}

	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.difficultylevel_fragment,
				container, false);
		beginnerBtn = (Button) rootView.findViewById(R.id.beginnerBtn);
		intermediateBtn = (Button) rootView.findViewById(R.id.intermediateBtn);
		advancedBtn = (Button) rootView.findViewById(R.id.advancedBtn);

		beginnerBtn.setOnClickListener(this);
		intermediateBtn.setOnClickListener(this);
		advancedBtn.setOnClickListener(this);
		beginnerBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
				R.drawable.white_line);
		intermediateBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		advancedBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		beginnerBtn.setTextColor(Color.parseColor("#ffffff"));
		intermediateBtn.setTextColor(Color.parseColor("#ff9a75"));
		advancedBtn.setTextColor(Color.parseColor("#ff9a75"));

		gridMenu = (GridView) rootView.findViewById(R.id.gvdifficultyMenu);
		
		gridMenu.setOnItemClickListener(this);
		
		refreshBeginnerLevel();
		
		/*gridMenu.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {

				String QuizId;
				try {

					QuizId = levelJsonArray.getJSONObject(position).getString(
							"QuizId");
					String QuizName = levelJsonArray.getJSONObject(position)
							.getString("LevelName");
					int isActive = levelJsonArray.getJSONObject(position)
							.getInt("IsActive");
					int isCompleted = levelJsonArray.getJSONObject(position)
							.getInt("IsCompleted");

					if (isActive != 0 ) {
						Fragment fragment = new ChallengeListFragment(QuizId,
								QuizName);
						FragmentManager fragmentManager = getActivity()
								.getFragmentManager();

						FragmentTransaction ft = fragmentManager
								.beginTransaction();

						ft.addToBackStack("ChallengeListFragment");
						ft.replace(R.id.content_frame, fragment,
								"ChallengeListFragment");

						ft.commit();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});*/

		return rootView;
	}

	private void refreshBeginnerLevel() {
		// TODO Auto-generated method stub
		new ApplicationAsk(getActivity(), "Fetching level", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				

				try {

				
					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {
						
						
						JSONArray dataArray = dashboardResponseObj
								.getJSONArray("data");
						levelJsonArray = dataArray;
						
						gridMenu.setAdapter(new LevelGridMenuAdapter(getActivity(),
								levelJsonArray));
						
						   
					} else {
						Toast.makeText(
								LevelFragment.this.getActivity(),
								""
										+ dashboardResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject subTopicReq = new JSONObject();
				try {
					subTopicReq.put("subTopic", LevelFragment.this.subTopicId);
					subTopicReq.put("Difficulty", "DD896FBB-A01B-4B7C-BC19-D2397EFAA33B");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				String subtopicURL = URLGenerator.getQuizListUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(subtopicURL,
						subTopicReq.toString() , CacheMemory.getToken(LevelFragment.this.getActivity()),LevelFragment.this.getActivity());//preferences.getString("Token"));

				// return resp;

			}
		}).execute();
	}
	
	
	protected void loadQuiz(int position) {
		// TODO Auto-generated method stub

		try {
			final String QuizId = levelJsonArray.getJSONObject(position)
					.getString("QuizId");

			new ApplicationAsk(getActivity(), "Fetching questions", new ApplicationService() {
				private String resp;

				@Override
				public void postExecute() {
					

					try {

						JSONObject dashboardResponseObj = new JSONObject(resp);
						String status = dashboardResponseObj
								.getString("status");

						if (status.equalsIgnoreCase("success")) {

							JSONArray dataArray = dashboardResponseObj
									.getJSONArray("data");

							QueListFragment fragment = new QueListFragment(
									dataArray);
							fragment.setQuizId(QuizId);
							FragmentManager fragmentManager = getActivity()
									.getFragmentManager();
							FragmentTransaction ft = fragmentManager
									.beginTransaction();

							ft.addToBackStack("QueListFragment");
							ft.replace(R.id.content_frame, fragment,
									"QueListFragment");

							ft.commit();

						} else {
							Toast.makeText(
									LevelFragment.this.getActivity(),
									""
											+ dashboardResponseObj
													.getString("message"),
									Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {

						e.printStackTrace();
					}

				}

				@Override
				public void execute() {
					JSONObject subTopicReq = new JSONObject();
					try {
						subTopicReq.put("QuizId", QuizId);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					String subtopicURL = URLGenerator.getQuestionByQuizUrl();
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequestWithToken(subtopicURL,
							subTopicReq.toString(), CacheMemory
									.getToken(LevelFragment.this.getActivity()),LevelFragment.this.getActivity());// preferences.getString("Token"));

					// return resp;

				}
			}).execute();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == beginnerBtn) {
			beginnerBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
					R.drawable.white_line);
			intermediateBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			advancedBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			beginnerBtn.setTextColor(Color.parseColor("#ffffff"));
			intermediateBtn.setTextColor(Color.parseColor("#ff9a75"));
			advancedBtn.setTextColor(Color.parseColor("#ff9a75"));
			loadLevels(0);
		} else if (arg0 == intermediateBtn) {
			intermediateBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
					R.drawable.white_line);
			beginnerBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			advancedBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			intermediateBtn.setTextColor(Color.parseColor("#ffffff"));
			beginnerBtn.setTextColor(Color.parseColor("#ff9a75"));
			advancedBtn.setTextColor(Color.parseColor("#ff9a75"));
			loadLevels(1);
		} else if (arg0 == advancedBtn) {
			advancedBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
					R.drawable.white_line);
			beginnerBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			intermediateBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			advancedBtn.setTextColor(Color.parseColor("#ffffff"));
			beginnerBtn.setTextColor(Color.parseColor("#ff9a75"));
			intermediateBtn.setTextColor(Color.parseColor("#ff9a75"));
			loadLevels(2);
		}
	}

	private void loadLevels(final int level) {
		// TODO Auto-generated method stub

		new ApplicationAsk(LevelFragment.this.getActivity(), "Fetching levels",
				new ApplicationService() {
					private String resp;

					@Override
					public void postExecute() {
						

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

								JSONArray dataArray = dashboardResponseObj
										.getJSONArray("data");
								gridMenu.invalidateViews();
								levelJsonArray = dataArray;
								gridMenu.setAdapter(new LevelGridMenuAdapter(
										getActivity(), dataArray));

							} else {
								Toast.makeText(
										LevelFragment.this.getActivity(),
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {
						JSONObject subTopicReq = new JSONObject();
						try {
							subTopicReq.put("subTopic",
									LevelFragment.this.subTopicId);
							if (level == 0)
								subTopicReq.put("Difficulty", BEGINNER);
							if (level == 1)
								subTopicReq.put("Difficulty", INTERMEDIATE);
							if (level == 2)
								subTopicReq.put("Difficulty", ADVANCED);

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						String subtopicURL = URLGenerator.getQuizListUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								subtopicURL, subTopicReq.toString(),
								CacheMemory.getToken(LevelFragment.this
										.getActivity()),LevelFragment.this.getActivity());// preferences.getString("Token"));

						// return resp;

					}
				}).execute();

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v,
			int position, long id) {
		// TODO Auto-generated method stub
		String QuizId;
		try {

			QuizId = levelJsonArray.getJSONObject(position).getString(
					"QuizId");
			String QuizName = levelJsonArray.getJSONObject(position)
					.getString("LevelName");
			int isActive = levelJsonArray.getJSONObject(position)
					.getInt("IsActive");
			int isCompleted = levelJsonArray.getJSONObject(position)
					.getInt("IsCompleted");

			if (isActive != 0 ) {
				Fragment fragment = new ChallengeListFragment(QuizId,
						QuizName);
				FragmentManager fragmentManager = getActivity()
						.getFragmentManager();

				FragmentTransaction ft = fragmentManager
						.beginTransaction();

				ft.addToBackStack("ChallengeListFragment");
				ft.replace(R.id.content_frame, fragment,
						"ChallengeListFragment");

				ft.commit();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.v("LevelFragment", "OnDestroy");
	}

}
