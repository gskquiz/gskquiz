package com.o2m.gsk.home;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.CryptedConfiguration;
import com.o2m.gsk.utility.URLGenerator;

public class SubtopicDialog extends Dialog implements
		android.view.View.OnClickListener, OnItemClickListener {

	public Activity activity;
	public Dialog dialog;
	public Button verifyotpBtn, resendBtn, cancelBtn;
	private EditText verifyOtpET;
	private TextView topicName;
	private JSONArray subtopiclistJsonArray;
	private ListView subtopicListView;

	public SubtopicDialog(Activity context, JSONArray subtopic) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;
		this.subtopiclistJsonArray = subtopic;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.subtopic_dialog);

		subtopicListView = (ListView) findViewById(R.id.subtopicList);
		topicName = (TextView) findViewById(R.id.topicName);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);

		cancelBtn.setOnClickListener(this);

		TopicAdapter adapter = new TopicAdapter(activity, subtopiclistJsonArray);
		subtopicListView.setAdapter(adapter);

		subtopicListView.setOnItemClickListener(this);

		topicName.setText("" + CacheMemory.getTopicName(activity));

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {
		// TODO Auto-generated method stub

		try {
			final String SubTopicId = subtopiclistJsonArray.getJSONObject(
					position).getString("SubTopicId");
						
			CacheMemory.setSubTopicId(activity, SubTopicId);
			
			this.dismiss();
			Fragment fragment = new LevelFragment(SubTopicId);
			FragmentManager fragmentManager = activity.getFragmentManager();
			FragmentTransaction ft = fragmentManager.beginTransaction();

			ft.addToBackStack("LevelFragment");
			ft.replace(R.id.content_frame, fragment, "LevelFragment");

			ft.commit();

			this.dismiss();
			
			/*
			 * new ApplicationAsk(activity, "Fetching level", new
			 * ApplicationService() { private String resp;
			 * 
			 * @Override public void postExecute() {
			 * 
			 * 
			 * try {
			 * 
			 * 
			 * JSONObject dashboardResponseObj = new JSONObject(resp); String
			 * status = dashboardResponseObj.getString("status");
			 * 
			 * if (status.equalsIgnoreCase("success")) {
			 * SubtopicDialog.this.dismiss(); JSONArray dataArray =
			 * dashboardResponseObj .getJSONArray("data"); Fragment fragment =
			 * new LevelFragment(dataArray, SubTopicId); FragmentManager
			 * fragmentManager = activity.getFragmentManager();
			 * FragmentTransaction ft = fragmentManager.beginTransaction();
			 * 
			 * ft.addToBackStack("LevelFragment");
			 * ft.replace(R.id.content_frame, fragment, "LevelFragment");
			 * 
			 * ft.commit();
			 * 
			 * 
			 * } else { Toast.makeText( activity, "" + dashboardResponseObj
			 * .getString("message"), Toast.LENGTH_LONG).show(); }
			 * 
			 * } catch (JSONException e) {
			 * 
			 * e.printStackTrace(); }
			 * 
			 * }
			 * 
			 * @Override public void execute() { JSONObject subTopicReq = new
			 * JSONObject(); try { subTopicReq.put("subTopic", SubTopicId);
			 * subTopicReq.put("Difficulty",
			 * "DD896FBB-A01B-4B7C-BC19-D2397EFAA33B"); } catch (JSONException
			 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
			 * 
			 * String subtopicURL = URLGenerator.getQuizListUrl();
			 * CommunicationService commService = new CommunicationService();
			 * resp = commService.sendSyncRequestWithToken(subtopicURL,
			 * subTopicReq.toString() ,
			 * CacheMemory.getToken(SubtopicDialog.this.
			 * activity));//preferences.getString("Token"));
			 * 
			 * // return resp;
			 * 
			 * } }).execute();
			 */

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == cancelBtn) {
			this.dismiss();
		}
	}

}
