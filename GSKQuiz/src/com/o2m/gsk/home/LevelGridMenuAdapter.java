package com.o2m.gsk.home;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.gsk.R;

public class LevelGridMenuAdapter extends BaseAdapter {
	
	
	Context context;
	JSONArray levelJsonArray;

	private static LayoutInflater inflater=null;
	public LevelGridMenuAdapter(Activity activity, JSONArray levelJsonArray) {
		
		context = activity;
		this.levelJsonArray = levelJsonArray;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return levelJsonArray.length();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
    public class Holder
    {
        TextView tv , progressTV;
        ImageView img;
        
    }
	@Override
	public View getView(final int position, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		Holder holder= new Holder();
		View rowView;
		rowView = inflater.inflate(R.layout.level_grid_item, null);
        holder.tv=(TextView) rowView.findViewById(R.id.titleTV);
        holder.img=(ImageView) rowView.findViewById(R.id.iconIV);
        holder.progressTV  = (TextView) rowView.findViewById(R.id.levelProgressTV);
        
        JSONObject quizDetails;
		try {
			
			quizDetails = levelJsonArray.getJSONObject(position);
			String title = quizDetails.getString("LevelName");
			int isActive =  quizDetails.getInt("IsActive");
			int isCompleted =  quizDetails.getInt("IsCompleted");
			
			holder.tv.setText(title);
			
			if(isActive == 0)
			{
				holder.img.setImageResource(R.drawable.locked);
				holder.progressTV.setText("");
				//holder.img.set
			}
			else if(isActive == 1 && isCompleted == 0)
			{
				holder.img.setImageResource(R.drawable.inprogress);
				holder.progressTV.setText("In progress");
			}
			else if(isActive == 1 && isCompleted == 1)
			{
				holder.img.setImageResource(R.drawable.completed);
				holder.progressTV.setText("Completed");
			}
			
			//IsActive
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        
        return rowView;


}
}
