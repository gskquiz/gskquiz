package com.o2m.gsk.home;

import android.app.Fragment;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.o2m.gsk.R;

public class ReportAdverseEventFragment extends Fragment  {

	private TextView raeTXT;

	public ReportAdverseEventFragment() {
		// TODO Auto-generated constructor stub
	
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.report_adverse_event_fragment,
				container, false);
		raeTXT = (TextView) rootView.findViewById(R.id.raemailTV);
		raeTXT.setMovementMethod(LinkMovementMethod.getInstance());
		return rootView;
	}


	

}
