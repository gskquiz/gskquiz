package com.o2m.gsk.home;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.BadgeDialog;
import com.o2m.gsk.R;
import com.o2m.gsk.myscore.MyScoreFragment;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.BadgeType;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class DashboardFragment extends Fragment implements OnItemClickListener, OnClickListener {

	private ListView topicList;
	private JSONArray topiclistJsonArray, followedtopiclistJsonArray,
			badgeJsonArray;
	private String fullName, points, UserId;
	protected String TAG;
	private ImageView badgeImage1, badgeImage2, badgeImage3, badgeImage4,
			avatarIv;
	private TextView badgeName1, badgeName2, badgeName3, badgeName4,
			badgeTitle, noBadgeTV;
	private Button followedTopicBtn;
	private Button allTopicBtn;
	private boolean isMale, isFollowedTopicSelected = true;
	private String topicId, topicName;

	public DashboardFragment(JSONArray _topiclistJsonArray,
			JSONArray _followedtopiclistJsonArray, JSONArray _badgeJsonArray,
			String _fullName, String _points, String _userId, boolean _isMale) {
		// Empty constructor required for fragment subclasses
		this.topiclistJsonArray = _topiclistJsonArray;
		this.followedtopiclistJsonArray = _followedtopiclistJsonArray;
		this.badgeJsonArray = _badgeJsonArray;
		this.fullName = _fullName;
		this.points = _points;
		this.UserId = _userId;
		this.isMale = _isMale;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.fragment_dashboard,
				container, false);
		topicList = (ListView) rootView.findViewById(R.id.topicList);
		TopicAdapter topicAdapter = new TopicAdapter(this.getActivity(),
				followedtopiclistJsonArray);
		topicList.setAdapter(topicAdapter);
		topicList.setOnItemClickListener(this);

		TextView nameTxt = (TextView) rootView.findViewById(R.id.name);
		TextView pointsTxt = (TextView) rootView.findViewById(R.id.points);
		noBadgeTV = (TextView) rootView.findViewById(R.id.nobadgemsgTV);

		nameTxt.setText(fullName);
		pointsTxt.setText("" + points);

		pointsTxt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				loadMyScore();
			}
		});

		followedTopicBtn = (Button) rootView
				.findViewById(R.id.followedTopicsBtn);
		allTopicBtn = (Button) rootView.findViewById(R.id.allTopicsBtn);

		badgeImage1 = (ImageView) rootView.findViewById(R.id.badgeImage1);
		badgeImage2 = (ImageView) rootView.findViewById(R.id.badgeImage2);
		badgeImage3 = (ImageView) rootView.findViewById(R.id.badgeImage3);
		badgeImage4 = (ImageView) rootView.findViewById(R.id.badgeImage4);
		avatarIv = (ImageView) rootView.findViewById(R.id.ismaleIv);

		badgeName1 = (TextView) rootView.findViewById(R.id.badgeName1);
		badgeName2 = (TextView) rootView.findViewById(R.id.badgeName2);
		badgeName3 = (TextView) rootView.findViewById(R.id.badgeName3);
		badgeName4 = (TextView) rootView.findViewById(R.id.badgeName4);
		badgeTitle = (TextView) rootView.findViewById(R.id.badgecasesTV);

		/*
		 * ListView myListView = (ListView) rootView.findViewById(R.id.mylist);
		 * String[] array = new String[] {"a", "b"}; ArrayAdapter<String> a =
		 * new ArrayAdapter<String>(this.getActivity(),
		 * android.R.layout.simple_list_item_1, array);
		 * myListView.setAdapter(a);
		 */

		if (isMale) {
			avatarIv.setImageResource(R.drawable.avatar_m);
		} else {
			avatarIv.setImageResource(R.drawable.avatar);
		}

		this.loadBadges();

		badgeTitle.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Fragment fragment = new BadgeFragment(
						DashboardFragment.this.badgeJsonArray);

				FragmentTransaction ft = getFragmentManager()
						.beginTransaction();

				ft.addToBackStack("BadgeFragment");
				ft.replace(R.id.content_frame, fragment, "BadgeFragment");

				ft.commit();
			}
		});

		allTopicBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
				R.drawable.grey_line);
		followedTopicBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
				R.drawable.orange_line);

		followedTopicBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Log.v("", "followedtopiclistJsonArray : "
						+ followedtopiclistJsonArray);
				isFollowedTopicSelected = true;

				allTopicBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
						R.drawable.grey_line);
				followedTopicBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						0, R.drawable.orange_line);
				topicList.invalidateViews();
				TopicAdapter topicAdapter = new TopicAdapter(
						DashboardFragment.this.getActivity(),
						followedtopiclistJsonArray);
				topicList.setAdapter(topicAdapter);

			}
		});

		allTopicBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Log.v("", "topiclistJsonArray : " + topiclistJsonArray);

				isFollowedTopicSelected = false;
				allTopicBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
						R.drawable.orange_line);
				followedTopicBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0,
						0, R.drawable.grey_line);
				topicList.invalidateViews();
				TopicAdapter topicAdapter = new TopicAdapter(
						DashboardFragment.this.getActivity(),
						topiclistJsonArray);
				topicList.setAdapter(topicAdapter);
			}
		});

		showBadgeEarnedPopup();

		return rootView;
	}

	private void showBadgeEarnedPopup() {
		// TODO Auto-generated method stub
		/*
		 * int prevPts = 0; String prevPoints =
		 * CacheMemory.getPreviousPoints(getActivity());
		 * 
		 * if(prevPoints != null) prevPts = Integer.parseInt(prevPoints);
		 */

		int currentPts = 0;

		if (this.points != null)
			currentPts = Integer.parseInt(this.points);

		

		if (currentPts >= 240 && currentPts < 480) {
			// bronze
			if (CacheMemory.getBronzeFlag(getActivity()) == null) {
				BadgeDialog badgeDialog = new BadgeDialog(getActivity(),
						BadgeType.BRONZE, this.points);
				badgeDialog.show();
				CacheMemory.setBronzeFlag(getActivity(), "Y");
			}

		} else if (currentPts >= 480 && currentPts < 600) {
			// silver
			if (CacheMemory.getSilverFlag(getActivity()) == null) {
				BadgeDialog badgeDialog = new BadgeDialog(getActivity(),
						BadgeType.SILVER, this.points);
				badgeDialog.show();
				CacheMemory.setSilverFlag(getActivity(), "Y");
			}
		} else if (currentPts >= 600 && currentPts < 720) {
			// gold
			if (CacheMemory.getGoldFlag(getActivity()) == null) {
				BadgeDialog badgeDialog = new BadgeDialog(getActivity(),
						BadgeType.GOLD, this.points);
				badgeDialog.show();
				CacheMemory.setGoldFlag(getActivity(), "Y");
			}

		} else if (currentPts >= 720) {
			// platinum
			if (CacheMemory.getPlatinumFlag(getActivity()) == null) {
				BadgeDialog badgeDialog = new BadgeDialog(getActivity(),
						BadgeType.PLATINUM, this.points);
				badgeDialog.show();
				CacheMemory.setPlatinumFlag(getActivity(), "Y");
			}
		}

	}

	public void loadBadges() {
		
		
		badgeImage1.setOnClickListener(this);
		badgeName1.setOnClickListener(this);
		badgeImage2.setOnClickListener(this);
		badgeName2.setOnClickListener(this);
		badgeImage3.setOnClickListener(this);
		badgeName3.setOnClickListener(this);
		badgeImage4.setOnClickListener(this);
		badgeName4.setOnClickListener(this);
		

		if (badgeJsonArray.length() == 0) {
			noBadgeTV.setVisibility(View.VISIBLE);
		}

		for (int i = 0; i < badgeJsonArray.length(); i++) {
			JSONObject badgeJson;
			try {
				badgeJson = badgeJsonArray.getJSONObject(i);
				String base64 = badgeJson.getString("ImageData");
				// String base64 =
				// "iVBORw0KGgoAAAANSUhEUgAAACYAAAAdCAYAAADGgB7AAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlEOTJBQkVDQjIzNDExRTc4MEQyQTBFMDlGQTY4NkNEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlEOTJBQkVEQjIzNDExRTc4MEQyQTBFMDlGQTY4NkNEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUQ5MkFCRUFCMjM0MTFFNzgwRDJBMEUwOUZBNjg2Q0QiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUQ5MkFCRUJCMjM0MTFFNzgwRDJBMEUwOUZBNjg2Q0QiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7YY2/xAAAKZUlEQVR42pxYCXBV1Rn+7vL25OXl5WVfgCTIGgRUMBJQ0ShYawdbp7aCtGqVuFTFmQrdtLaCVhSZDlgdsODSolTtMKBUXAoKKEQQIlskQMhG1pe8l7ffpd+5oJICgr0zJ/e+e8/yn//7v+//TyTzHgy4DFOy7rJk4lsvBXi7rxD5thjGeXquhY4aSHDAwKqUKa/W2cUpGTifS+eaYj3plHcqvttVytH30qiJkKFe5Or+SJHMHL6bZc1qWjNOs2nGLMWUdvL5CuuNgd1sy/lce74LSefhMTtbOlsll1jDBZy1kQASpoxJaR0QBq0ODjE/iuaGuHPHGGfQeVvml3DKOmqjAWjsMNHVCYke5o87+HcdW5gt+v97TMKf6J2beffyV/beiA9LukdE9id87uOaS5oX+AI3ew9jWc9wRE01wyVp2BzNxTXpLTiSTMf9bRPgVxJmkS2q3+s/oFZltC+nV3s4Vz+NXMvn+eJZOsPS8lmNkvEB2282h3PLFrePyu5P2cRiqXf6izy5akwqtEWxPZaFBDuNcgSlgBKHT0nyuRcZchJboznwyikUqVFpcyRX3RP3wdQlLG0f7t/QW1BihYSM7URIOVMknuYxC0IJ8wnBlcs6h2NVbzm6dCdaUy48lV9rWxsuxiF6w8fFOzUHYoaKYhp5MJEBTZJQ5ghD4RRt7C8M7TNsmOjuxBx/PZZ0jMTTPSORo8QNwqzPzdk3wiHpyxKGMscjaebZPWZiHN98Sm4tUE0Ta/oGQyWzqtzteDdSgF0xP2r8BxHUHbDxfa/hQLfmRKEaRZIDk6aCYnuE7+1oTHqQJmsg5LjPvx/HUml4ua8Ul7k7hMHy6vAQm04PKk7zTo+q7efaUwcaJlmxJC4//fdBSLdPWNhcgd1xP1YVf2wZFtTtyFPjWNw1EsMJlZi8m97iTrmwEzlqDBrJICApoJHCmH56KqzbUOHsRSU99gzHpispxDkmxG8vFX6EDt2Fx5ovRHvKNYxrv8fhgy1XkSjkjvSViXdykO93HWPNpT0jcE/rRDj49fmCbeji4m4G9heJTHzQn4/ZmYcQMu0W7K2aGy72s0u6pRZiA80pDwzC2sUNzfYdwo5YAFui2chSEvRkGhbl7kC5PYya1kvxQs8wPHz8IqQMWUpBnvs8f/+2jcDtifrxbriQ9Fcq4pqCTZE8aayrG2mKhpsar0A2F1qUvwONmoeeiWN5cCjylRgmE944oTuSSgcnpBdUHE85rXhoSrkZewoudAYxzB7CixyTxXg7TKMezd2F8a4e3HjsSggRvtjVhU9j2Qhrqphvyt+C5RBxrDw+wRA7npFji89zOnW7j9HyTn+hFR92wvhWeBBIdWSpSWzsL0CYnhps68cETrgqfAG0RAxXRffBn2jEaKMRw5M9eD0+GHVGPh4J7EY7vf1aaIhl6JzMeszIOIafNl8uHEEUdDQy9h4I7MckXwcRMvJKlGjPD7xN25UnK/USvyvxyYGo1/5o+1jcmtmA73ub8e9wAbXJZgH9Ng2dl10ntMoiQIjBPTYtDLmnAz9Or8eUa2/A5Btn4vLqahTlBZDZvAX2SBBjsnT8vXcQGpJeXJfejPuyDuDOtsvQwRg8wTUJfy3aRtZ2YV7bxYKtmJzZPn2QLfIvld8eTKQUPNlVYXnkYDwDf6S73yz5j4X9NsZGP2Ga3z4eC3N24iilYn2kBMNaGvBs2SHggTeB4TO+odMkoHrKBlQvmo6nj+Vgg1aCalczfhXYi0c6x+FLyopEkyoI85/zanGUHpvVVIU6kq2DhFrh2Aq3rC2WGR6TG2Lp2BPPxKVkj5CBmpZKrGBcPJW3AzN9h62UsYWKLmLlfj9hQ5CwtQJ3rxho1NcZdRrwi5UYbXYgwL5zs/Zhdd8QCwXB3hu9jVhGUv2TcnR7cxXZLFlr1ye8qb0UYrJyiipSRLY9hhGOEK32IZcBPtQhArYcn8cy8UTeZyi1h80nOiuwLDhCSlc1rPRsROmYMcCY286ezsbNRuUlz2Jl3XvYGR+Cv5DpXory3Ky9uDqtDXOZrrYwO5SRnUI+DiS8YJ7tG2TvD9DSkExqPJ6tJvA8sZ6fU4ds4nyE7BHUrk9m4NaWyUJApWfztjMNRbCABhpxA77Bpd+aZoV0eIdeAFtKx8LuCpInjqdzazGSOnhbyyR8xnQm1jhKacnk/SFC/VzhJwHBfNp0l5CzTbRwokdKvXFz1hG8Omgzfp+z21LzFCE8ztQyp7USDZSFZXnbqFMxvNyfCcS6zpH/eSWDeCPkhEfoIce2M7Xd1XqZlQUIn56vxlIPB+qwvHArZgcahACv54crOXLNibKHs6R0GXsTvqlZtsSDxWmR65n9sbavGG+HCrm7ANp0N2oyD2C8sxv1IQl3ZLfD80wj4Mk/s2WJPiQeKsaKZhdKfA4LqiXMkwE5jkvc3fr09Bble+lNsNsMUQ6tMXRpKQV7\n"
				// +
				// "08B67GSR9wkZuI7ilqEkqn7mb7gr2xX/CdVTEWxdFyrCm6FBGOnso6Qcxi1RMrLqahj3bzytRNFEdbD0emDzerzmKsXKnjKSy2fO8B4zr0tvkadROmRRPup4hW1xCtLnEtFRT6l4lUcnfJ0r0csUUk/N2ZPIPKbAfGuoElptk02lzB0eOd3TYr+EGeEQv/+DDEt5ZFza/CmkpveRyBjMTFsCWZZhHq2F8vLPga0b8Jw8DE9RhoZQ/Rfm7ZRqAgeloe5Qkuu/SOtn0Rmiqj1unkzYp5bzAzwmJEN4rJkCeBWZMy2tBR5JJA4U0y13kMa/5N13qN9rkWKssxOPSdsREGVkUQHdxGK35Sh6u4E/mOOxKV6Ilwo+xmhvULgxoWvSEm74BfZuGOhhGvA/HlO/NXi/KXab6PhHaPzd7bpLX9dfJE1xt8sH6b3bpamo7j+C0rom1uUGDttK8KFeBlHSXOVqw8ZIPrzMk3lKTI8a6gKPrPXZcO5Dinr64ce08liE2iJy5deHDJAtKgKsQLWPozmySFtuVqh9hH99tBySXM6xBnOnhKGELtuWIBs1vMdqpCXhxqKiWrddT17LMu/18zmMqAOLM9Mq8lyEr5I1l23g8eszBmpTTeBAsdElWTlzBDVJ1PlTCXsXi0dRQonKQwimxHj5IuHDMFsIDwb2ic11sX0onecpSR6ItQyFxtzgbUK5J2x5yjRPOIwtRIf8Ot8RQ549ygwRw654FvubIETWROrJjTk4h6jdhIAWsW+hgwciAwupXZ0n5xrQzn58OwmXKAKF8o72BK2Dw5ljz3wXTlS/2lGKOqYsUZMJb/nlBJI8o0UNhYWRIVKbVMxMMSf/ILS4vDNmyFXcdOxMGUK2zoimtckzQimAE3XYOa5reDRac4vv8I/ABPBKsMwyqpGpJWKqxih7ryyq2tv9X1ob7ow63m9MeqePsnenRFWhQzrNsHPGmBgiDrI4x38H+P0mths4YOZMf8NNJ06tJ0ND+eoUi/X8/lLYsL0eIdQ2ekM7lefflZXne1F71nKttYpmjubPLK74Q97d/PAa7yKR7hIbVNlJkMj8jvP/V4ABAM5Mja7xj3lmAAAAAElFTkSuQmCC";
				String name = badgeJson.getString("Name");
				int isActive = badgeJson.getInt("IsActive");
				// setTextColor(Color.parseColor("#d5d1ce"))
				byte[] data = Base64.decode(base64, Base64.DEFAULT);
				Bitmap bmp = BitmapFactory
						.decodeByteArray(data, 0, data.length);
				// Log.v(TAG, "base64 : " + base64 + "\ndata : " + data +
				// "\nbmp "
				// + bmp);
				if (i == 0) {
					badgeImage1.setVisibility(View.VISIBLE);
					badgeName1.setVisibility(View.VISIBLE);
					if (bmp != null)
						badgeImage1.setImageBitmap(bmp);
					badgeName1.setText(name);
					if (isActive == 0)
						badgeName1.setTextColor(Color.parseColor("#d5d1ce"));
				} else if (i == 1) {
					badgeImage2.setVisibility(View.VISIBLE);
					badgeName2.setVisibility(View.VISIBLE);
					if (bmp != null)
						badgeImage2.setImageBitmap(bmp);
					badgeName2.setText(name);
					if (isActive == 0)
						badgeName2.setTextColor(Color.parseColor("#d5d1ce"));
				} else if (i == 2) {
					badgeImage3.setVisibility(View.VISIBLE);
					badgeName3.setVisibility(View.VISIBLE);
					if (bmp != null)
						badgeImage3.setImageBitmap(bmp);
					badgeName3.setText(name);
					if (isActive == 0)
						badgeName3.setTextColor(Color.parseColor("#d5d1ce"));
				} else if (i == 3) {
					badgeImage4.setVisibility(View.VISIBLE);
					badgeName4.setVisibility(View.VISIBLE);
					if (bmp != null)
						badgeImage4.setImageBitmap(bmp);
					badgeName4.setText(name);
					if (isActive == 0)
						badgeName4.setTextColor(Color.parseColor("#d5d1ce"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {
		// TODO Auto-generated method stub
		Log.v(TAG, "onItemClick");
		try {

			topicId = topiclistJsonArray.getJSONObject(position).getString(
					"TopicId");
			topicName = topiclistJsonArray.getJSONObject(position).getString(
					"Name");

			if (isFollowedTopicSelected) {
				topicId = followedtopiclistJsonArray.getJSONObject(position)
						.getString("TopicId");
				topicName = followedtopiclistJsonArray.getJSONObject(position)
						.getString("Name");
			}

			new ApplicationAsk(getActivity(), "Fetching subtopic . .",
					new ApplicationService() {
						private String resp;

						@Override
						public void postExecute() {

							try {

								JSONObject dashboardResponseObj = new JSONObject(
										resp);
								String status = dashboardResponseObj
										.getString("status");

								if (status.equalsIgnoreCase("success")) {

									JSONArray dataArray = dashboardResponseObj
											.getJSONArray("data");

									if (dataArray.length() <= 0) {
										Toast.makeText(getActivity(),
												"Coming Soon",
												Toast.LENGTH_LONG).show();
									} else {

										SubtopicDialog sugDialog = new SubtopicDialog(
												getActivity(), dataArray);
										sugDialog.show();
									}
								} else {
									Toast.makeText(
											DashboardFragment.this
													.getActivity(),
											""
													+ dashboardResponseObj
															.getString("message"),
											Toast.LENGTH_LONG).show();
								}

							} catch (JSONException e) {

								e.printStackTrace();
							}

						}

						@Override
						public void execute() {
							JSONObject subTopicReq = new JSONObject();
							try {
								subTopicReq.put("TopicId",
										DashboardFragment.this.topicId);
								CacheMemory.setTopicName(getActivity(),
										DashboardFragment.this.topicName);

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							String subtopicURL = URLGenerator
									.getGetSubTopicUrl();
							CommunicationService commService = new CommunicationService();
							resp = commService.sendSyncRequestWithToken(
									subtopicURL, subTopicReq.toString(),
									CacheMemory.getToken(DashboardFragment.this
											.getActivity()),
									DashboardFragment.this.getActivity());// preferences.getString("Token"));

							// return resp;

						}
					}).execute();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadMyScore() {
		// TODO Auto-generated method stub
		new ApplicationAsk(this.getActivity(), "Fetching score . .",
				new ApplicationService() {

					private String resp;

					@Override
					public void postExecute() {
						Log.v(TAG, " onPostExecute Login Response : " + resp);

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

								JSONArray dataObj = dashboardResponseObj
										.getJSONArray("data");
								
								String totalPoints = null;
								
								if(dataObj.length() > 0)
								{
									if(dataObj.getJSONObject(0).has("totalPoints"))
									{
										totalPoints = dataObj.getJSONObject(0).getString("totalPoints");
									}
								}
								
								MyScoreFragment fragment = new MyScoreFragment(dataObj);
								FragmentTransaction ft = getFragmentManager()
										.beginTransaction();
								fragment.setTotalPoints(totalPoints);
								ft.addToBackStack("MyScoreFragment");
								ft.replace(R.id.content_frame, fragment,
										"MyScoreFragment");

								ft.commit();

							} else {
								Toast.makeText(
										DashboardFragment.this.getActivity(),
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {

						String loginURL = URLGenerator.getMyScoreUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(loginURL,
								"", CacheMemory.getToken(DashboardFragment.this
										.getActivity()), DashboardFragment.this
										.getActivity());
						//

					}
				}).execute();

	}

	@Override
	public void onClick(View arg0) {
		
		if (arg0 == badgeImage1 || arg0 == badgeName1 || arg0 == badgeImage2 || arg0 == badgeName2 || arg0 == badgeImage3 || arg0 == badgeName3 || arg0 == badgeImage4 || arg0 == badgeName4) {
			// TODO Auto-generated method stub
			Fragment fragment = new BadgeFragment(
					DashboardFragment.this.badgeJsonArray);
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.addToBackStack("BadgeFragment");
			ft.replace(R.id.content_frame, fragment, "BadgeFragment");
			ft.commit();
		}
	}

}
