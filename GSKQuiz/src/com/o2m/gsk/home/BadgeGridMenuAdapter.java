package com.o2m.gsk.home;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.o2m.gsk.R;

public class BadgeGridMenuAdapter extends BaseAdapter {

	Context context;
	JSONArray levelJsonArray;

	private static LayoutInflater inflater = null;

	public BadgeGridMenuAdapter(Activity activity, JSONArray levelJsonArray) {

		context = activity;
		this.levelJsonArray = levelJsonArray;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return levelJsonArray.length();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class Holder {
		TextView badgename, points, badgelevel;
		ImageView badgeimage;

	}

	@Override
	public View getView(final int position, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub

		Holder holder = new Holder();
		View rowView;
		rowView = inflater.inflate(R.layout.badge_grid_item, null);
		holder.badgename = (TextView) rowView.findViewById(R.id.badgenameTV);
		holder.points = (TextView) rowView.findViewById(R.id.pointsTV);
		holder.badgeimage = (ImageView) rowView.findViewById(R.id.iconIV);
		holder.badgelevel = (TextView) rowView.findViewById(R.id.levelTV);

		JSONObject badgeObj;
		try {

			badgeObj = levelJsonArray.getJSONObject(position);
			String name = badgeObj.getString("Name");
			String points = badgeObj.getString("Points");
			String level = badgeObj.getString("LevelName");
			int isActive = badgeObj.getInt("IsActive");
			String base64 = badgeObj.getString("ImageData");
			// String base64 =
			// "iVBORw0KGgoAAAANSUhEUgAAACYAAAAdCAYAAADGgB7AAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlEOTJBQkVDQjIzNDExRTc4MEQyQTBFMDlGQTY4NkNEIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlEOTJBQkVEQjIzNDExRTc4MEQyQTBFMDlGQTY4NkNEIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUQ5MkFCRUFCMjM0MTFFNzgwRDJBMEUwOUZBNjg2Q0QiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUQ5MkFCRUJCMjM0MTFFNzgwRDJBMEUwOUZBNjg2Q0QiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7YY2/xAAAKZUlEQVR42pxYCXBV1Rn+7vL25OXl5WVfgCTIGgRUMBJQ0ShYawdbp7aCtGqVuFTFmQrdtLaCVhSZDlgdsODSolTtMKBUXAoKKEQQIlskQMhG1pe8l7ffpd+5oJICgr0zJ/e+e8/yn//7v+//TyTzHgy4DFOy7rJk4lsvBXi7rxD5thjGeXquhY4aSHDAwKqUKa/W2cUpGTifS+eaYj3plHcqvttVytH30qiJkKFe5Or+SJHMHL6bZc1qWjNOs2nGLMWUdvL5CuuNgd1sy/lce74LSefhMTtbOlsll1jDBZy1kQASpoxJaR0QBq0ODjE/iuaGuHPHGGfQeVvml3DKOmqjAWjsMNHVCYke5o87+HcdW5gt+v97TMKf6J2beffyV/beiA9LukdE9id87uOaS5oX+AI3ew9jWc9wRE01wyVp2BzNxTXpLTiSTMf9bRPgVxJmkS2q3+s/oFZltC+nV3s4Vz+NXMvn+eJZOsPS8lmNkvEB2282h3PLFrePyu5P2cRiqXf6izy5akwqtEWxPZaFBDuNcgSlgBKHT0nyuRcZchJboznwyikUqVFpcyRX3RP3wdQlLG0f7t/QW1BihYSM7URIOVMknuYxC0IJ8wnBlcs6h2NVbzm6dCdaUy48lV9rWxsuxiF6w8fFOzUHYoaKYhp5MJEBTZJQ5ghD4RRt7C8M7TNsmOjuxBx/PZZ0jMTTPSORo8QNwqzPzdk3wiHpyxKGMscjaebZPWZiHN98Sm4tUE0Ta/oGQyWzqtzteDdSgF0xP2r8BxHUHbDxfa/hQLfmRKEaRZIDk6aCYnuE7+1oTHqQJmsg5LjPvx/HUml4ua8Ul7k7hMHy6vAQm04PKk7zTo+q7efaUwcaJlmxJC4//fdBSLdPWNhcgd1xP1YVf2wZFtTtyFPjWNw1EsMJlZi8m97iTrmwEzlqDBrJICApoJHCmH56KqzbUOHsRSU99gzHpispxDkmxG8vFX6EDt2Fx5ovRHvKNYxrv8fhgy1XkSjkjvSViXdykO93HWPNpT0jcE/rRDj49fmCbeji4m4G9heJTHzQn4/ZmYcQMu0W7K2aGy72s0u6pRZiA80pDwzC2sUNzfYdwo5YAFui2chSEvRkGhbl7kC5PYya1kvxQs8wPHz8IqQMWUpBnvs8f/+2jcDtifrxbriQ9Fcq4pqCTZE8aayrG2mKhpsar0A2F1qUvwONmoeeiWN5cCjylRgmE944oTuSSgcnpBdUHE85rXhoSrkZewoudAYxzB7CixyTxXg7TKMezd2F8a4e3HjsSggRvtjVhU9j2Qhrqphvyt+C5RBxrDw+wRA7npFji89zOnW7j9HyTn+hFR92wvhWeBBIdWSpSWzsL0CYnhps68cETrgqfAG0RAxXRffBn2jEaKMRw5M9eD0+GHVGPh4J7EY7vf1aaIhl6JzMeszIOIafNl8uHEEUdDQy9h4I7MckXwcRMvJKlGjPD7xN25UnK/USvyvxyYGo1/5o+1jcmtmA73ub8e9wAbXJZgH9Ng2dl10ntMoiQIjBPTYtDLmnAz9Or8eUa2/A5Btn4vLqahTlBZDZvAX2SBBjsnT8vXcQGpJeXJfejPuyDuDOtsvQwRg8wTUJfy3aRtZ2YV7bxYKtmJzZPn2QLfIvld8eTKQUPNlVYXnkYDwDf6S73yz5j4X9NsZGP2Ga3z4eC3N24iilYn2kBMNaGvBs2SHggTeB4TO+odMkoHrKBlQvmo6nj+Vgg1aCalczfhXYi0c6x+FLyopEkyoI85/zanGUHpvVVIU6kq2DhFrh2Aq3rC2WGR6TG2Lp2BPPxKVkj5CBmpZKrGBcPJW3AzN9h62UsYWKLmLlfj9hQ5CwtQJ3rxho1NcZdRrwi5UYbXYgwL5zs/Zhdd8QCwXB3hu9jVhGUv2TcnR7cxXZLFlr1ye8qb0UYrJyiipSRLY9hhGOEK32IZcBPtQhArYcn8cy8UTeZyi1h80nOiuwLDhCSlc1rPRsROmYMcCY286ezsbNRuUlz2Jl3XvYGR+Cv5DpXory3Ky9uDqtDXOZrrYwO5SRnUI+DiS8YJ7tG2TvD9DSkExqPJ6tJvA8sZ6fU4ds4nyE7BHUrk9m4NaWyUJApWfztjMNRbCABhpxA77Bpd+aZoV0eIdeAFtKx8LuCpInjqdzazGSOnhbyyR8xnQm1jhKacnk/SFC/VzhJwHBfNp0l5CzTbRwokdKvXFz1hG8Omgzfp+z21LzFCE8ztQyp7USDZSFZXnbqFMxvNyfCcS6zpH/eSWDeCPkhEfoIce2M7Xd1XqZlQUIn56vxlIPB+qwvHArZgcahACv54crOXLNibKHs6R0GXsTvqlZtsSDxWmR65n9sbavGG+HCrm7ANp0N2oyD2C8sxv1IQl3ZLfD80wj4Mk/s2WJPiQeKsaKZhdKfA4LqiXMkwE5jkvc3fr09Bble+lNsNsMUQ6tMXRpKQV7\n"
			// +
			// "08B67GSR9wkZuI7ilqEkqn7mb7gr2xX/CdVTEWxdFyrCm6FBGOnso6Qcxi1RMrLqahj3bzytRNFEdbD0emDzerzmKsXKnjKSy2fO8B4zr0tvkadROmRRPup4hW1xCtLnEtFRT6l4lUcnfJ0r0csUUk/N2ZPIPKbAfGuoElptk02lzB0eOd3TYr+EGeEQv/+DDEt5ZFza/CmkpveRyBjMTFsCWZZhHq2F8vLPga0b8Jw8DE9RhoZQ/Rfm7ZRqAgeloe5Qkuu/SOtn0Rmiqj1unkzYp5bzAzwmJEN4rJkCeBWZMy2tBR5JJA4U0y13kMa/5N13qN9rkWKssxOPSdsREGVkUQHdxGK35Sh6u4E/mOOxKV6Ilwo+xmhvULgxoWvSEm74BfZuGOhhGvA/HlO/NXi/KXab6PhHaPzd7bpLX9dfJE1xt8sH6b3bpamo7j+C0rom1uUGDttK8KFeBlHSXOVqw8ZIPrzMk3lKTI8a6gKPrPXZcO5Dinr64ce08liE2iJy5deHDJAtKgKsQLWPozmySFtuVqh9hH99tBySXM6xBnOnhKGELtuWIBs1vMdqpCXhxqKiWrddT17LMu/18zmMqAOLM9Mq8lyEr5I1l23g8eszBmpTTeBAsdElWTlzBDVJ1PlTCXsXi0dRQonKQwimxHj5IuHDMFsIDwb2ic11sX0onecpSR6ItQyFxtzgbUK5J2x5yjRPOIwtRIf8Ot8RQ549ygwRw654FvubIETWROrJjTk4h6jdhIAWsW+hgwciAwupXZ0n5xrQzn58OwmXKAKF8o72BK2Dw5ljz3wXTlS/2lGKOqYsUZMJb/nlBJI8o0UNhYWRIVKbVMxMMSf/ILS4vDNmyFXcdOxMGUK2zoimtckzQimAE3XYOa5reDRac4vv8I/ABPBKsMwyqpGpJWKqxih7ryyq2tv9X1ob7ow63m9MeqePsnenRFWhQzrNsHPGmBgiDrI4x38H+P0mths4YOZMf8NNJ06tJ0ND+eoUi/X8/lLYsL0eIdQ2ekM7lefflZXne1F71nKttYpmjubPLK74Q97d/PAa7yKR7hIbVNlJkMj8jvP/V4ABAM5Mja7xj3lmAAAAAElFTkSuQmCC";

			byte[] data = Base64.decode(base64, Base64.DEFAULT);
			Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);

			holder.badgename.setText(name);
			holder.points.setText(points);
			holder.badgelevel.setText(level);
			if (bmp != null)
				holder.badgeimage.setImageBitmap(bmp);

			if (isActive == 0) {
				holder.badgename.setTextColor(Color.parseColor("#d5d1ce"));
				holder.points.setTextColor(Color.parseColor("#d5d1ce"));
				holder.badgelevel.setTextColor(Color.parseColor("#d5d1ce"));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rowView;

	}
}
