package com.o2m.gsk.home;

import org.json.JSONArray;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.o2m.gsk.R;

public class BadgeFragment extends Fragment  {

	
	private GridView gridMenu;
	private TextView nobadgeTxt;
	private JSONArray badgeJsonArray;
	
	protected String TAG;
	

	public BadgeFragment(JSONArray _badgeJsonArray) {
		// TODO Auto-generated constructor stub
		this.badgeJsonArray = _badgeJsonArray;
		//this.subTopicId = _subTopicId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.badge_fragment,
				container, false);
	
		gridMenu = (GridView) rootView.findViewById(R.id.gvbadgeMenu);
		nobadgeTxt = (TextView) rootView.findViewById(R.id.nobadgemsgTV);
		gridMenu.setAdapter(new BadgeGridMenuAdapter(getActivity(),
				badgeJsonArray));
		
		if(!(badgeJsonArray.length() > 0))
		{
			nobadgeTxt.setVisibility(View.VISIBLE);
			gridMenu.setVisibility(View.GONE);
		}
		
		gridMenu.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {


		}});

		return rootView;
	}

	
}
