package com.o2m.gsk;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobicule.crashnotifier.*;
import com.o2m.gsk.challenge.ChallengeeQuestionListFragment;
import com.o2m.gsk.challenge.OnlineChallengeWinnerlFragment;
import com.o2m.gsk.challenge.OnlineQuestionListFragment;
import com.o2m.gsk.challenge.QuestionListFragment;
import com.o2m.gsk.feedback.FeedbackFragment;
import com.o2m.gsk.home.AskQueryFragment;
import com.o2m.gsk.home.DashboardFragment;
import com.o2m.gsk.home.ReportAdverseEventFragment;
import com.o2m.gsk.invitefriend.InviteFriendFragment;
import com.o2m.gsk.myaccount.MyAccountFragment;
import com.o2m.gsk.myfriends.FriendListFragment;
import com.o2m.gsk.myscore.MyScoreFragment;
import com.o2m.gsk.notification.NotificationListFragment;
import com.o2m.gsk.notification.RequestAcceptDialog;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;
import com.o2m.gsk.utility.ValidationUtility;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class GSKHomeActivity extends Activity {

	private ArrayList<String> list;
	private String TAG = "GSKHomeActivity";
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private String[] menuTitles;
	private int i, selectedPosition;
	MyCountDownOnlineTimer countDownOnlineTimer;
	private TextView badgeTxt;
	IGenericExceptionHandler handler;

	enum MenuType {

		Home, AppTutorial, LeaderBoard, MyAccount, MyFriends, PI, ReportAnAdverseEvent, AskQuery, Feedback, Logout

	};

	public class MyCountDownOnlineTimer extends CountDownTimer {

		public MyCountDownOnlineTimer(long millisInFuture,
				long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {

			if (CacheMemory.getOnlineChallengeStatus(GSKHomeActivity.this)
					.equalsIgnoreCase(CacheMemory.ONLINE_CHALLENGE_YES)) {

				Log.v("", "!!!! ONLINE QUIZ IN PROGRESS !!!!");
				//Toast.makeText(GSKHomeActivity.this, "PLAYING QUIZ", Toast.LENGTH_SHORT).show();
				GSKHomeActivity.this.repeatTimer();
				return;
			}
			
			
			
			ApplicationAsk aa = new ApplicationAsk(GSKHomeActivity.this, "",
					new ApplicationService() {

						private String resp;

						@Override
						public void postExecute() {

							try {
								//Toast.makeText(GSKHomeActivity.this, "Checking Online Opponent", Toast.LENGTH_SHORT).show();
								JSONObject notificationResponseObj = new JSONObject(
										resp);
								String status = notificationResponseObj
										.getString("status");

								if (status.equalsIgnoreCase("success")) {

									JSONArray notificationArray = notificationResponseObj
											.getJSONArray("data");
									if (CacheMemory.DEBUG_MODE)
									{
										Toast.makeText(
												GSKHomeActivity.this,
												""
														+ notificationArray
																.toString(),
												Toast.LENGTH_SHORT).show();
										Log.v("GSKHomeActivity", "====notificationArray" + notificationArray.toString());
									}

									if (notificationArray.length() > 0) {
										JSONObject nTObj = notificationArray
												.getJSONObject(0);

										badgeTxt.setText(""
												+ nTObj.getInt("IsNew"));
									}
									else
									{
										badgeTxt.setText("0");
									}

									for (int i = 0; i < notificationArray
											.length(); i++) {

										JSONObject nTObj = notificationArray
												.getJSONObject(i);

										if (nTObj.getString("Type")
												.equalsIgnoreCase("5")) {
											Log.v("",
													"------------- Online Challenge request received ---------- ");
											// Show Pop up Dialog

											CacheMemory
													.setOnlineChallengeStatus(
															GSKHomeActivity.this,
															CacheMemory.ONLINE_CHALLENGE_YES);

											RequestAcceptDialog rad = new RequestAcceptDialog(
													GSKHomeActivity.this, nTObj);
											rad.show();

											break;
										}

									}

								} else {
									Toast.makeText(
											GSKHomeActivity.this,
											""
													+ notificationResponseObj
															.getString("message"),
											Toast.LENGTH_LONG).show();
								}

							} catch (JSONException e) {

								e.printStackTrace();
							}

						}

						@Override
						public void execute() {

							String notificationURL = URLGenerator
									.getMyNotificationUrl();
							JSONObject param = new JSONObject();
							try {
								param.put("IsRead", "0");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							CommunicationService commService = new CommunicationService();
							resp = commService.sendSyncRequestWithToken(
									notificationURL, param.toString(),
									CacheMemory.TOKEN, GSKHomeActivity.this);
						}
					});
			aa.setProgressHidden(true);
			aa.execute();

			GSKHomeActivity.this.repeatTimer();
		}

		@Override
		public void onTick(long milliUntilFinished) {

		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inbox);
		CacheMemory.setOnlineChallengeStatus(this,
				CacheMemory.ONLINE_CHALLENGE_NO);
		badgeTxt = (TextView) findViewById(R.id.badge_notification_1);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		menuTitles = getResources()
				.getStringArray(R.array.gsk_menu_title_array);
		// set up the drawer's list view with items and click listener
		MsgNavigationDrawerListAdapter msgMenuAdapter = new MsgNavigationDrawerListAdapter(
				this, menuTitles);
		mDrawerList.setAdapter(msgMenuAdapter);
		/*
		 * // mDrawerList.setAdapter(new ArrayAdapter<String>(this, //
		 * R.layout.drawer_list_item, mPlanetTitles));
		 */
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
		list = new ArrayList<String>();
		selectItem(MenuType.Home.ordinal());
		countDownOnlineTimer = new MyCountDownOnlineTimer(1000 * 5, 1000 * 5);

		CacheMemory.setOnlineChallengeStatus(GSKHomeActivity.this,
				CacheMemory.ONLINE_CHALLENGE_NO);

		configureCrash();

		// testCrash();
		// testMenthod();
	}

	private void testMenthod() {
		// TODO Auto-generated method stub
		if (ValidationUtility.isValidPassword("Ankur@123")) {
			Log.v("", "Ankur@123 true");
		}
		if (ValidationUtility.isValidPassword("Ankur123")) {
			Log.v("", "Ankur123 true");
		}
		if (ValidationUtility.isValidPassword("Ankur@abc")) {
			Log.v("", "Ankur@abc true");
		}
		if (ValidationUtility.isValidPassword("A@a1")) {
			Log.v("", "A@a1 true");
		}
		if (ValidationUtility.isValidPassword("Ankur@1")) {
			Log.v("", "Ankur@1 true");
		}

	}

	private void testCrash() {
		// TODO Auto-generated method stub
		try {
			int a = 13 / 0;
		} catch (Exception exception) {
			handler.logCrashReport(exception);
		}
	}

	private void configureCrash() {
		// TODO Auto-generated method stub
		handler = GenericExceptionHandler.sharedInstance(this);
		handler.setIsLogEnable(true);
		String[] toRecipients = { "ankurm1593@gmail.com" };
		handler.setRecipientsEmail(toRecipients);
		handler.enableBackgroundMailTriggering(false);
		handler.registerForCapturingGenericException();

	}

	protected void repeatTimer() {
		// TODO Auto-generated method stub

		countDownOnlineTimer = new MyCountDownOnlineTimer(1000 * 5, 1000 * 5);
		countDownOnlineTimer.start();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.v("", "------------Offline-----------");
		if (countDownOnlineTimer != null)
			countDownOnlineTimer.cancel();
		this.notifyISOfflineOrOnline(0);
	}

	private void notifyISOfflineOrOnline(final int isOfflineOrOnline) {
		// TODO Auto-generated method stub
		ApplicationAsk aa = new ApplicationAsk(GSKHomeActivity.this, "",
				new ApplicationService() {
					private String resp;

					@Override
					public void postExecute() {
						// TODO Auto-generated method stub
						try {

							JSONObject acceptResponseObj = new JSONObject(resp);
							String status = acceptResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

							} else {
								Toast.makeText(
										GSKHomeActivity.this,
										""
												+ acceptResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {
						// TODO Auto-generated method stub
						JSONObject onlineStatuObj = new JSONObject();
						try {

							onlineStatuObj.put("IsOnline", isOfflineOrOnline);

						} catch (JSONException e) {

							e.printStackTrace();
						}

						String onlineStatuURL = URLGenerator
								.getUpdateOnlineStatusUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								onlineStatuURL, onlineStatuObj.toString(),
								CacheMemory.getToken(GSKHomeActivity.this),
								GSKHomeActivity.this);
					}

				});
		aa.setProgressHidden(true);
		aa.execute();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		CacheMemory.mActivity = this;

		Log.v("", "------------Online-----------");
		this.notifyISOfflineOrOnline(1);
		if (countDownOnlineTimer != null)
			countDownOnlineTimer.start();
	}

	public void menuButton(View v) {
		if (i == 0) {
			mDrawerLayout.openDrawer(mDrawerList);
			i = 1;
		} else {
			i = 0;
			mDrawerLayout.closeDrawer(mDrawerList);
		}

	}

	public void notificationButton(View v) {

		this.loadNotifications();
	}

	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
		}
	}

	private void selectItem(final int position) {
		
		CacheMemory.setOnlineChallengeStatus(GSKHomeActivity.this,
				CacheMemory.ONLINE_CHALLENGE_NO);
		selectedPosition = position;
		switch (position) {
		case 0:
			this.loadHome();
			break;
		case 1:
			// this.loadImportant();
			//Toast.makeText(this, "Coming Soon", Toast.LENGTH_LONG).show();
			this.loadAppTutorialEvent();
			// this.loadWeb();
			break;
		case 2:
			// this.loadArchieves();
			this.loadMyScore();
			// Toast.makeText(this, "Under Progress", Toast.LENGTH_LONG).show();
			break;
		case 3:
			this.loadMyAccount();
			break;
		case 4:
			this.loadfriends();
			break;

		case 5:
			this.loadPIWeb();
			break;

		case 6:
			//this.loadReportAdverseEvent();
			this.loadAEWeb();
			break;

		case 7:
			this.loadAskQueryEvent();
			break;

		case 8:

			this.loadFeedback();
			break;

		case 9:

			this.logout();

			break;

		default:
			break;
		}

	}

	private void loadReportAdverseEvent() {
		// TODO Auto-generated method stub
		Fragment fragment = new ReportAdverseEventFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack("ReportAdverseEventFragment");
		ft.replace(R.id.content_frame, fragment, "ReportAdverseEventFragment");

		ft.commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	private void loadAskQueryEvent() {
		// TODO Auto-generated method stub
		Fragment fragment = new AskQueryFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack("AskQueryFragment");
		ft.replace(R.id.content_frame, fragment, "AskQueryFragment");

		ft.commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}
	
	private void loadAppTutorialEvent() {
		// TODO Auto-generated method stub
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		fragment.setScreenName(6);
		ft.addToBackStack("AppTutorial");
		ft.replace(R.id.content_frame, fragment, "AppTutorial");

		ft.commit();
		
		/*Intent webIntent = new Intent(this, LoadWebActivity.class);
    	webIntent.putExtra("screen", 4);
        startActivity(webIntent);*/

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	private void logout() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setMessage("Do you want to logout ?").setTitle("Logout");

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				CacheMemory.setOnlineChallengeStatus(GSKHomeActivity.this,
						CacheMemory.ONLINE_CHALLENGE_NO);

				logoutService();

			}

		});

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.dismiss();

					}

				});

		AlertDialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.show();

	}

	private void exitGame() {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setMessage("Are you sure want to quit game ?").setTitle("");

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			
				CacheMemory.setOnlineChallengeStatus(GSKHomeActivity.this,
						CacheMemory.ONLINE_CHALLENGE_NO);
				Log.v("getFragmentManager().getBackStackEntryCount()", "getFragmentManager().getBackStackEntryCount() "+getFragmentManager().getBackStackEntryCount());
				if (getFragmentManager().getBackStackEntryCount() == 1)
				{
					getFragmentManager().popBackStack();
					removeCurrentFragment();
					selectItem(MenuType.Home.ordinal());
				}
				else
				{
				getFragmentManager().popBackStack();
				removeCurrentFragment();
				}
				
			
			}

		});

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.dismiss();

					}

				});

		AlertDialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.show();

	}

	protected void logoutService() {
		// TODO Auto-generated method stub
		ApplicationAsk aa = new ApplicationAsk(GSKHomeActivity.this,
				"Please wait", new ApplicationService() {
					private String resp;

					@Override
					public void postExecute() {
						// TODO Auto-generated method stub
						try {

							if (countDownOnlineTimer != null)
								countDownOnlineTimer.cancel();
							Intent intent = new Intent(GSKHomeActivity.this,
									LoginActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
									| Intent.FLAG_ACTIVITY_CLEAR_TASK
									| Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);

							JSONObject acceptResponseObj = new JSONObject(resp);
							String status = acceptResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

							} else {
								Toast.makeText(
										GSKHomeActivity.this,
										""
												+ acceptResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {
						// TODO Auto-generated method stub

						String onlineStatuURL = URLGenerator.getLogoutUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								onlineStatuURL, "",
								CacheMemory.getToken(GSKHomeActivity.this),
								GSKHomeActivity.this);
					}

				});
		aa.setProgressHidden(true);
		aa.execute();
	}

	private void loadWeb() {
		// TODO Auto-generated method stub
		Fragment fragment = new LoadWebPageFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment, "LoadWebPageFragment");

		ft.commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	// http://india-pharma.gsk.com/en-in/products/prescribing-information
	private void loadPIWeb() {
		// TODO Auto-generated method stub
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		fragment.setScreenName(1);
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment, "LoadWebPageFragment");

		ft.commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}
	
	private void loadAEWeb() {
		// TODO Auto-generated method stub
		LoadWebPageFragment fragment = new LoadWebPageFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		fragment.setScreenName(5);
		ft.addToBackStack("LoadWebPageFragment");
		ft.replace(R.id.content_frame, fragment, "LoadWebPageFragment");

		ft.commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	private void loadInvite() {
		// TODO Auto-generated method stub
		Fragment fragment = new InviteFriendFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		ft.addToBackStack("InviteFriendFragment");
		ft.replace(R.id.content_frame, fragment, "InviteFriendFragment");

		ft.commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	private void loadfriends() {
		/*
		 * new ApplicationAsk(this, "Fetching friends . .", new
		 * ApplicationService() { private String resp, inviteResp;
		 * 
		 * @Override public void execute() {
		 * 
		 * String friendListURL = URLGenerator.getfriendslist();
		 * CommunicationService commService = new CommunicationService(); resp =
		 * commService.sendSyncRequestWithToken( friendListURL, "",
		 * CacheMemory.getToken(GSKHomeActivity.this)); String inviteListURL =
		 * URLGenerator.getInviteelist(); inviteResp =
		 * commService.sendSyncRequestWithToken( inviteListURL, "",
		 * CacheMemory.getToken(GSKHomeActivity.this)); // InviteeList }
		 * 
		 * @Override public void postExecute() {
		 * 
		 * try {
		 * 
		 * JSONObject friendResponseObj = new JSONObject(resp); JSONObject
		 * inviteResponseObj = new JSONObject( inviteResp); String status =
		 * friendResponseObj .getString("status"); if
		 * (status.equalsIgnoreCase("success")) {
		 * 
		 * JSONArray dataObj = friendResponseObj .getJSONArray("data");
		 * JSONArray inviteArray = inviteResponseObj .getJSONArray("data");
		 * 
		 * Fragment fragment = new FriendListFragment( dataObj, inviteArray,
		 * GSKHomeActivity.this); FragmentTransaction ft = getFragmentManager()
		 * .beginTransaction();
		 * 
		 * ft.addToBackStack("FriendListFragment");
		 * ft.replace(R.id.content_frame, fragment, "FriendListFragment");
		 * 
		 * ft.commit();
		 * 
		 * // update selected item and title, then close // the drawer
		 * mDrawerList.setItemChecked(selectedPosition, true);
		 * setTitle(menuTitles[selectedPosition]);
		 * mDrawerLayout.closeDrawer(mDrawerList);
		 * 
		 * } else { Toast.makeText( GSKHomeActivity.this, "" + friendResponseObj
		 * .getString("message"), Toast.LENGTH_LONG).show(); }
		 * 
		 * } catch (JSONException e) {
		 * 
		 * e.printStackTrace(); } } }).execute();
		 */

		Fragment fragment = new FriendListFragment(GSKHomeActivity.this);
		FragmentTransaction ft = getFragmentManager().beginTransaction();

		ft.addToBackStack("FriendListFragment");
		ft.replace(R.id.content_frame, fragment, "FriendListFragment");

		ft.commit();

		// update selected item and title, then close
		// the drawer
		mDrawerList.setItemChecked(selectedPosition, true);
		setTitle(menuTitles[selectedPosition]);
		mDrawerLayout.closeDrawer(mDrawerList);

	}

	private void loadNotifications() {

		Fragment fragment = new NotificationListFragment();
		FragmentTransaction ft = getFragmentManager().beginTransaction();

		ft.addToBackStack("NotificationListFragment");
		ft.replace(R.id.content_frame, fragment, "NotificationListFragment");

		ft.commit();

		// TODO Auto-generated method stub
		/*
		 * new ApplicationAsk(this, "Fetching notifications . . ", new
		 * ApplicationService() {
		 * 
		 * private String resp;
		 * 
		 * @Override public void postExecute() {
		 * 
		 * try {
		 * 
		 * JSONObject notificationResponseObj = new JSONObject( resp); String
		 * status = notificationResponseObj .getString("status");
		 * 
		 * if (status.equalsIgnoreCase("success")) {
		 * 
		 * JSONArray notificationArray = notificationResponseObj
		 * .getJSONArray("data");
		 * 
		 * if (notificationArray.length() > 0) { Fragment fragment = new
		 * NotificationListFragment( notificationArray); FragmentTransaction ft
		 * = getFragmentManager() .beginTransaction();
		 * 
		 * ft.addToBackStack("NotificationListFragment");
		 * ft.replace(R.id.content_frame, fragment, "NotificationListFragment");
		 * 
		 * ft.commit(); } else { Toast.makeText(GSKHomeActivity.this,
		 * "No notifications to display", Toast.LENGTH_LONG).show(); } } else {
		 * Toast.makeText( GSKHomeActivity.this, "" + notificationResponseObj
		 * .getString("message"), Toast.LENGTH_LONG).show(); }
		 * 
		 * } catch (JSONException e) {
		 * 
		 * e.printStackTrace(); }
		 * 
		 * }
		 * 
		 * @Override public void execute() {
		 * 
		 * String notificationURL = URLGenerator .getMyNotificationUrl();
		 * 
		 * JSONObject param = new JSONObject(); try { param.put("IsRead", "1");
		 * } catch (JSONException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * CommunicationService commService = new CommunicationService(); resp =
		 * commService.sendSyncRequestWithToken( notificationURL,
		 * param.toString(), CacheMemory.getToken(GSKHomeActivity.this));//
		 * "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0="
		 * );//preferences.getString("Token"));
		 * 
		 * // resp = // ValidationUtility.readFromAssets(GSKHomeActivity.this,
		 * // "notificationresponse.json");
		 * 
		 * } }).execute();
		 */

	}

	private void loadMyAccount() {
		// TODO Auto-generated method stub
		new ApplicationAsk(this, "Fetching Account information . .",
				new ApplicationService() {

					private String resp;

					@Override
					public void postExecute() {
						Log.v(TAG, " Response : " + resp);

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

								JSONObject dataObj = dashboardResponseObj
										.getJSONArray("data").getJSONObject(0);

								CacheMemory.setUserDetails(
										GSKHomeActivity.this,
										dataObj.toString());

								Fragment fragment = new MyAccountFragment(
										dataObj);
								FragmentTransaction ft = getFragmentManager()
										.beginTransaction();

								ft.addToBackStack("MyAccountFragment");
								ft.replace(R.id.content_frame, fragment,
										"MyAccountFragment");

								ft.commit();

								// update selected item and title, then close
								// the drawer
								mDrawerList.setItemChecked(selectedPosition,
										true);
								setTitle(menuTitles[selectedPosition]);
								mDrawerLayout.closeDrawer(mDrawerList);

							} else {
								Toast.makeText(
										GSKHomeActivity.this,
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {

						String loginURL = URLGenerator.getMyAccountUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(loginURL,
								"", CacheMemory.getToken(GSKHomeActivity.this),
								GSKHomeActivity.this);// "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0=");//preferences.getString("Token"));

					}
				}).execute();

	}

	private void loadFeedback() {
		// TODO Auto-generated method stub
		new ApplicationAsk(this, "Loading feeback screen . .",
				new ApplicationService() {

					private String resp;

					@Override
					public void postExecute() {

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

								JSONArray dataObj = dashboardResponseObj
										.getJSONArray("data");

								Fragment fragment = new FeedbackFragment(
										dataObj);
								FragmentTransaction ft = getFragmentManager()
										.beginTransaction();

								ft.addToBackStack("FeedbackFragment");
								ft.replace(R.id.content_frame, fragment,
										"FeedbackFragment");

								ft.commit();

								// update selected item and title, then close
								// the drawer
								mDrawerList.setItemChecked(selectedPosition,
										true);
								setTitle(menuTitles[selectedPosition]);
								mDrawerLayout.closeDrawer(mDrawerList);

							} else {
								Toast.makeText(
										GSKHomeActivity.this,
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {

						String loginURL = URLGenerator.getFeedbacksUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(loginURL,
								"", CacheMemory.getToken(GSKHomeActivity.this),
								GSKHomeActivity.this);// "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0=");//preferences.getString("Token"));

					}
				}).execute();

	}

	private void loadMyScore() {
		// TODO Auto-generated method stub
		new ApplicationAsk(this, "Fetching score", new ApplicationService() {

			private String resp;

			@Override
			public void postExecute() {

				try {

					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = dashboardResponseObj
								.getJSONArray("data");
						String totalPoints = null;
						
						if(dataObj.length() > 0)
						{
							if(dataObj.getJSONObject(0).has("totalPoints"))
							{
								totalPoints = dataObj.getJSONObject(0).getString("totalPoints");
							}
						}
						
						MyScoreFragment fragment = new MyScoreFragment(dataObj);
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();
						fragment.setTotalPoints(totalPoints);
						ft.addToBackStack("MyScoreFragment");
						ft.replace(R.id.content_frame, fragment,
								"MyScoreFragment");

						ft.commit();

						// update selected item and title, then close the drawer
						mDrawerList.setItemChecked(selectedPosition, true);
						setTitle(menuTitles[selectedPosition]);
						mDrawerLayout.closeDrawer(mDrawerList);

					} else {
						Toast.makeText(GSKHomeActivity.this,
								"" + dashboardResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {

				// SecurePreferences preferences = new
				// SecurePreferences(GSKHomeActivity.this, "my-preferences",
				// "SometopSecretKey1235", true);
				// preferences.put("Token", Token);
				String loginURL = URLGenerator.getMyScoreUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(loginURL, "",
						CacheMemory.getToken(GSKHomeActivity.this),
						GSKHomeActivity.this);// "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0=");//preferences.getString("Token"));

				// return resp;

			}
		}).execute();

	}

	private void loadHome() {

		new ApplicationAsk(this, "Please wait . . ", new ApplicationService() {

			private String resp;

			@Override
			public void postExecute() {

				try {

					JSONObject dashboardResponseObj = new JSONObject(resp);
					String status = dashboardResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONObject dataObj = dashboardResponseObj.getJSONArray(
								"data").getJSONObject(0);
						JSONArray topicArray = dataObj.getJSONArray("All");
						JSONArray followedtopicArray = dataObj
								.getJSONArray("Followed");
						JSONArray badgeListArray = dataObj
								.getJSONArray("BadgeList");
						JSONObject userJson = dataObj
								.getJSONArray("UserDetail").getJSONObject(0);
						String fullname = userJson.getString("fullname");
						int points = userJson.getInt("Points");
						String userId = userJson.getString("UserId");
						String therapy = userJson.getString("Therapy");
						String gender = userJson.getString("Gender");
						boolean isMale = false;
						if (gender.equalsIgnoreCase("1"))
							isMale = true;
						else
							isMale = false;

						CacheMemory.setUserId(GSKHomeActivity.this, userId);
						CacheMemory.setTherapy(GSKHomeActivity.this, therapy);
						CacheMemory
								.setPoints(GSKHomeActivity.this, "" + points);

						Fragment fragment = new DashboardFragment(topicArray,
								followedtopicArray, badgeListArray, fullname,
								String.valueOf(points), userId, isMale);
						FragmentTransaction ft = getFragmentManager()
								.beginTransaction();

						ft.addToBackStack("DashboardFragment");
						ft.replace(R.id.content_frame, fragment,
								"DashboardFragment");

						ft.commit();

						// update selected item and title, then close the drawer
						mDrawerList.setItemChecked(selectedPosition, true);
						setTitle(menuTitles[selectedPosition]);
						mDrawerLayout.closeDrawer(mDrawerList);

					} else {
						Toast.makeText(GSKHomeActivity.this,
								"" + dashboardResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {

				String loginURL = URLGenerator.getGetAllDashboardDataUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(loginURL, "",
						CacheMemory.getToken(GSKHomeActivity.this),
						GSKHomeActivity.this);// "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0=");//preferences.getString("Token"));

				// return resp;

			}
		}).execute();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		Log.v("", "event" + event);
		Log.v("", "event" + event.toString());
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			Fragment f = getFragmentManager().findFragmentById(
					R.id.content_frame);
			if (f instanceof OnlineQuestionListFragment
					|| f instanceof QuestionListFragment
					|| f instanceof ChallengeeQuestionListFragment) {
				// show alert
				exitGame();
				return true;
			} else if (getFragmentManager().getBackStackEntryCount() == 1) {
				this.finish();
				// return false;
			} else {

				getFragmentManager().popBackStack();
				removeCurrentFragment();
				return false;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

	public void removeCurrentFragment() {
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		Fragment currentFrag = getFragmentManager().findFragmentById(
				R.id.content_frame);

		if (currentFrag != null)
			transaction.remove(currentFrag);

		transaction.commit();

	}
}
