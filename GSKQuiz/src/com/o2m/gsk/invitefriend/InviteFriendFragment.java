package com.o2m.gsk.invitefriend;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.o2m.gsk.GSKHomeActivity;
import com.o2m.gsk.LoadWebPageFragment;
import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;
import com.o2m.gsk.utility.ValidationUtility;

public class InviteFriendFragment extends Fragment implements OnClickListener {

	protected String TAG ;
	
	private EditText emailET;
	
	private Button sendBtn;
	

	public InviteFriendFragment() {
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.invite_friend_fragment, container,
				false);
		emailET = (EditText) rootView.findViewById(R.id.emailIdET);
		
		sendBtn = (Button) rootView.findViewById(R.id.sendBtn);
		sendBtn.setOnClickListener(this);
		

		

		return rootView;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		emailET.setText("");
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		String email = emailET.getText().toString().trim();
		if(ValidationUtility.isValidEmail(emailET.getText().toString().trim()) && !email.equalsIgnoreCase(""))
		{
			this.sendInviteFriend(email);
		}
		else
		{
			Toast.makeText(getActivity(), " Please enter valid Email Address", Toast.LENGTH_SHORT).show();
		}
		
	}

	private void sendInviteFriend(final String email) {
		// TODO Auto-generated method stub
		new ApplicationAsk(InviteFriendFragment.this.getActivity(),"Inviting friend",
				new ApplicationService() {

					private String resp;

					@Override
					public void postExecute() {
						Log.v(TAG, " onPostExecute Login Response : " + resp);

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

								JSONArray dataObj = dashboardResponseObj
										.getJSONArray("data");
								
								Toast.makeText(
										InviteFriendFragment.this.getActivity(),
										"Invitation Sent Successfully",
										Toast.LENGTH_LONG).show();

								Fragment fragment = new InviteMoreFriendFragment();
								FragmentTransaction ft = getFragmentManager().beginTransaction();
								ft.addToBackStack("InviteMoreFriendFragment");
								ft.replace(R.id.content_frame, fragment, "InviteMoreFriendFragment");

								ft.commit();

							} else {
								Toast.makeText(
										InviteFriendFragment.this.getActivity(),
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {

						JSONObject emailJson = new JSONObject();
						try {
							emailJson.put("Email", email);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String feedbackURL = URLGenerator.getAddFriendUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								feedbackURL, emailJson.toString(), CacheMemory
										.getToken(InviteFriendFragment.this
												.getActivity()),InviteFriendFragment.this
												.getActivity());// "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0=");//preferences.getString("Token"));

						// return resp;

					}
				}).execute();

	}

	
}
