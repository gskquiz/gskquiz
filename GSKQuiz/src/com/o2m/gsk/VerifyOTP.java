package com.o2m.gsk;

import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.CryptedConfiguration;
import com.o2m.gsk.utility.URLGenerator;

import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class VerifyOTP extends Dialog implements
android.view.View.OnClickListener, TextWatcher, OnFocusChangeListener,
android.view.View.OnKeyListener {

	public Activity activity;
	public Dialog dialog;
	public Button verifyotpBtn, resendBtn, cancelBtn;
	public String userId;
	// private EditText verifyOtpET;
	private EditText mPinFirstDigitEditText;
	private EditText mPinSecondDigitEditText;
	private EditText mPinThirdDigitEditText;
	private EditText mPinForthDigitEditText;
	private EditText mPinHiddenEditText;

	public VerifyOTP(Activity context , String _userId) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;
		this.userId = _userId;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.verify_otp);

		verifyotpBtn = (Button) findViewById(R.id.verifyOTPBtn);
		resendBtn = (Button) findViewById(R.id.resendOTPBtn);
		// verifyOtpET = (EditText) findViewById(R.id.otpET);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
		mPinFirstDigitEditText = (EditText) findViewById(R.id.pin_first_edittext);
		mPinSecondDigitEditText = (EditText) findViewById(R.id.pin_second_edittext);
		mPinThirdDigitEditText = (EditText) findViewById(R.id.pin_third_edittext);
		mPinForthDigitEditText = (EditText) findViewById(R.id.pin_forth_edittext);
		// mPinFifthDigitEditText = (EditText)
		// findViewById(R.id.pin_fifth_edittext);
		mPinHiddenEditText = (EditText) findViewById(R.id.pin_hidden_edittext);
		setPINListeners();

		verifyotpBtn.setOnClickListener(this);

		resendBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);
		setFocusedPinBackground(mPinFirstDigitEditText);
	}

	private void setPINListeners() {
		mPinHiddenEditText.addTextChangedListener(this);

		mPinFirstDigitEditText.setOnFocusChangeListener(this);
		mPinSecondDigitEditText.setOnFocusChangeListener(this);
		mPinThirdDigitEditText.setOnFocusChangeListener(this);
		mPinForthDigitEditText.setOnFocusChangeListener(this);
		// mPinFifthDigitEditText.setOnFocusChangeListener(this);

		mPinFirstDigitEditText.setOnKeyListener(this);
		mPinSecondDigitEditText.setOnKeyListener(this);
		mPinThirdDigitEditText.setOnKeyListener(this);
		mPinForthDigitEditText.setOnKeyListener(this);
		// mPinFifthDigitEditText.setOnKeyListener(this);
		mPinHiddenEditText.setOnKeyListener(this);
	}

	@Override
	public void afterTextChanged(Editable s) {
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	/**
	 * Hides soft keyboard.
	 * 
	 * @param editText
	 *            EditText which has focus
	 */
	public void hideSoftKeyboard(EditText editText) {
		if (editText == null)
			return;

		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Service.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	/*
	 * public class MainLayout extends LinearLayout {
	 * 
	 * public MainLayout(Context context, AttributeSet attributeSet) {
	 * super(context, attributeSet); LayoutInflater inflater = (LayoutInflater)
	 * context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 * inflater.inflate(R.layout.main, this); }
	 * 
	 * @Override protected void onMeasure(int widthMeasureSpec, int
	 * heightMeasureSpec) { final int proposedHeight =
	 * MeasureSpec.getSize(heightMeasureSpec); final int actualHeight =
	 * getHeight();
	 * 
	 * Log.d("TAG", "proposed: " + proposedHeight + ", actual: " +
	 * actualHeight);
	 * 
	 * if (actualHeight >= proposedHeight) { // Keyboard is shown if
	 * (mPinHiddenEditText.length() == 0)
	 * setFocusedPinBackground(mPinFirstDigitEditText); else
	 * setDefaultPinBackground(mPinFirstDigitEditText); }
	 * 
	 * super.onMeasure(widthMeasureSpec, heightMeasureSpec); } }
	 */

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == verifyotpBtn) {

			String otp = mPinFirstDigitEditText.getText().toString()
					+ mPinSecondDigitEditText.getText().toString()
					+ mPinThirdDigitEditText.getText().toString()
					+ mPinForthDigitEditText.getText().toString();
			if (otp.length() != 4) {
				Toast.makeText(activity, "Please enter 4 didt OTP",
						Toast.LENGTH_SHORT).show();
				// verifyOtpET.setError("Please enter 4 didt OTP");
				return;
			}

			verifyOTP(otp);
		}

		else if (arg0 == resendBtn) {

			resendOTP();
		} else if (arg0 == cancelBtn) {

			this.dismiss();
		}

	}

	private void verifyOTP(final String otp) {
		// TODO Auto-generated method stub
		new ApplicationAsk(activity, "Verifying OTP", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {

					JSONObject registraionResponseObj = new JSONObject(resp);
					String status = registraionResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						// {"status":"success", "message":"", "data"
						// :[{"Valid":"1"}]}
						/*String validFlag = registraionResponseObj
								.getJSONArray("data").getJSONObject(0)
								.getString("Valid").trim();
						String firstChar = String.valueOf(validFlag.charAt(0));
						
						if (firstChar.equalsIgnoreCase("1")) {
							VerifyOTP.this.dismiss();
							PasswordSetUp ps = new PasswordSetUp(activity);
							ps.setCancelable(false);
							ps.show();
						}*/
						String otpToken = registraionResponseObj
								.getJSONArray("data").getJSONObject(0)
								.getString("token").trim();
						CacheMemory.setOTPToken(activity, otpToken);
						VerifyOTP.this.dismiss();
						PasswordSetUp ps = new PasswordSetUp(activity);
						ps.setCancelable(false);
						ps.show();
						
						
					} else {
						Toast.makeText(
								activity,
								""
										+ registraionResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject verifyObj = new JSONObject();
				try {
					
					verifyObj.put("UserId", VerifyOTP.this.userId);
					verifyObj.put("OTP", otp);
					//String abc  = "ZYEQ2VNsQ73aDP6EQhoGdRpqgMQnYxlw1AhvbuI1SBSkVm3xikE8E/O7GwB91LWv";
					// Log.v("",
					// "Base64 : "+Base64.encodeToString(otp.getBytes(),
					// Base64.DEFAULT));

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String validateURL = URLGenerator.getValidateOTPURL();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				CommunicationService commService = new CommunicationService();
				String req = verifyObj.toString().replace("\\/", "/");
				Log.v("", "req " +req);
				resp = commService.sendSyncRequest(validateURL,
						req, VerifyOTP.this.activity);
			}

		}).execute();
	}

	private void resendOTP() {
		// TODO Auto-generated method stub
		new ApplicationAsk(activity, "Requesting Resend OTP", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {
				// TODO Auto-generated method stub
				try {

					JSONObject registraionResponseObj = new JSONObject(resp);
					String status = registraionResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						Toast.makeText(
								activity,
								"OTP has been sent to registered mobile number",
								Toast.LENGTH_LONG).show();

					} else {
						Toast.makeText(
								activity,
								""
										+ registraionResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				// TODO Auto-generated method stub
				JSONObject resendObj = new JSONObject();
				try {
					
					resendObj.put("UserId", VerifyOTP.this.userId);

				} catch (JSONException e) {

					e.printStackTrace();
				}

				String resendURL = URLGenerator.getResendOTPURL();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				CommunicationService commService = new CommunicationService();
				String req = resendObj.toString().replace("\\/", "/");
				Log.v("", "req " +req);
				resp = commService.sendSyncRequest(resendURL,
						req , VerifyOTP.this.activity);
			}

		}).execute();
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// TODO Auto-generated method stub

		setDefaultPinBackground(mPinFirstDigitEditText);
		setDefaultPinBackground(mPinSecondDigitEditText);
		setDefaultPinBackground(mPinThirdDigitEditText);
		setDefaultPinBackground(mPinForthDigitEditText);
		// setDefaultPinBackground(mPinFifthDigitEditText);

		if (s.length() == 0) {
			setFocusedPinBackground(mPinFirstDigitEditText);
			mPinFirstDigitEditText.setText("");
		} else if (s.length() == 1) {
			setFocusedPinBackground(mPinSecondDigitEditText);
			mPinFirstDigitEditText.setText(s.charAt(0) + "");
			mPinSecondDigitEditText.setText("");
			mPinThirdDigitEditText.setText("");
			mPinForthDigitEditText.setText("");
			// mPinFifthDigitEditText.setText("");
		} else if (s.length() == 2) {
			setFocusedPinBackground(mPinThirdDigitEditText);
			mPinSecondDigitEditText.setText(s.charAt(1) + "");
			mPinThirdDigitEditText.setText("");
			mPinForthDigitEditText.setText("");
			// mPinFifthDigitEditText.setText("");
		} else if (s.length() == 3) {
			setFocusedPinBackground(mPinForthDigitEditText);
			mPinThirdDigitEditText.setText(s.charAt(2) + "");
			mPinForthDigitEditText.setText("");
			// mPinFifthDigitEditText.setText("");
		} else if (s.length() == 4) {
			setFocusedPinBackground(mPinForthDigitEditText);
			mPinForthDigitEditText.setText(s.charAt(3) + "");
			hideSoftKeyboard(mPinForthDigitEditText);
		}
	}

	private void setDefaultPinBackground(EditText editText) {
		setViewBackground(
				editText,
				activity.getResources().getDrawable(
						R.drawable.textfield_default_holo_light));
	}

	private void setFocusedPinBackground(EditText editText) {
		setViewBackground(
				editText,
				activity.getResources().getDrawable(
						R.drawable.textfield_focused_holo_light));
	}

	@SuppressWarnings("deprecation")
	public void setViewBackground(View view, Drawable background) {
		if (view == null || background == null)
			return;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			view.setBackground(background);
		} else {
			view.setBackgroundDrawable(background);
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		final int id = v.getId();
		switch (id) {
		case R.id.pin_first_edittext:
			if (hasFocus) {
				setFocus(mPinHiddenEditText);
				showSoftKeyboard(mPinHiddenEditText);
			}
			break;

		case R.id.pin_second_edittext:
			if (hasFocus) {
				setFocus(mPinHiddenEditText);
				showSoftKeyboard(mPinHiddenEditText);
			}
			break;

		case R.id.pin_third_edittext:
			if (hasFocus) {
				setFocus(mPinHiddenEditText);
				showSoftKeyboard(mPinHiddenEditText);
			}
			break;

		case R.id.pin_forth_edittext:
			if (hasFocus) {
				setFocus(mPinHiddenEditText);
				showSoftKeyboard(mPinHiddenEditText);
			}
			break;

		default:
			break;
		}
	}

	public static void setFocus(EditText editText) {
		if (editText == null)
			return;

		editText.setFocusable(true);
		editText.setFocusableInTouchMode(true);
		editText.requestFocus();
	}

	public void showSoftKeyboard(EditText editText) {
		if (editText == null)
			return;

		InputMethodManager imm = (InputMethodManager) activity
				.getSystemService(Service.INPUT_METHOD_SERVICE);
		imm.showSoftInput(editText, 0);
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			final int id = v.getId();
			switch (id) {
			case R.id.pin_hidden_edittext:
				if (keyCode == KeyEvent.KEYCODE_DEL) {
					/*
					 * if (mPinHiddenEditText.getText().length() == 5)
					 * mPinFifthDigitEditText.setText("");
					 */if (mPinHiddenEditText.getText().length() == 4)
						 mPinForthDigitEditText.setText("");
					 else if (mPinHiddenEditText.getText().length() == 3)
						 mPinThirdDigitEditText.setText("");
					 else if (mPinHiddenEditText.getText().length() == 2)
						 mPinSecondDigitEditText.setText("");
					 else if (mPinHiddenEditText.getText().length() == 1)
						 mPinFirstDigitEditText.setText("");

					 if (mPinHiddenEditText.length() > 0)
						 mPinHiddenEditText
						 .setText(mPinHiddenEditText
								 .getText()
								 .subSequence(0,
										 mPinHiddenEditText.length() - 1));

					 return true;
				}

				break;

			default:
				return false;
			}
		}

		return false;
	}

}
