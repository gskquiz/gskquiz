package com.o2m.gsk;

import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.BadgeType;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.CryptedConfiguration;
import com.o2m.gsk.utility.URLGenerator;

import android.app.Activity;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class BadgeDialog extends Dialog implements android.view.View.OnClickListener 
 {

	public Activity activity;
	public Dialog dialog;
	public BadgeType badgeType;  
	public String points;
	public Button cancelBtn;
	
	public BadgeDialog(Activity context , BadgeType _badgeType , String _points) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;
		this.badgeType = _badgeType;
		this.points = _points;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.badge_dialog);
		
		TextView titleTV = (TextView) findViewById(R.id.badgetitletxt);
		ImageView badgeIcon = (ImageView) findViewById(R.id.badgewonImg);
		TextView pointsearnedTV = (TextView) findViewById(R.id.pointsearnbadgetv);
		TextView nextTargetTV = (TextView) findViewById(R.id.nexttargetbadgetv);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
		cancelBtn.setOnClickListener(this);
		
		if(badgeType == BadgeType.BRONZE)
		{
			titleTV.setText("Bronze Badge Earned");
			badgeIcon.setImageResource(R.drawable.bronze);
			pointsearnedTV.setText("You have scored "+ points +" points");
			nextTargetTV.setText("Keep playing and earn 480 scores for a Silver Badge");
		}
		else if(badgeType == BadgeType.SILVER)
		{
			titleTV.setText("Silver Badge Earned");
			badgeIcon.setImageResource(R.drawable.silver);
			pointsearnedTV.setText("You have scored "+ points +" points");
			nextTargetTV.setText("Keep playing and earn 600 scores for a Gold Badge");
		}
		else if(badgeType == BadgeType.GOLD)
		{
			titleTV.setText("Gold Badge Earned");
			badgeIcon.setImageResource(R.drawable.gold);
			pointsearnedTV.setText("You have scored "+ points +" points");
			nextTargetTV.setText("Keep playing and earn 720 scores for a Platinum Badge");
		}
		else if(badgeType == BadgeType.PLATINUM)
		{
			titleTV.setText("Platinum Badge Earned");
			badgeIcon.setImageResource(R.drawable.platinum);
			pointsearnedTV.setVisibility(View.INVISIBLE);
			nextTargetTV.setText("Your knowledge on Dermatology level reach to Platinum Badge");
		}
		

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
		if(arg0 == cancelBtn)
		{
			this.dismiss();
		}
		
		
	}

	
}
