package com.o2m.gsk;

import com.o2m.gsk.utility.URLParams;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.MailTo;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LoadWebActivity extends Activity {
	
	private WebView mWebview ;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        
        int screen = getIntent().getExtras().getInt("screen");
        
        mWebview  = new WebView(this);

        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.getSettings().setDomStorageEnabled(true);
        String databasePath = this.getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath(); 
        mWebview.getSettings().setDatabasePath(databasePath);

        
        final Activity activity = this;

        mWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
               
            }
            
        });

       // mWebview .loadUrl("file:///android_asset/www/index.html");
        //http://training.wockhardt.com/mobile/html/
        if(screen == 1)        
        	mWebview .loadUrl("file:///android_asset/tnc.html");
        else if(screen == 2)        
        	mWebview .loadUrl("file:///android_asset/privacy.html");
        else if(screen == 3)        
        	mWebview .loadUrl("file:///android_asset/contactus.html");
        else if(screen == 4)        
        	mWebview .loadUrl("file:///android_asset/GSKApptutorial/index.html");
        
        
        	
        setContentView(mWebview );
        
        
        mWebview.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			       
				  /* URLParams params = URLParams.getInstance();
				 //  loggedIn("4570", "6224", "B", "L1", "r1", "z1", "815330"); 
				 //  Function name loggedIn(UserId, oldUserId, divId, levelCode, regionCode, zoneCode, employeeId)
				   
				   String UserId = params.getUser_id();
				   String oldUserId = params.getOld_user_id();
				   String divId = params.getDiv_code();
				   String levelCode = params.getLevel_code();
				   String regionCode = params.getRegion_code();
				   String zoneCode = params.getZone_code();
				   String employeeId = params.getEmployee_id();
				   
				   Log.v("WEBACtivity", "JS Call");
				   view.loadUrl("javascript:loggedIn(\""+UserId+"\", \""+oldUserId+"\", \""+divId+"\",\""+levelCode+"\",\""+regionCode+"\",\""+zoneCode+"\", \""+employeeId+"\")");
*/					
				 
			    }
			   
			   @Override
	            public boolean shouldOverrideUrlLoading(WebView view, String url) {
	            	// TODO Auto-generated method stub
	            	Log.v("", "url  " + url);
	            	if(url.startsWith("mailto:"))
	            	{
	            		MailTo mt = MailTo.parse(url);
	            		
	            		Intent i = new Intent(Intent.ACTION_SEND);
	            		i.setType("text/plain");
	            		i.putExtra(Intent.EXTRA_EMAIL, new String[]{mt.getTo()});
	            		activity.startActivity(i);
	            		view.reload();
	            	}
	            	
	            	return true;
	            }
			});
        
        

    }


}
