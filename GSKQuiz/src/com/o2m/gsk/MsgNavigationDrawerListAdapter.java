package com.o2m.gsk;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class MsgNavigationDrawerListAdapter extends BaseAdapter
{
	Context context;

	String[] fragments;

	public MsgNavigationDrawerListAdapter(Context context, String[] mMenuTitles)
	{
		this.context = context;
		this.fragments = mMenuTitles;

	}

	@Override
	public int getCount()
	{

		return fragments.length;
	}

	@Override
	public Object getItem(int pos)
	{

		return fragments[pos];
	}

	@Override
	public long getItemId(int arg0)
	{

		return 0;
	}

	@Override
	public View getView(int postion, View view, ViewGroup parent)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null)
		{
			holder = new ViewHolder();
			view = inflater.inflate(R.layout.drawer_list, parent, false);
			holder.Icon = (ImageView) view.findViewById(R.id.menuIcon);
			holder.txtView_Fragment = (TextView) view.findViewById(R.id.txt_fragment);
			view.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) view.getTag();
		}
		try
		{
			holder.txtView_Fragment.setText(fragments[postion]);

			if (postion == 0)
			{
				holder.Icon.setBackgroundResource(R.drawable.mhome);
			}
			else if (postion == 1)
			{
				holder.Icon.setBackgroundResource(R.drawable.mapp_tutorial);
			}
			else if (postion == 2)
			{
				holder.Icon.setBackgroundResource(R.drawable.mleaderboard);
			}
			else if (postion == 3)
			{
				holder.Icon.setBackgroundResource(R.drawable.myaccount);
			}
			else if (postion == 4)
			{
				holder.Icon.setBackgroundResource(R.drawable.myfriends);
			}
			else if (postion == 5)
			{
				holder.Icon.setBackgroundResource(R.drawable.minvite);
			}
			else if (postion == 6)
			{
				holder.Icon.setBackgroundResource(R.drawable.mreport);
			}
			else if (postion == 7)
			{
				holder.Icon.setBackgroundResource(R.drawable.maskquery);
			}
			else if (postion == 8)
			{
				holder.Icon.setBackgroundResource(R.drawable.m_feedback);
			}
			else if (postion == 9)
			{
				holder.Icon.setBackgroundResource(R.drawable.mlogout);
			}
			
		}
		catch (Exception e)
		{

		}
		return view;
	}

	private static class ViewHolder
	{
		public ImageView Icon;

		public TextView txtView_Fragment;

	}
}
