package com.o2m.gsk;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class SplashActivity extends Activity implements View.OnClickListener {

    private Button registerBtn,loginBtn;
    private TextView privacyTXT, contactTXT;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ImageView bgimageView = ( ImageView) findViewById(R.id.bgImageView);
        bgimageView.setBackgroundResource(R.drawable.login_bg_animation);
        AnimationDrawable anim = (AnimationDrawable) bgimageView.getBackground();
        anim.start();

        registerBtn = (Button) findViewById(R.id.registerBtn);
        loginBtn = (Button) findViewById(R.id.loginBtn);
        
        privacyTXT = (TextView) findViewById(R.id.privacylink);
        contactTXT = (TextView) findViewById(R.id.contactuslink);
       /* privacyTXT.setMovementMethod(LinkMovementMethod.getInstance());
        contactTXT.setMovementMethod(LinkMovementMethod.getInstance());*/
        
        
        privacyTXT.setOnClickListener(this);
        contactTXT.setOnClickListener(this);

        registerBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == loginBtn)
        {
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
        }

        else if (v == registerBtn)
        {
            Intent registerIntent = new Intent(this, RegistrationActivity.class);
           startActivity(registerIntent);
        }
        else if(v == privacyTXT)
        {
        	Intent webIntent = new Intent(this, LoadWebActivity.class);
        	webIntent.putExtra("screen", 2);
            startActivity(webIntent);
        }
        else if(v == contactTXT)
        {
        	Intent webIntent = new Intent(this, LoadWebActivity.class);
        	webIntent.putExtra("screen", 3);
            startActivity(webIntent);
        }
    }
}
