package com.o2m.gsk.myfriends;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.challenge.OnlineQuestionListFragment;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class FriendAdapter extends BaseAdapter {

	private JSONArray items;
	private Activity context;

	public FriendAdapter(Activity _context, JSONArray _items) {
		this.items = _items;
		this.context = _context;
	}

	@Override
	public int getCount() {

		return this.items.length();
	}

	@Override
	public Object getItem(int position) {

		try {
			return this.items.get(position);
		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public long getItemId(int arg0) {

		return 0;
	}

	@Override
	public View getView(final int position, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ViewHolder holder = new ViewHolder();
		if (view == null) {

			holder = new ViewHolder();
			view = inflater
					.inflate(R.layout.adapter_friendslist, parent, false);
			holder.txtView_msgTitle = (TextView) view
					.findViewById(R.id.name_friend);

			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		try {
			final JSONObject msgObj = (JSONObject) this.items.get(position);

			holder.txtView_msgTitle.setText("" + msgObj.getString("Fullname"));
			Button deleteBtn = (Button) view.findViewById(R.id.deletebtn);
			deleteBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					String friendId = null;
					try {
						friendId = msgObj.getString("Id");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (friendId != null)
						showAlertAndPopBack(friendId, position);
						
				}
			});

		} catch (org.json.JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * if (position % 2 == 0) {
		 * view.setBackgroundColor(Color.parseColor(context.getResources()
		 * .getString(R.string.gray_gradient_1))); }
		 */
		return view;

	}

	protected void deleteSubmit(final String friendId, final int position) {
		// TODO Auto-generated method stub

		new ApplicationAsk(context, "Deleting friend", new ApplicationService() {
			private String resp;

			@Override
			public void postExecute() {

				try {

					JSONObject deleteResponseObj = new JSONObject(resp);
					String status = deleteResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						FriendAdapter.this.items = RemoveJSONArray(items,
								position);
						FriendAdapter.this.notifyDataSetChanged();

					} else {

						Toast.makeText(context,
								"" + deleteResponseObj.getString("message"),
								Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject subTopicReq = new JSONObject();
				try {
					subTopicReq.put("Id", "" + friendId);
					subTopicReq.put("Action", "1");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				String deleteFriendURL = URLGenerator
						.getDeleteUnDeleteFriendUrl();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(deleteFriendURL,
						subTopicReq.toString(), CacheMemory.getToken(context), context);// preferences.getString("Token"));

				// return resp;

			}
		}).execute();

	}

	private static class ViewHolder {

		public TextView txtView_msgTitle;

	}

	public JSONArray getItems() {
		return items;
	}

	public void setItems(JSONArray items) {
		this.items = items;
	}

	public JSONArray RemoveJSONArray(JSONArray jarray, int pos) {

		JSONArray Njarray = new JSONArray();
		try {
			for (int i = 0; i < jarray.length(); i++) {
				if (i != pos)
					Njarray.put(jarray.get(i));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Njarray;

	}
	
	public void showAlertAndPopBack(final String friendId, final int position) {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		// 2. Chain together various setter methods to set the dialog
		// characteristics
		builder.setMessage("Are you sure want to delete?").setTitle("");

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User clicked OK button
				deleteSubmit(friendId, position);
				
			}

		});
		
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User clicked OK button

				
			}

		});

		AlertDialog dialog = builder.create();
		dialog.setCancelable(false);
		dialog.show();
	}


}
