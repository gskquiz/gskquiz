package com.o2m.gsk.myfriends;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.GSKHomeActivity;
import com.o2m.gsk.R;
import com.o2m.gsk.invitefriend.InviteFriendFragment;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class FriendListFragment extends Fragment implements OnClickListener {

	private JSONArray friendList, inviteList;
	ListView list_friends;
	private static FriendAdapter adapter;
	private Button myFriendsBtn, invitesBtn, invitenowBtn, addFrndBtn;
	private TextView invitetitleTV;
	Context context;

	public FriendListFragment(
			Context context) {
		this.context = context;
		//this.friendList = _friendList;
		//this.inviteList = _inviteList;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//loadfriendsAndInvites();
		FriendListFragment.this.onClick(myFriendsBtn);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.myfriends_fragment_1, container,
				false);

		list_friends = (ListView) view.findViewById(R.id.list_friends);
		myFriendsBtn = (Button) view.findViewById(R.id.myfriendsBtn);
		invitesBtn = (Button) view.findViewById(R.id.invitesBtn);
		invitenowBtn = (Button) view.findViewById(R.id.invitenowBtn);
		invitetitleTV = (TextView) view.findViewById(R.id.notInvitedTitle);
		addFrndBtn = (Button) view.findViewById(R.id.addfrndBtn);
		myFriendsBtn.setOnClickListener(this);
		invitesBtn.setOnClickListener(this);
		invitenowBtn.setOnClickListener(this);
		addFrndBtn.setOnClickListener(this);

		myFriendsBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
				R.drawable.white_line);
		invitesBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

		myFriendsBtn.setTextColor(Color.parseColor("#ffffff"));
		invitesBtn.setTextColor(Color.parseColor("#ff9a75"));

		

		return view;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if (arg0 == myFriendsBtn) {
			
			loadfriends();

		} else if (arg0 == invitesBtn) {
			
			loadInvites();
			
		} else if (arg0 == invitenowBtn || arg0 == addFrndBtn) {
			Fragment fragment = new InviteFriendFragment();

			FragmentTransaction ft = getFragmentManager().beginTransaction();

			ft.addToBackStack("InviteFriendFragment");
			ft.replace(R.id.content_frame, fragment, "InviteFriendFragment");

			ft.commit();
		}

	}
	
	private void loadfriendsAndInvites() {
		new ApplicationAsk(this.getActivity(), "Fetching friends . .",
				new ApplicationService() {
			private String resp, inviteResp;

			@Override
			public void execute() {

				String friendListURL = URLGenerator.getfriendslist();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(
						friendListURL, "",
						CacheMemory.getToken(FriendListFragment.this.getActivity()), FriendListFragment.this.getActivity());
				String inviteListURL = URLGenerator.getInviteelist();
				inviteResp = commService.sendSyncRequestWithToken(
						inviteListURL, "",
						CacheMemory.getToken(FriendListFragment.this.getActivity()) , FriendListFragment.this.getActivity());
				// InviteeList
			}

			@Override
			public void postExecute() {

				try {

					JSONObject friendResponseObj = new JSONObject(resp);
					JSONObject inviteResponseObj = new JSONObject(
							inviteResp);
					String status = friendResponseObj
							.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = friendResponseObj
								.getJSONArray("data");
						JSONArray inviteArray = inviteResponseObj
								.getJSONArray("data");

						FriendListFragment.this.friendList = dataObj;
						FriendListFragment.this.inviteList = inviteArray;
						adapter = new FriendAdapter(getActivity(), friendList);
						list_friends.setAdapter(adapter);
						//FriendListFragment.this.onClick(myFriendsBtn);
						
						
					} else {
						Toast.makeText(
								FriendListFragment.this.getActivity(),
								""
										+ friendResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}
			}
		}).execute();
	}
	
	private void loadfriends() {
		new ApplicationAsk(this.getActivity(), "Fetching friends . .",
				new ApplicationService() {
			private String resp;

			@Override
			public void execute() {

				String friendListURL = URLGenerator.getfriendslist();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(
						friendListURL, "",
						CacheMemory.getToken(FriendListFragment.this.getActivity()), FriendListFragment.this.getActivity());
				
				// InviteeList
			}

			@Override
			public void postExecute() {

				try {

					JSONObject friendResponseObj = new JSONObject(resp);
					
					String status = friendResponseObj
							.getString("status");
					if (status.equalsIgnoreCase("success")) {

						JSONArray dataObj = friendResponseObj
								.getJSONArray("data");
						
						FriendListFragment.this.friendList = dataObj;
						
						/*adapter = new FriendAdapter(getActivity(), friendList);
						list_friends.setAdapter(adapter);
						FriendListFragment.this.onClick(myFriendsBtn);*/
						
						
						invitenowBtn.setVisibility(View.GONE);
						invitetitleTV.setVisibility(View.GONE);
						myFriendsBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
								R.drawable.white_line);
						invitesBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

						myFriendsBtn.setTextColor(Color.parseColor("#ffffff"));
						invitesBtn.setTextColor(Color.parseColor("#ff9a75"));

						list_friends.setVisibility(View.VISIBLE);
						if (friendList.length() == 0) {
							
							invitenowBtn.setVisibility(View.VISIBLE);
							invitetitleTV.setVisibility(View.VISIBLE);
							invitetitleTV.setText("You do not have any friends to Play");
							list_friends.setVisibility(View.GONE);
						}
						else
						{
							list_friends.setVisibility(View.VISIBLE);
							invitenowBtn.setVisibility(View.GONE);
							invitetitleTV.setVisibility(View.GONE);
						}
						adapter = new FriendAdapter(getActivity(), friendList);
						adapter.setItems(friendList);
						list_friends.setAdapter(adapter);
						//adapter.notifyDataSetChanged();
						
						
						
					} else {
						Toast.makeText(
								FriendListFragment.this.getActivity(),
								""
										+ friendResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}
			}
		}).execute();
	}
	
	private void loadInvites() {
		new ApplicationAsk(this.getActivity(), "Fetching Invitee . .",
				new ApplicationService() {
			private String resp, inviteResp;

			@Override
			public void execute() {

				/*String friendListURL = URLGenerator.getfriendslist();
				CommunicationService commService = new CommunicationService();
				resp = commService.sendSyncRequestWithToken(
						friendListURL, "",
						CacheMemory.getToken(FriendListFragment.this.getActivity()));*/
				String inviteListURL = URLGenerator.getInviteelist();
				CommunicationService commService = new CommunicationService();
				inviteResp = commService.sendSyncRequestWithToken(
						inviteListURL, "",
						CacheMemory.getToken(FriendListFragment.this.getActivity()), FriendListFragment.this.getActivity());
				// InviteeList
			}

			@Override
			public void postExecute() {

				try {

					
					JSONObject inviteResponseObj = new JSONObject(
							inviteResp);
					String status = inviteResponseObj
							.getString("status");
					if (status.equalsIgnoreCase("success")) {

						
						JSONArray inviteArray = inviteResponseObj
								.getJSONArray("data");

						
						FriendListFragment.this.inviteList = inviteArray;
						/*adapter = new FriendAdapter(getActivity(), friendList);
						list_friends.setAdapter(adapter);*/
						invitesBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
								R.drawable.white_line);
						myFriendsBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

						invitesBtn.setTextColor(Color.parseColor("#ffffff"));
						myFriendsBtn.setTextColor(Color.parseColor("#ff9a75"));
						
						Log.v("", "inviteArray.length : " + inviteArray.length());
						Log.v("", "inviteList.length : " + FriendListFragment.this.inviteList.length());

						if (FriendListFragment.this.inviteList.length() == 0) {
							
							invitenowBtn.setVisibility(View.VISIBLE);
							invitetitleTV.setVisibility(View.VISIBLE);
							invitetitleTV.setText("You have not invited anyone");
							list_friends.setVisibility(View.GONE);
						}
						else
						{
							list_friends.setVisibility(View.VISIBLE);
							invitenowBtn.setVisibility(View.GONE);
							invitetitleTV.setVisibility(View.GONE);
						}
						adapter = new FriendAdapter(getActivity(), FriendListFragment.this.inviteList);
						adapter.setItems(FriendListFragment.this.inviteList);
						list_friends.setAdapter(adapter);
						//adapter.notifyDataSetChanged();
						
						
					} else {
						Toast.makeText(
								FriendListFragment.this.getActivity(),
								""
										+ inviteResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}
			}
		}).execute();
	}

	
}
