package com.o2m.gsk;

import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.CryptedConfiguration;
import com.o2m.gsk.utility.URLGenerator;
import com.o2m.gsk.utility.ValidationUtility;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordSetUp extends Dialog implements
		android.view.View.OnClickListener {

	public Activity activity;
	public Dialog dialog;
	public Button confirmBtn , cancelBtn;
	private EditText passwordET, repasswordET;

	public PasswordSetUp(Activity context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.activity = context;

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.password_setup);

		confirmBtn = (Button) findViewById(R.id.psconfirmBtn);
		passwordET = (EditText) findViewById(R.id.passET);
		repasswordET = (EditText) findViewById(R.id.confirmpassET);
		// resendBtn = (Button) findViewById(R.id.verifyOTPBtn);
		cancelBtn = (Button) findViewById(R.id.cancelBtn);
		confirmBtn.setOnClickListener(this);
		cancelBtn.setOnClickListener(this);

		// resendBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

		if(arg0 == cancelBtn)
		{
			PasswordSetUp.this.dismiss();
		}
		else if(arg0 == confirmBtn)
		{
			if(!ValidationUtility.isValidPassword(passwordET.getText().toString()))
			{
				passwordET.setError("Minimum four characters, at least one uppercase letter, one lowercase letter, one number and one special character");
				return;
			}
			
			if(!passwordET.getText().toString().trim().equals(repasswordET.getText().toString().trim()))
			{
				Toast.makeText(activity, "password mismatch",
						Toast.LENGTH_SHORT).show();
				repasswordET.setError("Password mismatch");
				return;
			}

			new ApplicationAsk(activity,"Please wait", new ApplicationService() {
				private String resp;

				@Override
				public void postExecute() {
					// TODO Auto-generated method stub
					try {

						JSONObject registraionResponseObj = new JSONObject(resp);
						String status = registraionResponseObj.getString("status");

						if (status.equalsIgnoreCase("success")) {

							Toast.makeText(activity, "Password saved successfully", Toast.LENGTH_LONG).show();
							PasswordSetUp.this.dismiss();
							//Password successfully changed
							Intent loginIntent = new Intent(activity,
									LoginActivity.class);
							activity.startActivity(loginIntent);

						} else {
							Toast.makeText(
									activity,
									""
											+ registraionResponseObj
													.getString("message"),
									Toast.LENGTH_LONG).show();
						}

					} catch (JSONException e) {

						e.printStackTrace();
					}

				}

				@Override
				public void execute() {
					// TODO Auto-generated method stub
					JSONObject savepwdObj = new JSONObject();
					try {
						CryptedConfiguration preferences = new CryptedConfiguration(
								activity, activity.getResources().getString(R.string.crypted_configuration), activity.getResources().getString(R.string.key),
								true);
						
						//savepwdObj.put("UserId", preferences.getString("UserId"));
						savepwdObj.put("UserId", CacheMemory.getOTPToken(activity));
						savepwdObj.put("password", passwordET.getText().toString()
								.trim());

					} catch (JSONException e) {

						e.printStackTrace();
					}

					String savepasswordURL = URLGenerator.getSavePasswordURL();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
					CommunicationService commService = new CommunicationService();
					resp = commService.sendSyncRequest(savepasswordURL,
							savepwdObj.toString() , PasswordSetUp.this.activity);
				}

			}).execute();
		
		}
		
	}

}
