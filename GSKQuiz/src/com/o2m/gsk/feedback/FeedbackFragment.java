package com.o2m.gsk.feedback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class FeedbackFragment extends Fragment implements OnClickListener, OnCheckedChangeListener {

	protected String TAG, currentQueID;
	private JSONArray fbArray, fbAnsJsonArray;
	private TextView topicNameTV , fbque1 , fbque2;
	private CheckBox op1CB , op2CB;
	private RatingBar ratingBar;
	private Button nextBtn , yesBtn ;
	int currentPage = 0, totalPage = 0;

	public FeedbackFragment(JSONArray scoreObj) {
		// TODO Auto-generated constructor stub
		this.fbArray = scoreObj;
		fbAnsJsonArray = new JSONArray();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.feedback_fragment, container,
				false);
		
		
		
		yesBtn = (Button) rootView.findViewById(R.id.yesbtn);
		yesBtn.setOnClickListener(this);
		/*noBtn = (Button) rootView.findViewById(R.id.nobtn);
		noBtn.setOnClickListener(this);*/
		
		op1CB = (CheckBox) rootView.findViewById(R.id.option1cb);
		op2CB = (CheckBox) rootView.findViewById(R.id.option2cb);
		op1CB.setOnCheckedChangeListener(this);
		op2CB.setOnCheckedChangeListener(this);
		
		fbque1 = (TextView) rootView.findViewById(R.id.fbque1Tv);
		fbque2 = (TextView) rootView.findViewById(R.id.fbque2Tv);
		
		totalPage = this.fbArray.length();

		//this.loadPage(currentPage);
		this.setQuestions();

		return rootView;
	}
	
	private void setQuestions() {
		// TODO Auto-generated method stub
		try {
			
			if(fbArray.length() > 1)// change it to zero
			{
			String que = this.fbArray.getJSONObject(0).getString("Question");
			currentQueID = this.fbArray.getJSONObject(0).getString("Id");
			fbque1.setText("1. "+que);
			
			String op1 = this.fbArray.getJSONObject(0).getString("Option1");
			String op2 = this.fbArray.getJSONObject(0).getString("Option2");
			
			if(op1.trim().isEmpty())
				op1CB.setText("");
			if(op2.trim().isEmpty())
				op2CB.setText("");
			}
		} 
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	private void loadPage(int i) {
		// TODO Auto-generated method stub
		try {
			String que = this.fbArray.getJSONObject(i).getString("Question");
			currentQueID = this.fbArray.getJSONObject(i).getString("Id");
			topicNameTV.setText(que);
			
			if(currentPage == totalPage - 1)
			{
				nextBtn.setText("SUBMIT");
				nextBtn.setTag(0);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		/*
		if (currentPage < totalPage-1) {
			captureAns(currentPage);
			currentPage++;
			loadPage(currentPage);
		} else {
			// submit feedback
			captureAns(currentPage);// To capture last ans
			submitFeedback();
		}
		*/
		
		if(v == yesBtn)
		{
			
			Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
		    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		    
		    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
		                   
		                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		    try {
		        startActivity(goToMarket);
		    } catch (ActivityNotFoundException e) {
		        startActivity(new Intent(Intent.ACTION_VIEW,
		                Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
		    }
			
		}
		/*else if(v == noBtn)
		{
			getFragmentManager()
			.popBackStack();
		}*/
		
	}

	private void submitFeedback() {
		// TODO Auto-generated method stub
		new ApplicationAsk(FeedbackFragment.this.getActivity(), "Saving feedback",
				new ApplicationService() {

					private String resp;

					@Override
					public void postExecute() {
						
						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {


								/*getFragmentManager()
										.popBackStack();
*/
								Toast.makeText(
										FeedbackFragment.this.getActivity(),
										"Feedback submitted successfully",
										Toast.LENGTH_LONG).show();


							} else {
								Toast.makeText(
										FeedbackFragment.this.getActivity(),
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {

						String feedbackURL = URLGenerator.getSaveFeedbacksUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								feedbackURL, FeedbackFragment.this.fbAnsJsonArray.toString(), CacheMemory
										.getToken(FeedbackFragment.this
												.getActivity()), FeedbackFragment.this
												.getActivity());


					}
				}).execute();

	}

	private void captureAns(String ans) {
		// TODO Auto-generated method stub

		JSONObject fbAns = new JSONObject();

		try {

			fbAns.put("QuestionId", currentQueID);
			fbAns.put("rating", ans);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fbAnsJsonArray.put(fbAns);

	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		
		if(buttonView == op1CB)
		{
			this.captureAns(op1CB.getText().toString());
			this.submitFeedback();
		}
		
		else if(buttonView == op2CB)
		{
			this.captureAns(op2CB.getText().toString());
			this.submitFeedback();
		}
		
		
	}

}
