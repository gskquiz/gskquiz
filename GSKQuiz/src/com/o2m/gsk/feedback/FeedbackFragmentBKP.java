package com.o2m.gsk.feedback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.o2m.gsk.R;
import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.URLGenerator;

public class FeedbackFragmentBKP extends Fragment implements OnClickListener {

	protected String TAG, currentQueID;
	private JSONArray fbArray, fbAnsJsonArray;
	private TextView topicNameTV;
	private RatingBar ratingBar;
	private Button nextBtn;
	int currentPage = 0, totalPage = 0;

	public FeedbackFragmentBKP(JSONArray scoreObj) {
		// TODO Auto-generated constructor stub
		this.fbArray = scoreObj;
		fbAnsJsonArray = new JSONArray();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.feedback_fragment, container,
				false);
		topicNameTV = (TextView) rootView.findViewById(R.id.fbqueTv);
		ratingBar = (RatingBar) rootView.findViewById(R.id.rabingBar);
		nextBtn = (Button) rootView.findViewById(R.id.nextBtn);
		nextBtn.setTag(1);
		nextBtn.setOnClickListener(this);
		totalPage = this.fbArray.length();

		this.loadPage(currentPage);

		return rootView;
	}

	private void loadPage(int i) {
		// TODO Auto-generated method stub
		try {
			String que = this.fbArray.getJSONObject(i).getString("Question");
			currentQueID = this.fbArray.getJSONObject(i).getString("Id");
			topicNameTV.setText(que);
			
			if(currentPage == totalPage - 1)
			{
				nextBtn.setText("SUBMIT");
				nextBtn.setTag(0);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
		
		if (currentPage < totalPage-1) {
			captureAns(currentPage);
			currentPage++;
			loadPage(currentPage);
		} else {
			// submit feedback
			captureAns(currentPage);// To capture last ans
			submitFeedback();
		}
	}

	private void submitFeedback() {
		// TODO Auto-generated method stub
		new ApplicationAsk(FeedbackFragmentBKP.this.getActivity(), "Saving feedback",
				new ApplicationService() {

					private String resp;

					@Override
					public void postExecute() {
						Log.v(TAG, " onPostExecute Login Response : " + resp);

						try {

							JSONObject dashboardResponseObj = new JSONObject(
									resp);
							String status = dashboardResponseObj
									.getString("status");

							if (status.equalsIgnoreCase("success")) {

								/*JSONArray dataObj = dashboardResponseObj
										.getJSONArray("data");

								Fragment fragment = new MyScoreFragment(dataObj);
								FragmentManager fragmentManager = getFragmentManager();
								fragmentManager.beginTransaction()
										.replace(R.id.content_frame, fragment)
										.commit();*/

								getFragmentManager()
										.popBackStack();
								
/*								ft.addToBackStack("messageList");
								ft.replace(R.id.content_frame, fragment,
										"messageList");

								ft.commit();*/

							} else {
								Toast.makeText(
										FeedbackFragmentBKP.this.getActivity(),
										""
												+ dashboardResponseObj
														.getString("message"),
										Toast.LENGTH_LONG).show();
							}

						} catch (JSONException e) {

							e.printStackTrace();
						}

					}

					@Override
					public void execute() {

						String feedbackURL = URLGenerator.getSaveFeedbacksUrl();
						CommunicationService commService = new CommunicationService();
						resp = commService.sendSyncRequestWithToken(
								feedbackURL, FeedbackFragmentBKP.this.fbAnsJsonArray.toString(), CacheMemory
										.getToken(FeedbackFragmentBKP.this
												.getActivity()), FeedbackFragmentBKP.this
												.getActivity());// "sAGaW7GLq8xC9bZ+WHlL+JA7EpoZIEuhXvGcaVAY1OL4vptHL0N0leI23RBS0vXALJDxybbJ4RmuNdGzCnX3XsjDU6sAMch4AsvOv9BylN0=");//preferences.getString("Token"));

						// return resp;

					}
				}).execute();

	}

	private void captureAns(int currentPage2) {
		// TODO Auto-generated method stub

		JSONObject fbAns = new JSONObject();

		try {

			fbAns.put("QuestionId", currentQueID);
			fbAns.put("rating", String.valueOf((int) ratingBar.getRating()));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		fbAnsJsonArray.put(fbAns);

	}

}
