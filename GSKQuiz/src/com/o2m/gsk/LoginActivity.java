package com.o2m.gsk;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.o2m.gsk.utility.ApplicationAsk;
import com.o2m.gsk.utility.ApplicationService;
import com.o2m.gsk.utility.CacheMemory;
import com.o2m.gsk.utility.CommunicationService;
import com.o2m.gsk.utility.CryptLib;
import com.o2m.gsk.utility.URLGenerator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {

	private String TAG = "LoginActivity";

	private Button loginBtn, forgotPasswordBtn ,registerBtn,eyeBtn;
	private EditText userNameEditText, passwordEditText;

	//String divCode, roleId, feedback;

	@Override
	public void onClick(View arg0) {

		if (arg0 == loginBtn) {
			
			
			validateLogin(userNameEditText.getText().toString(),
					passwordEditText.getText().toString());
			
			
		} else if (arg0 == forgotPasswordBtn) {
			// Toast.makeText(this, "Not yet implemented",
			// Toast.LENGTH_LONG).show();
		  ForgotPassword fp = new ForgotPassword(this);
		  fp.show();
		}
		else if(arg0 == registerBtn)
		{
			 Intent registerIntent = new Intent(this, RegistrationActivity.class);
	           startActivity(registerIntent);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		userNameEditText = (EditText) findViewById(R.id.editTxt_UsrName);
		passwordEditText = (EditText) findViewById(R.id.editTxt_PWD);
		loginBtn = (Button) findViewById(R.id.btn_Login);
		forgotPasswordBtn = (Button) findViewById(R.id.forgotpasswordbtn);
		registerBtn = (Button) findViewById(R.id.registerbtn);
		eyeBtn = (Button) findViewById(R.id.eyeBtn);
		SharedPreferences sharedPref = this
				.getPreferences(Context.MODE_PRIVATE);
		String offlineName = sharedPref.getString("uname", "");
		String offlinePass = sharedPref.getString("upwd", "");

		if (!offlineName.equals(""))
			userNameEditText.setText(offlineName);

		//if (!offlineName.equals(""))
			//passwordEditText.setText(offlinePass);

		loginBtn.setOnClickListener(this);
		forgotPasswordBtn.setOnClickListener(this);
		registerBtn.setOnClickListener(this);
		eyeBtn.setOnTouchListener(new OnTouchListener() {
	       
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				// TODO Auto-generated method stub
		        switch ( event.getAction() ) {
                case MotionEvent.ACTION_DOWN: 
                   passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
                case MotionEvent.ACTION_UP: 
                	passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                	passwordEditText.setSelection(passwordEditText.getText().length());
                break;
                }
                return true;
			}
	    });
		
		TextView nu = (TextView) findViewById(R.id.newUserTxt);
		nu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 Intent registerIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
		           startActivity(registerIntent);
			}
		});
		
		testMethod();
	}

	private void testMethod() {
		// TODO Auto-generated method stub
		 try {

	            String plainText = "this is my plain text";
	            String key = "GSK";
	            String iv = "1234123412341234";

	            CryptLib cryptLib = new CryptLib();

	            String encryptedString = cryptLib.encryptSimple(plainText, key, iv);
	            System.out.println("encryptedString " + encryptedString);

	            String decryptedString = cryptLib.decryptSimple(encryptedString, key, iv);
	            System.out.println("decryptedString " + decryptedString);

	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	}

	void login() {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}


	private void validateLogin(final String username, final String password) {
		
		if(username.equalsIgnoreCase(""))
		{
			//Toast.makeText(this, "Please enter user name", Toast.LENGTH_SHORT).show();
			userNameEditText.setError("Please enter user name");
			return;
		}
		
		if(password.equalsIgnoreCase(""))
		{
			//Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
			passwordEditText.setError("Please enter password");
			return;
		}
		
		
		new ApplicationAsk(this, "Checking credentials", new ApplicationService() {

			private String resp;

			@Override
			public void postExecute() {
				

				try {

					SharedPreferences sharedPref = LoginActivity.this
							.getPreferences(Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putString("uname", username);
					editor.putString("upwd", password);
					editor.commit();
					JSONObject loginResponseObj = new JSONObject(resp);
					//JSONObject loginResponseObj1 = new JSONObject(
						//	loginResponseObj.getString("d"));
					/*
					 * {
    "status": "success",
    "message": "",
    "data": [
        {
            "Token": "aPBhNUiX3RXMmMETTLXfDGpBYvLDpoWGUgmg613HDVaVM7WdGR/RZ7AgFLfZL2KyiP/LjFSdVQ+ly58PaOsQ5dUBeMp6BchX9CfW5ccjwsc="
        }
    ]
}
					 * 
					 * */
					String status = loginResponseObj.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONObject dataObj = loginResponseObj
								.getJSONArray("data").getJSONObject(0);
						String Token = dataObj.getString("Token");
						CacheMemory.TOKEN = Token;
						CacheMemory.setToken(LoginActivity.this, Token);
						Intent homeIntent = new Intent(LoginActivity.this , GSKHomeActivity.class);
						homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(homeIntent);
						Toast.makeText(
								LoginActivity.this,
								"Login Successful",
										Toast.LENGTH_LONG).show();

					} else {
						Toast.makeText(
								LoginActivity.this,
								""
										+ loginResponseObj
										.getString("message"),
										Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {

					e.printStackTrace();
				}

			}

			@Override
			public void execute() {
				JSONObject loginObj = new JSONObject();
				try {
					loginObj.put("email", username);
					loginObj.put("password", password);
					// loginObj.put("", "");
				} catch (JSONException e) {

					e.printStackTrace();
				}

				// URLGenerator.getLoginUrl();

				String loginURL = URLGenerator.getLoginUrl();// "http://wockhardt.oh22media.com/myservice/service1.asmx/validate_sql_user";
				// String loginURL =
				// "http://training.wockhardt.com/mobile/service1.asmx/validate_sql_user";

				CommunicationService commService = new CommunicationService();
				/*resp = commService.sendSyncRequest(loginURL,
						loginObj.toString());*/
				resp = commService.sendSyncRequest(loginURL, loginObj.toString(), LoginActivity.this);
				// return resp;

			}
		}).execute();
	}

	
}
