package com.o2m.gsk;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;




@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LoadWebPageFragment extends Fragment {

	private WebView myBrowser;
	private int screenName = 0;

	public int getScreenName() {
		return screenName;
	}


	public void setScreenName(int screenName) {
		this.screenName = screenName;
	}


	public LoadWebPageFragment() {

	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_offline_web , container, false);

		myBrowser = (WebView) rootView.findViewById(R.id.mybrowser);
		myBrowser.setBackgroundColor(Color.TRANSPARENT);
		
		myBrowser.getSettings().setJavaScriptEnabled(true); // enable javascript
		
		myBrowser.getSettings().setDomStorageEnabled(true);
	        String databasePath = this.getActivity().getApplicationContext().getDir("database", Context.MODE_PRIVATE).getPath(); 
	        myBrowser.getSettings().setDatabasePath(databasePath);
		if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) 
			myBrowser.getSettings().setAllowUniversalAccessFromFileURLs(true);
		
	/*	JsHandler _jsHandler = new JsHandler(this.getActivity(), myBrowser);		
		myBrowser.addJavascriptInterface(_jsHandler, "JsHandler");
		*/
		myBrowser.setWebChromeClient(new WebChromeClient() {
			
			@Override
			public boolean onJsAlert(WebView view, String url, String message,
					JsResult result) {
				// TODO Auto-generated method stub
				Log.v("", "url : " + url);
				Log.v("", "message : " + message);
				
				//Toast.makeText(LoadWebPageFragment.this.getActivity(), message, Toast.LENGTH_LONG).show();
				//return true;
				return super.onJsAlert(view, "GSKApptutorial", message, result);
			}
			
			/*@Override
			public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
		                //Required functionality here
		                return super.onJsAlert(view, url, message, result);
		       }*/});
		
		
		this.loadRespectiveHtml();

		return rootView;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		myBrowser.onPause();
		super.onPause();
	}
	

	
	private void loadRespectiveHtml() {

		if(getScreenName() == 0)
			myBrowser.loadUrl("file:///android_asset/Map/index.html");
		else if(getScreenName() == 1)
			myBrowser.loadUrl("http://india-pharma.gsk.com/en-in/products/prescribing-information");
		else if(getScreenName() == 2)
			myBrowser.loadUrl("file:///android_asset/tnc.html");
		else if(getScreenName() == 3)
			myBrowser.loadUrl("file:///android_asset/privacy.html");
		else if(getScreenName() == 4)
			myBrowser.loadUrl("file:///android_asset/contactus.html");
		else if(getScreenName() == 5)
			myBrowser.loadUrl("file:///android_asset/adverseEvent.html");
		else if(getScreenName() == 6)
			myBrowser.loadUrl("file:///android_asset/GSKApptutorial/index.html");
		
		myBrowser.setWebViewClient(new WebViewClient() {
			 private ProgressDialog mProgress;

			   public void onPageFinished(WebView view, String url) {
				   mProgress.dismiss();
	                mProgress = null;
			   }
			   
			   @Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub
				super.onPageStarted(view, url, favicon);
				 if (mProgress == null) {
		                mProgress = new ProgressDialog(getActivity());
		                mProgress.setMessage("Loading . .");
		                mProgress.show();
		            }
			}
			});

			
			
				

	}


}
