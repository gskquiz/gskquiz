// JavaScript Document
const serverUrl = "http://gskquizapp.oh22works.com/api/QuizApp/";
var charLimit = 120;
var accessToken = localStorage.getItem("accessToken");
var notificationInterval;

var wockhardt = angular.module('gsk', []).filter('filter_trusted_html', ['$sce', function($sce) {
	return function(text) {
		//console.log(text);
		return $sce.trustAsHtml(unEntity(text));
	};
	
}]);

wockhardt.run(function($rootScope, $http, $sce){
	$rootScope.trustUrl = function(src){
		return $sce.trustAsResourceUrl(src);
	}
	if (accessToken!=null || accessToken!=""){
		notificationInterval = window.setInterval(notification, 3000);
	}

})



function getDataByKey(key){
	var obj = JSON.parse(localStorage.getItem("user"));
	return obj[key];
}
function errorShow(obj, msg){
	if (obj.parents(".elem").find(".error").length<1){
		obj.focus();
		obj.parents(".elem").append("<span class='error'></span>");
		obj.parents(".elem").find(".error").text(msg);
		obj.parents(".elem").find(".error").show();
		window.setTimeout(function(){
			obj.parents(".elem").find(".error").remove();
		}, 2000);
	}
}
function showDialog(msg, type, url){
	$(".dialog").text(msg);
	$(".dialog").fadeIn();
	window.setTimeout(function(){
			//$(".dialog, .overlay").hide();
			if (type=="refresh"){
				location.reload();
			}else if(type=="redirect"){
				location.href = url;
			}
		}, 2000);

}

	function waitDialog(msg){
	$(".dialog").text(msg);
	$(".dialog, .overlay").fadeIn();
}




$.easing.myease = function(x, t, b, c, d) {
		if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
			return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
};

function unEntity(str){
	
   //return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">").replace(/&nbsp;/g,'');
   return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
}

function entity(str){
	
   return str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
}




function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function validateUrl(url){
var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
if (!re.test(url)) { 

    return false;
}else{
	return true;
}

}





function notification(){


	accessToken = localStorage.getItem("accessToken");



			
				$.ajax({
				url:serverUrl + "GetNotification",
				type:"POST",
				headers: {
					"Token":accessToken,		
				},
				success:function(response){
					var obj = JSON.parse(JSON.stringify(response));
				console.info(obj);
					if (obj.data.length>0){						
						if(obj.data[0].Type=="5"){	
							var conId = obj.data[0].ChallengeId;
							
						if (localStorage.getItem("playing")=="" || localStorage.getItem("playing")=="0" || localStorage.getItem("playing")==null){
			
							var params = {ConnectionId:conId}
			
									$.ajax({
									url:serverUrl + "CheckConnectionStatus",
									type:"POST",
									data:params,
									headers: {
										"Token":accessToken,		
									},
									success:function(response){
										var obj = JSON.parse(JSON.stringify(response));
										
										if (obj.data[0].IsAccepted==0){
											if (confirm("Accept Request")==true){	
														requestAccepted(conId);
														localStorage.setItem("conId",conId);	
														window.clearInterval(notificationInterval);
											}
										}
										
										
										
									}

		
						})
							
							
							
							
							
							
								
								
							
							}
						}
						
					}
		
					
				}

		
		
		})






}


function requestAccepted(conId){
	//alert(conId);
	if (conId!="" || conId != null){
				var params = {ConnectionId:conId};			
				$.ajax({
				url:serverUrl + "AcceptQuizRequest",
				type:"POST",
				data:params,
				headers: {
					"Token":accessToken,		
				},
				success:function(response){
					var obj = JSON.parse(JSON.stringify(response));
					var opp = obj.data[0].OpponentId;
						var oppName = obj.data[0].OpponentName;
						var qId = obj.data[0].QuizId;
					
						localStorage.setItem("playing", "1");
						localStorage.setItem("qid", qId);
						localStorage.setItem("oppName", oppName);						
						location.href = "quiz.html";
				}

		})
}
	
}


function appendNotify(data){


for (i in data){
	}




}

